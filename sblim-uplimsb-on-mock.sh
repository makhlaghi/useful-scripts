#!/bin/bash
#
# Script to verify the surface brightness limit and upper-limit
# surface brightness: it will generate a mock noisy image with a
# pre-defined surface brightness limit along with a pre-defined Sersic
# profile in the center. It will then measure the surface brightness
# limit and upper-limit surface brightness on it.
#
# USAGE:
#   ./sblim-uplimsb-on-mock.sh /path/to/build/directory
#
# All built files will be in the given directory.
#
# Copyright (C) 2024-2024 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.

# Parameters/constants.
gal_n=1                         # Sersic index of galaxy in image.
gal_pa=30                       # Position angle of galaxy.
gal_q=0.7                       # Axis ratio of galaxy.
gal_re=30                       # Sersic effective radius (r_e).
gal_mag=20                      # Magnitude of mock galaxy in image.
gal_trunc=3                     # Trunction of galaxy (multiple of r_e).

sblimit=29                      # Surface brightness limit.
sblimitsigman=3                 # Multiple of sigma.
sblimitareaarcsec2=100          # Area SBL was measured on.
uplimsbnum=10000                # Num. good randoms for upper-limit SB.

center_dec=0                    # RA of image center (for WCS).
center_ra=180                   # Dec of image center (for WCS).
zeropoint=22.5                  # Zero point of image.
out_width=501                   # Number of pixels in width of output.
out_height=501                  # Number of pixels in height of output.
pixel_width_arcsec=0.2          # Pixel scale (arcsec/pixel)





# Abort the script in case of an error and be ready for operating
# system environments that put a ',' as a decimal point.
set -e
export LC_NUMERIC=C





# Make the build directory
if [ x"$1" = x ]; then
    echo "ERROR: a build directory should be given as an argument"
    exit 1
else
    bdir="$1"
fi
if ! [ -d "$bdir" ]; then mkdir "$bdir"; fi





# Necessary variables in multiple phases.
crpix1=$(echo $out_width  | awk '{print int($1/2)+1}')
crpix2=$(echo $out_height | awk '{print int($1/2)+1}')





# Build an arbitrary galaxy in the center of a noisy with a give
# background level (Poisson noise).
noised="$bdir"/noised.fits
nonoise="$bdir"/no-noise.fits
cdelt=$(echo $pixel_width_arcsec | awk '{print $1/3600}')
echo "1 $crpix1 $crpix2 1 $gal_re $gal_n $gal_pa $gal_q \
      $gal_mag $gal_trunc" \
    | astmkprof --oversample=1 \
                --output=$nonoise \
                --cdelt=$cdelt,$cdelt \
                --zeropoint=$zeropoint \
                --crpix=$crpix1,$crpix2 \
                --crval=$center_ra,$center_dec \
                --mergedsize=$out_width,$out_height





# Convert the surface brightness limit to a noise level in counts and
# add it to the mock image and write the inserted noise level into the
# 0-th HDU.
pixarea=$(astfits $nonoise --pixelareaarcsec2)
sigma_counts=$(astarithmetic $sblimit $zeropoint \
                             $sblimitareaarcsec2 $pixarea x sqrt \
                             sb-to-counts $sblimitsigman / --quiet)
astarithmetic $nonoise $sigma_counts mknoise-sigma -o$noised
astfits $noised -h0 --write=/,"From script" \
        --write=NOISESBL,$sblimit,"Input surface brightness limit" \
        --write=NOISESIG,$sigma_counts,"Standard deviation of noise"





# Prepare a mock label for the upper-limit surface brightness.
lab="$bdir"/uplim-lab.fits
r=$(astarithmetic $sblimitareaarcsec2 pi / sqrt \
                  $pixel_width_arcsec / -q)
echo "1 $crpix1 $crpix2 5 $r 0 0 1 1 1" \
    | astmkprof --background=$noised --clearcanvas \
                --oversample=1 --mforflatpix -o$lab \
                --type=uint8





# Run NoiseChisel to get the inputs for surface brightness limit.
nc="$bdir"/nc.fits
ncconv="$bdir"/nc-conv.fits
astnoisechisel $noised -o$nc





# Run MakeCatalog to get the surface brightness limit and upper limit
# magnitude.
cat="$bdir"/catalog.fits
catconv="$bdir"/catalog-conv.fits
astmkcatalog $lab -h1 \
             --output=$cat \
             --checkuplim=1 \
             --forcereadstd \
             --valuesfile=$nc \
             --upmaskfile=$nc \
             --upnum=$uplimsbnum \
             --upmaskhdu=DETECTIONS \
             --zeropoint=$zeropoint \
             --upnsigma=$sblimitsigman \
             --ids --upperlimit-sb --area-arcsec2





# Check the statistics of the upper-limit apertures.
upcheck=$(echo $cat | sed -e's|.fits|_upcheck.fits|')
aststatistics $upcheck -cRANDOM_SUM





# Print results:
sbl=$(astfits $cat -h1 --keyvalue=SBLMAG -q | asttable -Y)
upsb=$(asttable $cat -cUPPERLIMIT_SB -Y)
upqm=$(aststatistics $upcheck -cRANDOM_SUM --quantofmean \
                     | asttable -Y)
echo; echo; echo "Results:"
echo "Input Surface brightness limit:              $sblimit"
echo "Measured Surface brightness limit:           $sbl"
echo "Measured Upper limit surface brightness:     $upsb"
echo "Skewness (quantile of mean) of upper-limit:  $upqm"
