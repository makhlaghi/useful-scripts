# Assuming the input FITS file contains all the HDUs of the final
# file, this script will put the necessary pipeline metadata on the
# 0th HDU, and do loss-less compression on each HDU, such that the
# keywords of each HDU are not compressed and immediately accessible.
#
# This script was originally designed for making data releases: where
# you have many keywords/metadata to add in the 0-th HDU and you want
# to compress the data without lossing any precision (but consume less
# storage because FITS images usually have many blank/out-of-field
# pixels).
#
# Copyright (C) 2024-2024 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.





# Exit the script in the case of failure
set -e

# 'LC_NUMERIC' is responsible for formatting numbers printed by the OS.  It
# prevents floating points like '23,45' instead of '23.45'.
export LC_NUMERIC=C

# Default values
keys=""
input=""
output=""





# Output of '--usage' and '--help':
print_usage() {
    cat <<EOF
$0: run with '--help' for list of options
EOF
}

print_help() {
    cat <<EOF
Usage: $scriptname [OPTION] FITS-file

This script will take a FITS file and prepare it for a data release:
it will add any given set of header keywords in the 0th HDU and
compress the data (not keywords) in each HDU. The custom set of header
keywords can be written in a text file and passed to this program
using the '--keys' option. The custom keyword file is a basic
plain-text file where every non-commented and non-empty line will be
used as a keyword with the following format:
 - The first word in the line can be one of the following two cases:
   - A keyword name which is not case sensitive (FITS keywords are
     always FULL-CAPS: even if a small "smallcaps" string is given, it
     will be written as SMALLCAPS). It is better to set your keyword
     names to be 8 characters or less (otherwise the FITS standard
     will add a 'HIERARCH' prefix to it; which is still usable, but
     will take valuable space in the 80-character limit of
     keyword+value+comment; thus requiring a shorter comment: comments
     are important!).
   - A '/'. In this case, the line will be interpreted as a title-keyword.
 - The second word in the line is the value of the of the keyword in for a
   standard keyword and the actual title in a title-keyword.
 - The rest of the line will be interpreted as the optional comments
   of standard keywords (title keywords will not use this). Therefore,
   it does not need any quoting: anything including and after the
   third keyword will be written in the 0th HDU as a comment.
Here is an example of a file given to '--keys':
```
/ My added keywords
ABC   2     This is a comment for the first keyword.
MAGNI 34.21 Comment of second keyword.
PHOTZ 2.3   Photometric redshift of galaxy in the image.
/ Versions and URLs
VERSION 0.23 Version of pipeline that produced this image.
URL https://path.to/your/data/release
```

Options:
 Input:
  -k, --keys=STR          Name of text file containing keywords.

 Output:
  -o, --output=STR        Name of output file.

 Operating mode:
  -?, --help              Print this help list.

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

Report bugs to mohammad@akhlaghi.org.
EOF
}





# Functions to check option values and complain if necessary.
on_off_option_error() {
    if [ x"$2" = x ]; then
        echo "$0: '$1' doesn't take any values"
    else
        echo "$0: '$1' (or '$2') doesn't take any values"
    fi
    exit 1
}

check_v() {
    if [ x"$2" = x ]; then
        cat <<EOF
$0: option '$1' requires an argument. Try '$0 --help' for more information
EOF
        exit 1;
    fi
}





# Separate command-line arguments from options. Then put the option
# value into the respective variable.
#
# OPTIONS WITH A VALUE:
#
#   Each option has three lines because we want to all common formats: for
#   long option names: '--longname value' and '--longname=value'. For short
#   option names we want '-l value', '-l=value' and '-lvalue' (where '-l'
#   is the short version of the hypothetical '--longname' option).
#
#   The first case (with a space between the name and value) is two
#   command-line arguments. So, we'll need to shift it two times. The
#   latter two cases are a single command-line argument, so we just need to
#   "shift" the counter by one. IMPORTANT NOTE: the ORDER OF THE LATTER TWO
#   cases matters: '-h*' should be checked only when we are sure that its
#   not '-h=*').
#
# OPTIONS WITH NO VALUE (ON-OFF OPTIONS)
#
#   For these, we just want the two forms of '--longname' or '-l'. Nothing
#   else. So if an equal sign is given we should definitely crash and also,
#   if a value is appended to the short format it should crash. So in the
#   second test for these ('-l*') will account for both the case where we
#   have an equal sign and where we don't.
input=""
while [ $# -gt 0 ]
do
    # Initialize 'tcol':
    tcol=""

    # Put the values in the proper variable.
    case "$1" in
        # Input parameters.
        -k|--keys)     keys="$2";                                check_v "$1" "$keys";    shift;shift;;
        -k=*|--keys=*) keys="${1#*=}";                           check_v "$1" "$keys";    shift;;
        -k*)           keys=$(echo "$1"  | sed -e's/-h//');      check_v "$1" "$keys";    shift;;

        # Output parameters
        -o|--output)        out="$2";                               check_v "$1" "$out";  shift;shift;;
        -o=*|--output=*)    out="${1#*=}";                          check_v "$1" "$out";  shift;;
        -o*)                out=$(echo "$1"  | sed -e's/-o//');     check_v "$1" "$out";  shift;;

        # Non-operating options.
        -?|--help)        print_help; exit 0;;
        -'?'*|--help=*)   on_off_option_error --help -?;;

        # Unrecognized option:
        -*) echo "$0: unknown option '$1'"; exit 1;;

        # Not an option (not starting with a '-'): assumed to be input FITS
        # file name.
        *) if [ x"$input" = x ]; then
             if [ -f "$1" ]; then
                 input="$1";
             else echo "$0: $1: no such file"; exit 1;
             fi;
           else echo "$0: only one input should be given"; exit 1
           fi; shift;;
    esac
done





# Basic sanity checks.
if [ x"$input" = x ]; then
    echo "$0: no input (an argument)";  exit 1;
fi
if [ x"$out" = x ]; then
    echo "$0: no output (use '--output')"; exit 1;
fi





# Add the possibly given metadata.
#
# Note that on the command-line double quotes are necessary for the
# comments of each keyword (to preserve the space character between
# word). But dealing with shell comment rules will complicate the
# script. So we are using a Gnuastro configuration file to setup the
# keywords for writing.
conf=$out.conf
echo 'write "EXTNAME,METADATA,Name of this HDU."' > $conf
echo 'write "BUNIT,INFO,"This HDU is just information (no data)."' >> $conf
if [ x"$keys" != x ]; then
    awk '!/^#/ && NF>0{\
            if($1=="/") \
              { \
                $1=""; \
                gsub(/^ */,""); \
                printf "write \"/,%s\"\n", $0 \
              } \
            else \
              { \
                name=$1; \
                val=$2; \
                $1=""; \
                $2=""; \
                gsub(/^ */,""); \
                printf "write \"%s,%s,%s\"\n", name,val,$0; \
              } }' $keys >> $conf
fi
astfits $input -h0 --config=$conf
rm $conf





# Add the validation keywords (CFITSIO's fpack program below will
# automatically add the 'CHECKSUM', 'DATASUM' and the keywords).
hdus=$(astfits $input --listallhdus)
for h in $hdus; do
  astfits $input -h$h --write=/,"FITS Validation and Compression"
done





# Compress the input to the output. Note that in 'fpack', the input's file
# name should be after all the options. It also requires that the output
# file does not exist.
#
#     -g2        Shuffle bytes for better compression.
#     -q 0       No "dither" (loss-less for floating point).
#     -w         Treat whole image as a single "tile".
#     -O STR     Name of output.
rm -f $out
fpack -g2 -q 0 -w -O $out $input
