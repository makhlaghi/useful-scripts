#!/bin/bash
# Simple script to mask cosmic rays in an image.
#
# USAGE:
#    ./this-script INPUT.fits INPUT-HDU OUTPUT.fits
#
# Copyright (C) 2024-2024 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.





# Basic inputs (arguments)
hdu=$2
input=$1
output=$3
if [ x"$input" = x ]; then
    echo "no first argument (input image)"; exit 1;
fi
if [ x"$hdu" = x ]; then
    echo "no second argument (input image's HDU in input file)"; exit 1;
fi
if [ x"$output" = x ]; then
    echo "no third argument (output file name)"; exit 1;
fi





# Do the operation.
astarithmetic $input -h$hdu                        set-input \
              input input 0 eq nan where           set-i \
              4 0.01 7 7 i filter-madclip-median   set-f \
              i f - f / 5 gt 1 dilate              set-m \
              i m nan where --output=$output
