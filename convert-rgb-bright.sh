#!/bin/bash 
#
# Build a color image, with one filter used in the
# low-surface-brightness regions (noise is gray while high-S/N regions
# are color).
#
# Copyright (C) 2020-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2020-2021 Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2021 Samane Raji <samaneraji@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Basic settings
# --------------
# Stop the script if there are any errors.
set -e


# buikd directory
bdir=build

# Base names for each RGB color channel.
r=$(pwd)/r.fits
g=
b=$(pwd)/g.fits

# Zeropoint (rgb filters)
r_zp=22.5
g_zp=22.5
b_zp=22.5

# Reference filter (to define thresholds and use as gray region).
rgbref=r

# Thresholds
lowsigma=0
highsigma=5
graysigma=1
kernelfwhm=0.5
output=color-new.jpg





# If the build directory doesn't exist, make it.
if ! [ -d $bdir ]; then mkdir $bdir; fi





# Convert to muJy (micro-Jy(10^-6))
multip=$(echo $r_zp | awk '{print 3631 * 10^6 / 10^($1/2.5)}')
astarithmetic $r $multip x --output=$bdir/r.fits
multip=$(echo $b_zp | awk '{print 3631 * 10^6 / 10^($1/2.5)}')
astarithmetic $b $multip x --output=$bdir/b.fits

if [ x$g = x ]; then 
    astarithmetic $bdir/r.fits $bdir/b.fits \
		  2 mean -h1 -h1 --output=$bdir/g.fits
else 
    multip=$(echo $g_zp | awk '{print 3631 * 10^6 / 10^($1/2.5)}')
    astarithmetic $g $multip x --output=$bdir/g.fits
fi





# Make a log scale
for f in r g b; do
    astarithmetic $bdir/$f.fits set-i i i minvalue - 1 + log10 \
		  --output=$bdir/$f-log.fits
done
	 



# Find the necessary thresholds:
#    1st number: threshold to define gray region
#    2nd number: flux-low threshold for the color image.
#    3rd number: flux-high threshold for the color image.
threshs=$(aststatistics $bdir/$rgbref-log.fits \
			--median --sigclip-std \
	      | awk -vg=$graysigma -vl=$lowsigma -vh=$highsigma \
		    '{print $1+g*$2, $1+l*$2, $1+h*$2}')
graythresh=$(echo "$threshs" | awk '{print $1}')
fluxlow=$(echo    "$threshs" | awk '{print $2}')
fluxhigh=$(echo   "$threshs" | awk '{print $3}')





# For check
test=$(aststatistics $bdir/$rgbref.fits --median --sigclip-std)
echo "check: $fluxlow"
echo "check: $fluxhigh"
echo "check: $graythresh"
echo "check: $test"





# Convolve the reference image with a kernel to highlight the diffuse
# flux. Note that we are using spatial domain convolution so the edges
# of the image don't get affected, for more, see
# https://www.gnu.org/software/gnuastro/manual/html_node/Spatial-vs_002e-Frequency-domain.html
if [ $kernelfwhm = 0 ]; then
    ln -sf $bdir/$rgbref-log.fits $bdir/convolved.fits
else
    astmkprof --kernel=gaussian,$kernelfwhm,5 --oversample=1 \
	      --output=$bdir/kernel.fits
    astconvolve $bdir/$rgbref-log.fits \
		--kernel=$bdir/kernel.fits \
		--domain=spatial --output=$bdir/convolved.fits
fi





# We want to use a single filter for the low S/N regions. But to
# futher highlight the interesting diffuse flux, we need to invert it
# so the brigher pixels are darker.
#
# Here is the logic of the steps we take here:
#   1. We call the convolved image 'r'.
#   2. All pixels above the 'graythresh' are selected, they are eroded
#      (to avoid noise), then dilated and all remaining holes within
#      them are filled. This process makes nice and clean islands with
#      no lakes in the middle!
#   3. We set all islands to be blank (these will later have a
#      different pixel value in each color channel) and call it 'masked'.
#   4. In the 'maskedl' and 'maskedh', we make sure no pixel is
#      below/above the final requested range.
#   5. We calculate the minimum and maximum values (since it can
#      happen that no pixel is actually above/below the range).
#   6. We invert the whole pixel range to be in final flux range
#      with subtractions and mulitplications.
astarithmetic $bdir/convolved.fits set-r \
	      r $graythresh gt 2 erode 2 dilate 2 fill-holes set-mask \
	      r mask nan where set-masked \
	      masked  masked  $fluxlow  lt $fluxlow  where set-maskedl \
	      maskedl maskedl $fluxhigh gt $fluxhigh where set-maskedlh \
	      maskedlh minvalue set-minv \
	      maskedlh maxvalue set-maxv \
	      maxv minv - set-range \
	      maskedlh minv - range / -1 x 1 + \
	      $fluxhigh $fluxlow - x $fluxlow + \
	      -o$bdir/gray.fits






# In the other two images, replace all pixels less than the
# gray-threshold with the pixels of the reference image.  Since we
# won't be touching the reference image, we'll just make a symbolic
# link to it with a similar suffix as the rest. After applying
for f in r g b; do
    astarithmetic $bdir/$f-log.fits \
		    $bdir/gray.fits isblank not \
		    $bdir/gray.fits \
		  where -g1 -o $bdir/$f-channel.fits
done



# Put the three channels into one color image.
astconvertt $bdir/r-channel.fits $bdir/g-channel.fits -o$output \
	    $bdir/b-channel.fits --fluxlow=$fluxlow \
	    --fluxhigh=$fluxhigh \
	    -h1 -h1 -h1






