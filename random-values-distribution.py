#!/usr/bin/env python3

# Generate a random value distribution following a given function
#
# This is a Python script to obtain a random distribution of values that
# follows a given function within a specified range. It is also possible to
# obtain a figure with the function and the random distribution of the
# values.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2022, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Usage
# -----
#
# python3 random-values-distribution.py \
#         --function "def f(x): return 10**(2*x - 5)" \
#         --fname f --number 5000 --bins 1000 --range=8,22





# Import modules
import sys
import random
import argparse
import numpy as np





# Read input arguments from the command line
# ------------------------------------------
#
parser = argparse.ArgumentParser(description='Arguments from the command line')
# By default, argparse uses -h, --help as arguments to provide help. To be
# able to supress this behaviour and use -h for the extension number (as
# all Gnuastro programs do), it is necessary to disable this option. Here,
# the options to obtain help are: -H, --help
parser = argparse.ArgumentParser(add_help=False)

usage="""Example: $ python3 random-values-distribution.py
                   --function "def f(x): return 10**(2*x - 5)"
                   --fname f --number 5000 --bins 1000 --range=8,22
      """

parser.add_argument('-H', '--help', action='help', default=argparse.SUPPRESS,
                    help=usage)

parser.add_argument('-F', '--function', type=str, default='def f(x): return 1',
                    help='''Function definition. Numpy is "np.".
                            E.g.,: --function "def f(x): return 2*np.sqrt(x)+1"''')

parser.add_argument('-f', '--fname', type=str, default='f',
                    help='Function name defined by "--function". I.e.,: --function f')

parser.add_argument('-b', '--bins', type=int, default=1000,
                    help='Number of bins. The bigger the better (but slower).')

parser.add_argument('-c', '--colname', type=str, default='values',
                    help='Name of the column if "--output" is ".fits" file.')

parser.add_argument('-s', '--plotscale', type=str, default='linear,linear',
                    help='x,y axis scale type (linear or log).')

parser.add_argument('-r', '--range', type=str,  default="0,1",
                    help='Range of values: (min,max). Default is: 0,1')

parser.add_argument('-o', '--output', type=str, default="stdout",
                    help='Output file name (.fits, .txt).')

parser.add_argument('-t', '--dtype', type=str, default='float32',
                    help='Number type. Default: float32.')

parser.add_argument('-n', '--number', type=int, default=1,
                    help='How many random numbers to generate.')

parser.add_argument('-d', '--decimals', type=int, default=4,
                    help='Number of decimals.')

parser.add_argument('-i', '--seed', default=None,
                    help='Seed number for random values.')

parser.add_argument('-p', '--plot', default=None,
                    help='Name of the check plot.')






# Define variables from command line arguments
# --------------------------------------------
#
args = parser.parse_args()
bins = args.bins
plot = args.plot
seed = args.seed
fname = args.fname
dtype = args.dtype
number = args.number
output = args.output
colname = args.colname
function = args.function
decimals = args.decimals
minmax = args.range.split(',')
plotscale = args.plotscale.split(',')





# Define the function given by the user
# -------------------------------------
#
# From this point, the way of calling the function that the user has
# provided with the option '--function' is: eval(fname+"(xmin)").
# Note that 'fname' is the name that the user has provided with the option
# '--fname'.  This is equivalent to have a function 'f(x)' and evaluate it
# at the point xmin with: f(xmin).
exec(function)





# For reproducibility: initialize the seed to the one the user specified
if seed != None:
    random.seed(seed)





# Obtain min and max values from --range, the x and y plot scale type, and
# the number of decimals
xmin = float(minmax[0])
xmax = float(minmax[1])

xplotscale = plotscale[0]
yplotscale = plotscale[1]

ndecimals = str(decimals)





# Define any xmin-xmax interval here! (xmin < xmax)
# -------------------------------------------------
#
ymin = eval(fname+"(xmin)")
ymax = ymin
for i in range(bins):
    x = xmin + (xmax - xmin) * float(i) / bins
    y = eval(fname+"(x)")
    if y < ymin: ymin = y
    if y > ymax: ymax = y





# Generate the random distribution
# --------------------------------
#
rvals = []
for i in range(number):
    while True:
        # Generate a random number between 0 to 1
        xr = random.random()
        yr = random.random()
        x = xmin + (xmax - xmin) * xr
        y = ymin + (ymax - ymin) * yr
        if y <= eval(fname+"(x)"):
            rvals.append(xr)
            break





# Put the random numbers in the range of values
# ---------------------------------------------
#
# Construct a numpy array from the list of random numbers. The random
# numbers computed until here are in the range 0-1. With the following
# lines, these numbers are put within the specified range with '--range'.
result_newrange = np.array(rvals) * (xmax - xmin) + xmin

# Set the output dtype
result_formated = np.array(result_newrange).astype(dtype)





# Output
# ------
#
# Depending on the specifed '--output', the random numbers will be saved
# differently: standard output, fits, or plain text.
fmt='%.'+ndecimals+'f'
extension = output.split('.')[-1]
if extension == 'stdout':
    np.savetxt(sys.stdout, result_formated, fmt=fmt)

elif extension == 'fits':
    from astropy.table import Table
    table = Table({colname: result_formated})
    table.write(output, overwrite=True)

else:
    np.savetxt(output, result_formated, fmt=fmt)





# Plot figure with useful information if needed
# ---------------------------------------------
#
# Generate a check plot where the function and the random distribution are
# showed.
if plot != None:

    # Import and define the figure (two vertical plots)
    import matplotlib.pyplot as plt

    fig = plt.figure()
    gs = fig.add_gridspec(2, hspace=0)
    axs = gs.subplots(sharex=True)

    # X and Y data for the figure
    xplot = np.linspace(xmin, xmax, bins)
    yplot = f(xplot)

    # Subplot labels and figure title
    label1='Ntot=--number=' + str(number)
    label0='--function="' + str(function) + '"'
    fig.suptitle('Function and random distribution')

    # Generate the plots
    axs[0].plot(xplot, yplot, label=label0)
    axs[1].hist(result_formated, histtype='bar', bins='sqrt',label=label1)

    # Legends, scale type, and labels
    axs[1].legend()
    axs[0].legend()

    axs[1].set_xscale(xplotscale)
    axs[0].set_xscale(xplotscale)
    axs[1].set_yscale(yplotscale)
    axs[0].set_yscale(yplotscale)

    axs[1].set_ylabel('n')
    axs[0].set_ylabel('f(--range)')
    axs[1].set_xlabel('x = --range = ' + str(xmin)+ ',' + str(xmax))

    # Save the figure
    plt.savefig(plot, bbox_inches='tight')


