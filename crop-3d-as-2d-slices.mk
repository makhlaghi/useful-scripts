# Crop a 3D dataset as multiple independent 2D datasets
#
# Invocation (two necessary environment variables):
#   - INPUT: the input FITS file.
#   - CROP_OPTIONS: the options that are passed to Crop (put options
#     in double quotations when you need more than one option).
#
# Optional environment variables.
#   - 'ALIGN_FIRST': if it has any value, each slice will be aligned
#     before cropping.
#
# Invocation example:
#   $ make -f crop-3d-as-2d-slices.mk INPUT=xxxx CROP_OPTIONS=xxxx
#
# Opertion:
#    During operation: a temporary directory called 'XXXX-slices' in
#        the running directory (where 'XXXX' is the base-name of the
#        input, without suffix or directory) and store all the
#        temporary images of each slice there.
#
#    Output name: The output will be called 'XXXX-cropped.fits' (where
#        'XXXX' is the base-name of the input, without suffix or
#        directory).
#
# MULTI-THREADED (MUCH FASTER!) OPERATION:
#   One of the main reasons this task is implemented as a Makefile, is
#   this. You can specify the number of threads to be used in parallel
#   with the '-j' option, for example, if you add '-j8' to your
#   command, it will simultaneously work on 8 threads (and be 8 times
#   faster!).
#
# NOTE: This Makefile is designed to work with GNU Make and assumes
# GNU Astronomy Utilties (Gnuastro) is already installed.
#
# Copyright (C) 2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.





# Settings from inputs.
inbase = $(basename $(notdir $(INPUT)))

# Basic Make settings.
.ONESHELL:
.SUFFIXES:
.SECONDEXPANSION:
.SHELLFLAGS = -ec
output = $(inbase)-cropped.fits
length3d = $(shell astfits $(INPUT) -h1 | awk '/NAXIS3/{print $$3}')

# Ultimate Make target
all: $(output)

# Build the individual directory (hosing the individual slice crops as
# 2D images) if not present.
indivdir = $(inbase)-slices
$(indivdir):; mkdir $@





# Crop out each slice, then do the actual crop over it.
slices = $(shell seq 1 $(length3d))
cropped-slices = $(foreach i, $(slices), $(indivdir)/$(i).fits)
$(cropped-slices): $(indivdir)/%.fits: $(INPUT) | $(indivdir)

        # First, cutout the slice as a 2D image.
	slice=$(subst .fits,-slice.fits,$@)
	astcrop $(INPUT) --mode=img --section=:,:,$*:$* --output=$$slice

        # Remove/correct the necessary headers so its treated as 2D.
	astfits $$slice --update=NAXIS,2 --update=WCSAXES,2 --delete=NAXIS3 \
	        --delete=CDELT3 --delete=CUNIT3 --delete=CTYPE3 \
	        --delete=CRVAL3 --delete=CRPIX3

        # If the user has asked to align each slice first, do it here.
	if [ x"$(ALIGN_FIRST)" = x ]; then
	  afteralign=$$slice
	else
	  afteralign=$(subst .fits,-aligned.fits,$@)
	  astwarp $$slice --align -o$$afteralign
	  rm $$slice
	fi

        # Run the main crop program and remove the intermediate file.
	astcrop $$afteralign $(CROP_OPTIONS) -o$@
	rm $$afteralign

        # For the first image, add the 3rd-dimension WCS keywords so
        # they are used in teh final cube. Note that the CUNIT and
        # CTYPE keyword values are string (and have single quotes
        # around them), so we need to manully remove the single
        # quotes.
	if [ $* = 1 ]; then
	  fullheader="$$(astfits $(INPUT) -h1)"
	  naxis=$$(echo "$$fullheader" | awk '/^NAXIS3/{print $$3}')
	  cdelt=$$(echo "$$fullheader" | awk '/^CDELT3/{print $$3}')
	  crval=$$(echo "$$fullheader" | awk '/^CRVAL3/{print $$3}')
	  crpix=$$(echo "$$fullheader" | awk '/^CRPIX3/{print $$3}')
	  cunit=$$(echo "$$fullheader" | awk '/^CUNIT3/{print $$3}' \
	                | sed -e"s/'//g")
	  ctype=$$(echo "$$fullheader" | awk '/^CTYPE3/{print $$3}' \
	                | sed -e"s/'//g")
	  astfits $@ -h1 --update=WCSAXES,3 \
	          --write=NAXIS3,$$naxis --write=CDELT3,$$cdelt \
	          --write=CUNIT3,$$cunit --write=CTYPE3,$$ctype \
	          --write=CRVAL3,$$crval --write=CRPIX3,$$crpix
	fi





# Build final target by stacking all the individual crops.
$(output): $(cropped-slices)

        # Put all the slices into one 3D cube.
	astarithmetic $^ $(length3d) add-dimension -g1 --output=$@

        # Delete the individual directory
	rm -rf $(indivdir)
