#!/bin/bash
#
# A script to perform incremental backups using 'rsync'.  An incremental
# backup stores only the data that has been changed since the previous
# backup was made, only the first backup of the series is a "full backup".
# Even if the backups are incremental, by taking a look inside each
# directory we will always see the complete set of files, not only the ones
# that changed: this is because the unchanged files will be represented by
# hard links. Those who where modified since the last backup will be the
# only ones to occupy new space on the disk.
#
# The current script implement this backup strategy by using the
# '--link-dest' option of 'rsync'. This option takes a directory as
# argument.  When invoking 'rsync' it will specify:
#  The source directories: 'inputs'.
#  The destination directory: 'output' (joined with the time-stamp).
#  The directory to be used as argument of the '--link-dest' option
#  'latest_link'
#
# The content of the source directories will be compared to that of the
# directory passed to the '--link-dest' option. New and modified files
# existing in the source directory will be copied to the destination
# directory as always (and files deleted in the source will also not appear
# in the backup if the '--delete' option is used); unchanged files will
# also appear in the backup directory, but they will just be hard links
# pointing to inodes created in the previously made backups.
#
# This script is intendeed to be executed automatically via 'crontab'. For
# example, by opening the 'crontab' file ($ crontab -e) and setting the
# line:
#
#  0 */12 * * * /path/to/backup-incremental.sh /home/user -o /backup/dir
#
# This backup script will be executed to make a backup of '/home/user' to
# '/backup/dir' each 12 hours. Note that more than one source directory can
# be specified at once.
#
#
# Copyright (C) 2021-2023 Raul Infante-Sainz <infantesainz@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.





# Exit the script in the case of failure
set -e





# Default options
quiet=""
rmsnapshots=0
nmaxsnapshots=10
version=@VERSION@
scriptname=@SCRIPT_NAME@





# Output of `--usage' and `--help':
print_usage() {
    cat <<EOF
$scriptname: run with '--help' for list of options
EOF
}

print_help() {
    cat <<EOF
Usage: $scriptname [OPTION] dir1/to/be/saved dir2/to/be/saved --output=/backup/path

This script uses 'rsync' to make incremental backups of the directores
provided as inputs into the directory specified as output.

$scriptname options:
 Input:
  Directories to be backuped

 Output:
  -r, --rmsnapshots       Remove older snapshots.
  -n, --nmaxsnapshots     Maximum number of snapshots in the destination.
  -o, --output            Absolute path to make the backup of the inputs.

 Operating mode:
  -h, --help              Print this help list.
  -q, --quiet             Don't print the list.
  -V, --version           Print program version.

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

EOF
}





# Output of `--version':
print_version() {
    cat <<EOF
$scriptname (GNU Astronomy Utilities) $version
Copyright (C) 2021, Free Software Foundation, Inc.
License GPLv3+: GNU General public license version 3 or later.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written/developed by Raul Infante-Sainz
EOF
}





# Functions to check option values and complain if necessary.
on_off_option_error() {
    if [ "x$2" = x ]; then
        echo "$scriptname: '$1' doesn't take any values."
    else
        echo "$scriptname: '$1' (or '$2') doesn't take any values."
    fi
    exit 1
}

check_v() {
    if [ x"$2" = x ]; then
        echo "$scriptname: option '$1' requires an argument."
        echo "Try '$scriptname --help' for more information."
        exit 1;
    fi
}





# Separate command-line arguments from options. Then put the option
# value into the respective variable.
#
# OPTIONS WITH A VALUE:
#
#   Each option has three lines because we want to all common formats: for
#   long option names: `--longname value' and `--longname=value'. For short
#   option names we want `-l value', `-l=value' and `-lvalue' (where `-l'
#   is the short version of the hypothetical `--longname' option).
#
#   The first case (with a space between the name and value) is two
#   command-line arguments. So, we'll need to shift it two times. The
#   latter two cases are a single command-line argument, so we just need to
#   "shift" the counter by one. IMPORTANT NOTE: the ORDER OF THE LATTER TWO
#   cases matters: `-h*' should be checked only when we are sure that its
#   not `-h=*').
#
# OPTIONS WITH NO VALUE (ON-OFF OPTIONS)
#
#   For these, we just want the two forms of `--longname' or `-l'. Nothing
#   else. So if an equal sign is given we should definitely crash and also,
#   if a value is appended to the short format it should crash. So in the
#   second test for these (`-l*') will account for both the case where we
#   have an equal sign and where we don't.
while [ $# -gt 0 ]
do
    case "$1" in
        -n|--nmaxsnapshots)      nmaxsnapshots="$2";                          check_v "$1" "$nmaxsnapshots"; shift;shift;;
        -n=*|--nmaxsnapshots=*)  nmaxsnapshots="${1#*=}";                     check_v "$1" "$nmaxsnapshots"; shift;;
        -n*)                     nmaxsnapshots=$(echo "$1" | sed -e's/-n//'); check_v "$1" "$nmaxsnapshots"; shift;;

        # Output parameters.
        -r|--rmsnapshots)     rmsnapshots=1; shift;;
        -r*|--rmsnapshots=*)  on_off_option_error --rmsnapshots -r;;
        -o|--output)          output="$2";                          check_v "$1" "$output"; shift;shift;;
        -o=*|--output=*)      output="${1#*=}";                     check_v "$1" "$output"; shift;;
        -o*)                  output=$(echo "$1" | sed -e's/-o//'); check_v "$1" "$output"; shift;;

        # Non-operating options.
        -q|--quiet)       quiet="--quiet"; shift;;
        -q*|--quiet=*)    on_off_option_error --quiet -q;;
        -?|--help)        print_help; exit 0;;
        -'?'*|--help=*)   on_off_option_error --help -?;;
        -V|--version)     print_version; exit 0;;
        -V*|--version=*)  on_off_option_error --version -V;;

        # Unrecognized option:
        -*) echo "$scriptname: unknown option '$1'"; exit 1;;

	# Not an option (not starting with a `-'): assumed to be directory
	# names.
        *) inputs="$inputs $1"; shift;;
    esac
done





# Basic sanity checks
# -------------------
#
# If no input is given, let the user know.
if [ x"$inputs" = x ]; then
    echo "$scriptname: no inputs given."
    echo "Run with '--help' for more information on how to run."
    exit 1
fi

# If no output is given, let the user know.
if [ x"$output" = x ]; then
    echo "$scriptname: no output directory given."
    echo "Run with '--help' for more information on how to run."
    exit 1
fi





# Define directories and sub-directories
# --------------------------------------
#
# Each backup will be saved into a subdirectory of the provided directory
# (with '--output') with the date and time as the name (YYYYMMDD-HHMMSS).
# The 'latest' directory is a symbolic link to the latest backup.
datetime="$(date '+%Y%m%d-%H%M%S')"
backup_path="$output/snapshot_$datetime"
latest_link="$output/latest"

# Create the output directories if they don't exist
mkdir -p "$output"
mkdir -p "$latest_link"




# Make the backup using 'rsync'
# -----------------------------
#
# The directories given as 'inputs' are the directores which will be
# backuped. 'output' directory contains the absolute path to the directory
# where all the backups will be stored. 'backup_path' is the absolute path
# of the backup directory obtained by joining 'output' and the current
# 'datetime'.  Finally, the 'latest_link' variable contains the path of the
# symbolic link which will always point to the latest backup. The
# '--delete' option makes sure that files deleted from source are also
# deleted on destination.

# The first time this script is launched with the '--link-dest
# "$latest_link"', this directory will not exist. This will not generate an
# error, but will cause a full backup. After the command is successfully
# executed, the link pointing to the previous backup is removed, and
# another one with the same name, pointing to the new backup is created.

# In short:

# --quiet: run in quiet or verbose mode.
# --stats: print statistics.
# --delete: files deleted from source are also deleted on destination.
# --info=progress2: print overall progress info. Not use this option
#                   together with '-v' or '--verbose'.
# --human-readable: sizes in human-friendly format (Mb, Gb, etc.).
# --archive: several options together to preserve most important files
#            metadata.
rsync $quiet \
      --stats \
      --delete \
      --human-readable \
      --info=progress2 \
      --archive $inputs \
      --link-dest "$latest_link" \
      "$backup_path"





# Update 'latest' folder to be the latest backup
# ----------------------------------------------
#
# After the above command has been successfully executed, the link pointing to
# the previous backup is removed, and another one with the same name,
# pointing to the new backup is created.
rm -rf "$latest_link"
ln -s "$backup_path" "$latest_link"





# Remove older snapshots
# -----------------------
#
# In order to not have an infinite number of snapshots and fill the output
# directory completely, older snapshots are removed. Only a maximum number
# of snapshots will remain, the older ones are removed.
ntotal_snapshots=$(ls -d $output/snapshot* | wc -l)
nexcess_snapshots=$(($ntotal_snapshots - $nmaxsnapshots))
if [ x"$rmsnapshots" = x1 ]; then
    if [ $ntotal_snapshots -gt $nmaxsnapshots ]; then
        oldest_snapshots=$(ls -d $output/snapshot* \
                              | head -$nexcess_snapshots)
        echo " "
        echo "Removing the following $nexcess_snapshots snapshots:"
        echo "$oldest_snapshots"
	rm -rf $oldest_snapshots
    else
        echo " "
        echo "Not removing old snapshots"
    fi
fi


