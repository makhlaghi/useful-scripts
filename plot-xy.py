#!/usr/bin/env python3

# Plot y against x from the input files
#
# This is a Python script to obtain xy plots quickly. It is not a
# sofisticated script for making beautiful plots, but a tool for plotting
# data quickly and get feeling from the data.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2023, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


# Usage
# -----
#
# $ python3 plot-xy.py
#           --pattern=*profile.fits
#           --xscale=log --yscale=log
#           --xcol=0 --ycol=1 --output=figure.pdf"


# Import modules
import sys
import glob
import argparse
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt


# Read input arguments from the command line
# ------------------------------------------
#
parser = argparse.ArgumentParser(description='Arguments from the command line')
# By default, argparse uses -h, --help as arguments to provide help. To be
# able to supress this behaviour and use -h for the extension number (as
# all Gnuastro programs do), it is necessary to disable this option. Here,
# the options to obtain help are: -H, --help
parser = argparse.ArgumentParser(add_help=False)

usage="""Example:
         $ python3 plot-xy.py
                   --pattern=*profile.fits
                   --xscale=log --yscale=log
                   --xcol=0 --ycol=1 --output=figure.pdf

         If:   OSError: [Errno 24] Too many open files
         Try:  ulimit -n 2048 (or similar)
      """

parser.add_argument('-H', '--help', action='help', default=argparse.SUPPRESS,
                    help=usage)

parser.add_argument('-h', '--hdu', type=int, default=1,
                    help='HDU number. Default is: 1')

parser.add_argument('-p', '--pattern', type=str, default=None,
                    help='Pattern to read the input files.')

parser.add_argument('-a', '--alpha', type=float, default=0.7,
                    help='Alpha or transparency. Default is: 0.7')

parser.add_argument('-o', '--output', type=str, default='figure.pdf',
                    help='Output figure name. Default is: figure.pdf')

parser.add_argument('-x', '--xcol', type=int, default=0,
                    help='X column number (starting from 0). Default is: 0')

parser.add_argument('-y', '--ycol', type=int, default=1,
                    help='Y column number (starting from 0). Default is: 1')

parser.add_argument('-f', '--fmt', type=str, default='-',
                    help="Format of the plot (o,.,-,etc. Default is line: -")

parser.add_argument('-X', '--xscale', type=str, default='linear',
                    help="X axis scale (linear, log, etc. Default is: linear")

parser.add_argument('-Y', '--yscale', type=str, default='linear',
                    help="Y axis scale (linear, log, etc. Default is: linear")





# Define variables from command line arguments
args = parser.parse_args()
fmt = args.fmt
hdu = args.hdu
xcol = args.xcol
ycol = args.ycol
alpha = args.alpha
output = args.output
xscale = args.xscale
yscale = args.yscale
pattern = args.pattern


if pattern is None:
    sys.exit("No input files provided with the option -p or --pattern. Please run with --help for more information.")


# Read the files
files = glob.glob(pattern)
plt.clf()
plt.ion()


# For each file, read the array and plot y against x
for file in files:
    hdu_list = fits.open(file)
    array = hdu_list[hdu].data
    x = array[array.names[xcol]]
    y = array[array.names[ycol]]
    plt.plot(x, y, fmt, label=file, alpha=alpha)


# Plot parameters
plt.grid()
plt.show()
plt.xlabel('x')
plt.ylabel('y')
plt.xscale(xscale)
plt.yscale(yscale)

# Plot the legends
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.tight_layout()

plt.savefig(output)
print("Output written to","'"+output+"'.")


