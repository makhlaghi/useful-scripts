# This Makefile will create a stack of flat-field images that are in the
# space-separated list of files given to 'INPUTS'. The final output will be
# the scaled stacked image ('stack.fits' in the directory given to 'BDIR').
#
# Usage:
#
#   - Before executing this Makefile, set the input and build (output)
#     directories:
#
#     - INPUTS: This is the list of input files that will be used,
#       they can be anywhere within your filesystem, it is just
#       important that the files do not have the same file name.
#
#     - INPUTHDU: The HDU name/number to be used for all the input
#       files.
#
#     - BDIR: This is the build or output directory where all
#       intermediate files are placed. You can set this to any
#       directory on your file system.
#
#     - OUTPUT: Name of output file. If not given, the output will be
#       'stack.fits' within the build directory.
#
#     - NUMTHREADS: Number of threads to use in each Gnuastro call
#       (this is independent of the value given to Make's '-j'
#       option).
#
#     - STATISTICS_OPTIONS [OPTIONAL]: Any option that you would like
#       to pass to Gnuastro's Statistics program for measuring the
#       "sky" (which is actually the reference for scaling the
#       different flat flields). The most common option is when the
#       amplifier footprints of your bias-subtracted images still
#       remain and you want the tiles created by Statistics to not
#       pass over amplifiers. In this case, you would use the
#       '--numchannels' option of Statistics.
#
#     - MASKFILL_MULTIP [OPTIONAL]: The arithmetic masked-filling multiple
#       of the MAD that you would like to use for the stacking. See the
#       code below for the default operator (if this environment variable
#       is not defined).
#
#     - KEEPMASK [OPTIONAL]: Mostly useful for debuging purposes: if
#       this variable has any value (is not an empty string), a
#       seperate multi-HDU FITS image called 'maskfilled.fits' will be
#       created in the build directory, containing all the normalized
#       images but with outliers (compared to other images) masked.
#
#     - CLIP_MULTIP [OPTIONAL]: The arithmetic sigma-clipping multiple of
#       the standard deviation that you would like to use for the
#       stacking. See the code below for the default operator (if this
#       environment variable is not defined).
#
#     - NO_ERROR [OPTIONAL]: Do not add a HDU showing the error of each
#       pixel in the stack.
#
#     - NORMALIZATION_QUANTILE [OPTIONAL]: the quantile of the final stack
#       that you want to normalize (set to a value of 1) (if not given, no
#       normalization will take place). TIP: do not set this to 1.0 because
#       the maximum usually has a high scatter. Something like 0.98 would
#       be good (if the image is very large, 0.99 would also be fine): you
#       don't want random noise scatter to affect the result.
#
#     - STACK_LARGE_MK [OPTIONAL]: the location of the Makefile for
#       stacking many large files. It is available here:
#       https://gitlab.cefca.es/gnuastro/scripts/-/blob/master/stack-large.mk
#
#     - LIBGNUASTRO_MAKE [mandatory with STACK_LARGE_MK]: The full
#       absolute address of Gnuastro's Makefile extensions:
#       'libgnuastro_make.so'.
#
#     - STACK_LARGE_CROPNUM [mandatory with STACK_LARGE_MK]: the
#       number of crops along both dimension to crop the images when
#       stacking. Only one number is expected, and it will be used for
#       both dimensions. For example if given a value of 3, the images
#       will be cropped into 3x3=9 sub-images for stacking. You can
#       try with smaller values (maybe 3 or 4), and see if
#       memory-mapping warning is printed by 'astarithmetic' along
#       with the creation of a 'gnuastro_mmap' directory.
#       - If memory mapping has happened, use a larger number so more
#         crops are made (consuming less RAM for each stack) until you
#         see that no memory-mapping has happened.
#       - If no memory mapping has happened, you can decrease the
#         value to make fewer crops and be faster.
#
#   - After setting the directories above, to execute this Makefile,
#     simply run 'make' like below:
#
#        export BDIR=/path/to/build
#        export INPUTS="/path/to/file-1.fits /other/path/file-2.fits ..."
#        make -f 2009-flat-master-scale.mk
#
#     You can also set the 'INPUTS' and 'BDIR' variables from the
#     command-line. In this case, the command-line values will take
#     precedence over the values within the Makefile. Like below:
#
#        make -f 2009-flat-master-scale.mk \
#                BDIR=/path/to/build \
#                INPUTS="/path/to/file-1.fits /other/path/file-2.fits ..."
#
#   - To execute it on many threads use the '-j' (for "jobs") option
#     of Make). For example '-j12' will execute on 12 threads.
#
# Output:
#
#   - The output of this script (value to 'OUTPUT' variable above) has
#     to be a FITS file. It will contain two HDUs with data: the first
#     will be the stacked image and the second will be the numbers
#     image (or exposure map).
#
#   - The 0-th HDU of the output has all the necessary metadata,
#     including:
#     - IN-NUM: Number of images used to build the stack
#     - IN-*: file name of each input image.
#     - IN-S-*: scale factor that is multiplied to that input image.
#
# Copyright (C) 2023-2024 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# For a copy of the GNU General Public License see
# <http://www.gnu.org/licenses/>.





# First target (print a message if the necessary options are are empty).
ifeq ($(strip $(INPUTS)),)
all:
	@echo "flat-stack.mk: 'INPUTS' is empty!"; exit 1
else
ifeq ($(words $(INPUTHDU) $(OUTPUT) $(BDIR)),3)
all: $(OUTPUT)
else
all:
	@printf "flat-stack.mk: t least one of 'INPUTHDU', "
	printf "'OUTPUT' or 'BDIR' are missing.\n"; exit 1
endif
endif





# Internal Make settings
# ----------------------
#
# These are just basic Make settings to make sure things run smoothly.
.ONESHELL:
.SECONDEXPANSION:
.SHELLFLAGS = -ec
$(BDIR):; mkdir $@





# Number of threads to use in Gnuastro's programs.
ifeq ($(strip $(NUMTHREADS)),)
nt =
else
nt = "--numthreads=$(NUMTHREADS)"
endif





# Associate each input file with a number in the build directory. We
# will create a plain-text file containing the full name of the input,
# but numbered based on its position. In the process, we will also
# make sure
ids := $(shell seq $$(echo $(words $(INPUTS))))
files := $(foreach i, $(ids), $(BDIR)/$(i)-file.txt)
$(files): $(BDIR)/%-file.txt: | $(BDIR)
	name=$(word $*, $(INPUTS))
	echo astfits $(nt) $$name
	if astfits $(nt) $$name &> /dev/null; then
	  echo $$name > $@
	else
	  echo "ERROR: '$$name' does not exist or is not a FITS file"
	  exit 1
	fi





# Generate the sky image on each image using Gnuastro's Statistics
# program. To avoid systematics caused by the interpolation,
sky := $(foreach i,$(ids),$(BDIR)/$(i)-sky.fits)
$(sky): $(BDIR)/%-sky.fits: $(BDIR)/%-file.txt

#	FOr easy commands.
	infits=$$(cat $(BDIR)/$*-file.txt)

#	We are adding '|| true' so even if Statistics fails because of
#	a lack of tiles, the command passes successfully (and doesn't
#	abort the Makefile): we do not need it to complete.
	statout=$(subst .fits,_sky.fits,$@)
	skysteps=$(subst .fits,_sky_steps.fits,$@)
	aststatistics $$infits -h$(INPUTHDU) --sky $(nt) \
	              -o$(BDIR)/$*-sky.fits --oneelempertile \
	              $(STATISTICS_OPTIONS) --checkskynointerp \
	              2> /dev/null || true

#	In case the previous command didn't succeed to make its output,
#	re-run it without redirecting its error to /dev/null, so its error
#	message is printed; but keep the '|| true' so it doesn't abort the
#	Makefile.
	if ! [ -f $$skysteps ]; then
	  aststatistics $$infits -h$(INPUTHDU) --sky $(nt) \
	                -o$(BDIR)/$*-sky.fits --oneelempertile \
	                $$chopt --checkskynointerp || true
	fi

#	Only keep the 'VALUE1_NO_OUTLIER' HDU; it contains the good
#	tiles (without the outliers); but before interpolation.
	astfits $$skysteps --copy=VALUE1_NO_OUTLIER $(nt) -o$@

#	Clean up. If 'aststatistics' breaks, its output will not
#	exist, so should only attempt to delete it when it does exist.
	if [ -f $$statout ]; then rm $$statout; fi
	rm $$skysteps





# Number of good tiles (to reject bad flat fields)
stat = $(BDIR)/statistics.txt
statindiv=$(subst .txt,-individual.txt,$(stat))
$(stat): $(sky)

#	Get the basic information of each sky image.
	echo "# Column 1: ID  [counter, u16] ID of image."    \
	     > $(statindiv)
	echo "# Column 2: NUM [counter, u32] Num good tiles." \
	     >> $(statindiv)
	for f in $(sky); do
	  n=$$(aststatistics $$f --number $(nt))
	  i=$$(echo $$f | sed -e's|$(BDIR)/||' -e's|-sky.fits||')
	  echo $$i $$n
	done >> $(statindiv)

#	Find the one with the largest number of good tiles (this will
#	become the reference image
	refid=$$(asttable $(statindiv) $(nt) --sort=NUM --tail=1 -cID)

#	Find the sigma-clipped mean and median to remove outliers.
	ms=$$(aststatistics $(statindiv) -cNUM --sigclip-median \
	                    --sigclip-std $(nt))

#	Write the values into a final table:
	echo "# Column 1: REF-ID  [counter, u16] ID of image." > $@.txt
	echo "# Column 2: MED-NUM [counter, f32] Median of num tiles." \
	     >> $@.txt
	echo "# Column 3: STD-NUM [counter, f32] STD of num tiles." \
	     >> $@.txt
	echo $$refid $$ms >> $@.txt

#	Rename the temporary target to the final target.
	mv $@.txt $@





# Match the sky level in all the images to the first one.
matched := $(foreach i,$(ids),$(BDIR)/$(i)-matched.fits)
$(matched): $(BDIR)/%-matched.fits: $(stat)

#	Extract the necessary information from the statistics.
	refid=$$(asttable $(stat) -cREF-ID)
	med=$$(asttable $(stat)   -cMED-NUM)
	std=$$(asttable $(stat)   -cSTD-NUM)

#	No need to match the reference image (only put the 0th
#	extension into the first extension to be similar to the rest).
	infits=$$(cat $(BDIR)/$*-file.txt)
	if [ $* == $$refid ]; then
	  astarithmetic $$infits -h$(INPUTHDU) float32 $(nt) --output=$@
	  astfits $@ --write=/,Processing --write=STATUS,1 $(nt) \
	          --write=SCALE,1.0
	else
#	  See if the this image's number of good tiles is within the
#	  acceptable range; otherwise, the image will be too crowded
#	  and it is better to not use it at all).
	  check=$$(asttable $(statindiv) --equal=ID,$* $(nt) -cNUM \
	                    | awk -vm=$$med -vs=$$std \
	                          -vM=5 \
	                         '$$1 > m-(s*M) && $$1 < m+(s*M) \
	                          {print "good"}')

# 	  Continue with the normalization if the check is good.
	  if [ x$$check = xgood ]; then
#	    Divide the reference image's sky by this image, to get the
#	    necessary scaling factor on a per-tile basis. Note that:
#           - Each tile is relatively small so this measurement has a
#	      low signal-to-noise ratio individually. Hence we later
#	      calculate the actual value to multiply by the
#	      sigma-clipped median for all the tiles.
#           - We need to divide (and not subtract), because this is a
#	      flat field. You can see from the "div" output if you
#	      substitute division by subtraction: the subtraction will
#	      keep the amplifier footprints, but they will be removed in
#	      the division.
	    sky=$(BDIR)/$*-sky.fits
	    refsky=$(BDIR)/$$refid-sky.fits
	    div=$(subst .fits,-div.fits,$@)
	    astarithmetic $$refsky $$sky / -g1 --output=$$div

#	    To increase the signal-to-noise ratio of the constant to
#	    multiply by, we will take the sigma-clipped median; then
#	    multiply it.
	    scale=$$(aststatistics $$div --sigclip-median $(nt))
	    astarithmetic $$infits -h$(INPUTHDU) $$scale x float32 \
	                  $(nt) --output=$@

#	    Add the status and scale factor value.
	    astfits $@ --write=/,Processing --write=STATUS,1 $(nt) \
	            --write=SCALE,$$scale

#	  The check was not good so the normalized image should be a fully
#	  NaN valued image (so it is ignored in the stacking). To do this,
#	  we'll just add it with NaN.
	  else
	    astarithmetic $$infits -h$(INPUTHDU) nan + float32 $(nt) -o$@
	    astfits $@ --write=/,Processing --write=STATUS,0 $(nt)
	  fi
	fi





# Stack the matched images.
$(OUTPUT): $(matched)

#	Get the file names of the final images that were used.
	list=""
	inkeyn=""
	inkeys=""
	counter=0
	namec="Input image file name."
	scalec="Scale factor of this image."
	for i in $(ids); do
	  check=$$(astfits $(BDIR)/$$i-matched.fits --keyvalue=STATUS \
	                   --quiet $(nt))
	  if [ $$check = 1 ]; then

#	    Raw values.
	    counter=$$(echo $$counter | awk '{print $$1+1}')
	    scale=$$(astfits $(BDIR)/$$i-matched.fits --keyvalue=SCALE \
	                     --quiet $(nt))
	    fname=$$(echo "$$(cat $(BDIR)/$$i-file.txt)")

#	    In case the file name is longer than 68 characters, truncate
#	    the ending, see the "Shell tips" section of the Gnuastro book.
	    if [ $${#fname} -gt 68 ]; then wname="...$${fname: -65}"
	    else                          wname=$$fname
	    fi

#	    Put in list to use.
	    list="$$list $(BDIR)/$$i-matched.fits"
	    inkeyn="$$inkeyn --write=IN-$$counter,$$wname"
	    inkeys="$$inkeys --write=IN-S-$$counter,$$scale"
	  fi
	done

#	--------------------------------------------------------------
#	For debugging: you can crop the same pixels from the
#	'*-matched.fits' with a command like this below and uncomment
#	the line below: 'for i in *-matched.fits; do astcrop $i
#	--mode=img --section=3469:4548,4648:6196; done'
#	--------------------------------------------------------------
#	list=$$(ls $(BDIR)/*-matched_cropped.fits)

#	Set the clipping operator and its multiple.
	if [ x"$(MASKFILL_MULTIP)" = x ]; then mfmultip=4
	else                                   mfmultip=$(MASKFILL_MULTIP)
	fi
	if [ x"$(CLIP_MULTIP)" = x ]; then clipmultip=4
	else                               clipmultip=$(CLIP_MULTIP)
	fi

#	If the user asked to keep the masked images, make them here.
	if ! [ x"$(KEEPMASK)" = x ]; then
	  astarithmetic $$list -g1 $(nt) --output=$(BDIR)/maskfilled.fits \
	                $$counter $$mfmultip 0.01 madclip-maskfilled \
	                --writeall
	fi

#	Stack all the images.
	stackraw=$(BDIR)/stack-raw.fits
	if [ x"$(STACK_LARGE_MK)" = x ]; then
	  astarithmetic $$list -g1 $(nt) \
	                $$counter $$mfmultip   0.01 madclip-maskfilled \
	                $$counter $$clipmultip 0.1  sigclip-median \
	                --writeall --output=$$stackraw
	else

#	  Make sure that LIBGNUASTRO_MAKE and 'STACK_LARGE_CROPNUM'
#	  are given:
	  if [ x"$(LIBGNUASTRO_MAKE)" = x ]; then
	    echo "ERROR: no 'LIBGNUASTRO_MAKE' environment variable";
	    exit 1
	  fi
	  if [ x"$(STACK_LARGE_CROPNUM)" = x ]; then
	    echo "ERROR: no 'STACK_LARGE_CROPNUM' environment variable";
	    exit 1
	  fi

#	  Run the stack-large Makefile and then delete its BDIR.
	  $(MAKE) MAKEFLAGS= -f $(STACK_LARGE_MK) \
	          INPUTS="$$list" \
	          INPUTHDU=1 \
	          OUTPUT=$$stackraw \
	          BDIR=$(BDIR)/stack-large \
	          LIBGNUASTRO_MAKE=$(LIBGNUASTRO_MAKE) \
	          CROP_NUMBER_D1=$(STACK_LARGE_CROPNUM) \
	          CROP_NUMBER_D2=$(STACK_LARGE_CROPNUM) \
	          STACK_OPERATOR="$$clipmultip 0.1 sigclip-median" \
	          MASK_OPERATOR="$$mfmultip 0.01 madclip-maskfilled"

#	  Clean up.
	  rm -r $(BDIR)/stack-large
	fi

#	Having the clean stack, we can now normalize the flat to have a
#	value of one.
	normval=1.0
	normquan=1.0
	out=$(BDIR)/output.fits
	if ! [ x"$(NORMALIZATION_QUANTILE)" = x ]; then
	  normquant=$(NORMALIZATION_QUANTILE)
	  normval=$$(aststatistics $$stackraw $(nt) \
	                           --quantile=$(NORMALIZATION_QUANTILE))
	  astarithmetic $$stackraw $$normval / --outfitsnoconfig \
	                --wcsfile=none --metaname=FLAT --metaunit=FRACTION \
	                $(nt) --output=$$out
	else
	  mv $$stackraw $$out
	fi

#	Include the variance of the stack as the second extension of the
#	output (in case the use wanted it).
	if [ x"$(NO_ERROR)" = x ]; then
#	  DO NOT FORGET: include the normalization factor in error.
	  printf "ERROR: the statistical error measurement is not yet "
	  printf "implemented\n"; exit 1
	fi

#	Add metadata on 0th HDU.
	astfits $$out -h0 \
	        --write=EXTNAME,METADATA,"Name of the extension/HDU." \
	        --write=/,"Images used for this stack" \
	        --write=IN-NUM,$$counter $$inkeyn $$inkeys \
	        --write=/,"Input parameters" \
	        --write=FILLMULT,$$mfmultip,"Multiple of MAD in maskfilled." \
	        --write=CLIPMULT,$$clipmultip,"Multiple of sigma for clip." \
	        --write=NORMQ,$$normquant,"Quantile for normalization. " \
	        --write=NORMVAL,$$normval,"Normalization value (to have ~max=1)."

#	Copy the numbers image into the output.
	astfits $$stackraw --copy=2 $(nt) --output=$$out
	astfits $$out --hdu=2 $(nt) \
	        --write=EXTNAME,NUM-EXPOSURES,"Name of the extension/HDU." \
	        --write=BUNIT,COUNTS,"Unit of the pixel values."

#	Move the output.fits in the build directory to the user's desired
#	location. This is done so if any errors happen in the final stages
#	above (after '$$out' has been created), the final output is not
#	created and Make re-runs this rule later.
	mv $$out $@
