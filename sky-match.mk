# This Makefile will match the sky level between all the input
# images. The output is a table containing the value that should be
# added to each image so the background value is matched in all of
# them.
#
# Usage:
#
#   - Before executing this Makefile, set the input and build (output)
#     directories:
#
#     - INPUTS: This is the list of input files that will be used,
#       they can be anywhere within your filesystem, it is just
#       important that the files do not have the same file name.
#
#     - INPUTHDU: The HDU name/number to be used for all the input
#       files.
#
#     - BDIR: This is the build or output directory where all
#       intermediate files are placed. You can set this to any
#       directory on your file system.
#
#     - OUTPUT: Output file: name of table containing the value to add
#       to each image so that its background is matched to the rest.
#
#     - GRID_RADIUS_ARCSEC: the radius of circles used to measure the sky
#       value on a grid.
#
#     - GRID_SPACING_ARCSEC: the spacing of each RA/Dec grid circle on the
#       sky.
#
#     - NOISECHISEL_OPTIONS [OPTIONAL]: NoiseChisel options.
#
#     - MINIMUM_AREA [OPTIONAL]: Minimum area of usable aperture for
#       matching.
#
#   - After setting the directories above, to execute this Makefile,
#     simply run 'make' like below:
#
#        export BDIR=/path/to/build
#        export INPUTS="/path/to/file-1.fits /other/path/file-2.fits ..."
#        make -f 2009-flat-master-scale.mk
#
#     You can also set the 'INPUTS' and 'BDIR' variables from the
#     command-line. In this case, the command-line values will take
#     precedence over the values within the Makefile. Like below:
#
#        make -f 2009-flat-master-scale.mk \
#                BDIR=/path/to/build \
#                INPUTS="/path/to/file-1.fits /other/path/file-2.fits ..."
#
#   - To execute it on many threads use the '-j' (for "jobs") option
#     of Make). For example '-j12' will execute on 12 threads.
#
# Copyright (C) 2023-2024 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# For a copy of the GNU General Public License see
# <http://www.gnu.org/licenses/>.





# First target (print a message if the necessary options are are empty).
ifeq ($(strip $(INPUTS)),)
all:
	@echo "sky-match.mk: 'INPUTS' is empty!"; exit 1
else
ifeq ($(words $(INPUTHDU) $(OUTPUT) $(BDIR) $(GRID_RADIUS_ARCSEC) \
              $(GRID_SPACING_ARCSEC)),5)
all: $(OUTPUT)
else
all:
	@printf "sky-match.mk: at least one of the following "
	printf "mandatory input environment variables are missing "
	printf "'INPUTHDU', 'OUTPUT', 'BDIR', 'GRID_RADIUS_ARCSEC', "
	printf "'GRID_SPACING_ARCSEC'\n"; exit 1
endif
endif





# Internal Make settings
# ----------------------
#
# These are just basic Make settings to make sure things run smoothly.
.ONESHELL:
.SECONDEXPANSION:
.SHELLFLAGS = -ec
$(BDIR):; mkdir $@





# Identifier for each input
# -------------------------
#
# Associate each input file with a number in the build directory. We will
# create a plain-text file containing the full name of the input.
ids := $(shell seq $$(echo $(words $(INPUTS))))
files := $(foreach i, $(ids), $(BDIR)/$(i)-file.txt)
$(files): $(BDIR)/%-file.txt: | $(BDIR)
	name=$(word $*, $(INPUTS))
	if astfits $$name &> /dev/null; then
	  echo $$name > $@
	else
	  echo "ERROR: '$$name' does not exist or is not a FITS file"
	  exit 1
	fi





# Grid of aperture centers
# ------------------------
#
# This grid of apertures is defined in the RA/Dec space (different
# per-pixel positions on each input exposure).
grid-table=$(BDIR)/grid-table.fits
$(grid-table): $(INPUTS) | $(BDIR)

#	Get the minimnum and maximum position of each exposure.
	gmn=$(subst .fits,-minmax.txt,$@)
	for i in $(INPUTS); do
	  astfits $$i --skycoverage -q | awk 'NR==2'
	done > $$gmn

#	Get the extrema
	minr=$$(aststatistics $$gmn -c1 --minimum)
	maxr=$$(aststatistics $$gmn -c2 --maximum)
	mind=$$(aststatistics $$gmn -c3 --minimum)
	maxd=$$(aststatistics $$gmn -c4 --maximum)
	rm $$gmn

#	Add metadata to make an optimal FITS table (with good types).
	grid=$(subst .fits,.txt,$@)
	echo "# Column 1:  ID     [counter, u32] ID of this profile" > $$grid
	echo "# Column 2:  RA     [deg,     f32] Right Ascension" >> $$grid
	echo "# Column 3:  DEC    [deg,     f32] Declination" >> $$grid
	echo "# Column 4:  PROF   [counter,  u8] Profile identifier" >> $$grid
	echo "# Column 5:  RADIUS [counter, f32] Radius of profile" >> $$grid
	echo "# Column 6:  PARAM  [n/a,     f32] Not relevant here" >> $$grid
	echo "# Column 7:  PA     [deg,     f32] Position angle" >> $$grid
	echo "# Column 8:  AXISR  [frac,    f32] Axis ratio" >> $$grid
	echo "# Column 9:  VALUE  [flux,    f32] Pixel value" >> $$grid
	echo "# Column 10: TRUNC  [frac,    f32] Truncation" >> $$grid

#	Build the grid.
	pscaledeg=$$(astfits $(word 1, $(INPUTS)) --pixelscale --quiet \
	                     | awk '{print $$1}')
	pscale=$$(echo $$pscaledeg | awk '{print $$1*3600}')
	rad=$$(echo $$pscale $(GRID_RADIUS_ARCSEC) \
	            | awk '{print $$1*$$2}')
	echo $$rad \
	     | awk 'BEGIN{sp=$(GRID_SPACING_ARCSEC)/3600} \
	            { for(r='$$minr'; r<'$$maxr'; r+=sp) \
	               for(d='$$mind'; d<'$$maxd'; d+=sp) \
	                print ++c, r, d, 5, '$$rad', 0, 0, 1, c, 1}' \
	     >> $$grid

#	Write the input pixel scale into a file.
	echo "$$pscaledeg" > $(BDIR)/pixel-scale.txt

#	Build the final output.
	asttable $$grid --output=$@
	rm $$grid





# Catalog of each aperture
# ------------------------
#
# Measurements of the grid apertures over each input image.
cat=$(foreach i,$(ids),$(BDIR)/$(i)-cat.fits)
$(cat): $(BDIR)/%-cat.fits: $(BDIR)/%-file.txt $(grid-table)

#	Build the apertures image for measurements
	input=$$(cat $(BDIR)/$*-file.txt)
	apers=$(subst .fits,-aper.fits,$@)
	astmkprof $(grid-table) --background=$$input --clearcanvas \
	          --type=uint32 --mode=wcs --mforflatpix \
	          --output=$$apers

#	Run NoiseChisel to avoid using detected regions:
	nc=$(subst .fits,-nc.fits,$@)
	astnoisechisel $$input --output=$$nc \
	               $(NOISECHISEL_OPTIONS)

#	Mask all the detections over the input.
	masked=$(subst .fits,-masked.fits,$@)
	astarithmetic $$input -h1 $$nc -hDETECTIONS \
	              nan where --output=$$masked

#	Generate the catalogs for this image.
	astmkcatalog $$apers -h1 --valuesfile=$$input \
	             --valueshdu=1 --ids --area --sigclip-median \
	             --std -o$@

#	Clean up
	rm $$apers $$masked $$nc





# Reference input
# ---------------
#
# This is the input with the most non-blank elements.
refinput = $(BDIR)/reference-input.txt
$(refinput): $(cat)
	for f in $(ids); do
	  n=$$(asttable $(BDIR)/$$f-cat.fits --noblank=_all | wc -l)
	  echo "$$f $$n"
	done | asttable --sort=2 -c1 -Afixed -B0 --tail=1 > $@





# Using the first catalog as reference, scale all the rest to the same
# flux level.
add=$(foreach i,$(ids),$(BDIR)/$(i)-add.txt)
$(add): $(BDIR)/%-add.txt: $(refinput)

#	In case this is the reference image, just set the value to 0.0
#	(since this is to be added) and abort. Otherwise, continue.
	refid=$$(cat $(refinput))
	if [ $* = $$refid ]; then echo "0.0" > $@; exit 0; fi

#	Match the two tables by their ID. We can't use the row
#	position because the maximum label within different images can
#	be different.
	matched=$(subst .txt,-matched.fits,$@)
	astmatch $(BDIR)/$$refid-cat.fits $(BDIR)/$*-cat.fits \
	         --ccol1=OBJ_ID --ccol2=OBJ_ID --aperture=0.2 \
	         --outcols=aAREA,bAREA,aSIGCLIP_MEDIAN,bSIGCLIP_MEDIAN \
	         --output=$$matched

#	Find the factor: only use those that have the similar area (with a
#	maximum allowable area difference of 2 pixels).
	if [ x"$(MINIMUM_AREA)" = x ]; then minareaopt=""
	else minareaopt="--range=1,$(MINIMUM_AREA),inf";
	fi
	asttable -c1,'arith $$1 $$2 - abs' $$matched  \
	         -c'arith $$3 $$4 -' --noblankend=2 -O \
	         | asttable $$minareaopt --range=2,0,5 \
	         | aststatistics -c3 --sigclip-median > $@

#	Clean up.
	rm $$matched





# Final target
# ------------
#
# Build the final table.
$(OUTPUT): $(add)

#	Make sure the output doesn't exist.
	rm -f $@
	out=$@.tmp

#	Calculate the the maximum length of the input file names.
	maxlen=0
	for f in $(INPUTS); do
	  namelen=$${#f}
	  if [ $$namelen -gt $$maxlen ]; then maxlen=$$namelen; fi
	done

#	Add the metadata for the output.
	echo "# Column 1: NAME     [name, str"$$maxlen"] Input file name" \
	     > $$out
	echo "# Column 2: ADDITIVE [n/a,  f32] Value to add to match sky" \
	     >> $$out

#	Put all the info in a plain-text file.
	for f in $(add); do
	  base=$$(echo $$f | sed -e's|-add.txt||')
	  name=$$(cat $$base-file.txt)
	  value=$$(cat $$f)
	  echo "$$name   $$value" >> $$out
	done

#	Write into the desired output format (preferrably FITS).
	asttable $$out -o$@
	rm $$out
