# Stack multiple images with good WCS onto one image and keep the
# deepest region (possibly with "holes" of shallower stacks, usually
# just noise).
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2020 Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# For a copy of the GNU General Public License see
# <http://www.gnu.org/licenses/>.





# Input parameters
# ----------------
#
# These parameters depend on the host system and selected image,
# please set them manually.
#   INDIR: Directory of input images, all files with the suffix given
#          below will be used as input.
#   IN_SUFFIX: suffix of input images (e.g., `fit' or `fits').
#   BDIR: Directory to put built products (will be made if not there).
#   FLIP_BEFORE_ALIGN: `0' or `1', this is done to account for bug
#                      55217 in Gnuastro's Warp program. Until we fix
#                      that bug, it is necessary when the WCS of the
#                      input images are left-hand oriented, see
#                      http://savannah.gnu.org/bugs/index.php?55217
#   STACK_WIDTH_PIX:   Width of final stack (in pixels), separated by comma.
#   STACK_CENTER_RADEC: Central position of stack (RA,Dec), separated by space.
#   STACK_SIGCLIP_MULTIP: Stacking sigma-clip multiple of sigma.
#   STACK_SIGCLIP_TOLERANCE: Stacking sigma-clip tolerance.
INDIR = /path/to/inputs
IN_SUFFIX = fits
BDIR = /path/to/top/build/directory
FLIP_BEFORE_ALIGN = 1
STACK_WIDTH_PIX = XWIDTH,YWIDTH
STACK_CENTER_RADEC = RA   DEC
STACK_SIGCLIP_MULTIP = 5
STACK_SIGCLIP_TOLERANCE = 0.2





# Basic Make settings.
all: final
.ONESHELL:
SHELL=bash
.SHELLFLAGS = -ec


# Define the build directory
$(BDIR):; mkdir $@


# Put a list of input files into the `inputs' variable.
inputs := $(shell ls $(INDIR)/*.$(IN_SUFFIX))


# Align all images to the proper WCS coordinates.
adir = $(BDIR)/aligned
aligned = $(patsubst $(INDIR)/%.$(IN_SUFFIX),$(adir)/%.fits,$(inputs))
$(adir): | $(BDIR); mkdir $@
$(aligned): $(adir)/%.fits: $(INDIR)/%.$(IN_SUFFIX) | $(adir)

        # Because of (bug #55217), we may first need to flip the
        # images along their second axis:
        # http://savannah.gnu.org/bugs/index.php?55217
	if [ x$(FLIP_BEFORE_ALIGN) = x1 ]; then
	  align_input=$(subst .fits,-flipped.fits,$@)
	  astwarp $< -h0 --align -o$$align_input
	else align_input=$<
	fi

        # Align the images.
	astwarp $$align_input --align -o$@

        # The PC matrix is now aligned, so remove meaningless floating
        # point errors in the WCS.
	astfits $@ -h1 --update=PC1_1,-1 --update=PC1_2,0 \
	        --update=PC2_1,0 --update=PC2_2,1

        # Clean up (if flipping was done).
	if [ x$(FLIP_BEFORE_ALIGN) = x1 ]; then rm $$align_input; fi



# Crop all images on same central point.
cdir = $(BDIR)/centered
centered = $(subst $(adir),$(cdir),$(aligned))
$(cdir): | $(BDIR); mkdir $@
$(centered): $(cdir)/%: $(adir)/% | $(cdir)

        # Find the central position's pixel positions for this
        # image. The reason is that Crop doesn't yet support
        # `TAN-SIP'.
	center_xy=$$(echo "$(STACK_CENTER_RADEC)" \
	                  | asttable -c'arith $$1 $$2 wcstoimg' \
	                             --wcsfile=$< \
	                  | awk '{printf("%s,%s", $$1, $$2)}')

        # Crop out the coordinates around the central point.
	astcrop $< --mode=img --center=$$center_xy \
	        --width=$(STACK_WIDTH_PIX) -o$@


# Stack the cropped images
sdir=$(BDIR)/stacked
stacked = $(sdir)/stacked.fits
$(sdir): | $(BDIR); mkdir $@
$(stacked): $(centered) | $(sdir)

        # Number of images to stack.
	num=$$(echo "$(centered)" | wc -w)

        # Build the exposure map.
	expname=$(dir $@)number.fits
	astarithmetic $(centered) $$num $(STACK_SIGCLIP_MULTIP) \
	              $(STACK_SIGCLIP_TOLERANCE) sigclip-number uint8 \
	              -g1 -o$$expname

        # Build the actual stack
	astarithmetic $(centered) $$num $(STACK_SIGCLIP_MULTIP) \
	              $(STACK_SIGCLIP_TOLERANCE) sigclip-mean -g1 -o$@


# Only keep the deepest region
ddir=$(BDIR)/deepest
deepest=$(ddir)/stack.fits
$(ddir): | $(BDIR); mkdir $@
$(deepest): $(stacked) | $(ddir)

        # --------------------
        # Note that all three `astarithmetic' commands here can be put
        # in one call to greatly speed it up (when there are many
        # images). I am just keeping them separate to help in
        # readability.
        # --------------------

        # Build the mask containing the deepest pixels.
	mask=$(ddir)/mask.fits
	astarithmetic $(sdir)/number.fits set-i \
	              i i maxvalue eq 2 fill-holes -o$$mask

        # Use the mask to build the exposure map and final stack
        # images over the deepest region.
	astarithmetic $(sdir)/number.fits $$mask not nan where \
	              -g1 -o$(ddir)/number.fits
	astarithmetic $(sdir)/stacked.fits $$mask not nan where \
	              -g1 -o$@


# Final Directory.
final: $(deepest)
	echo; echo; echo "DONE :-)"
