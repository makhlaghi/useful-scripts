#!/bin/sh

# Script to use Gnuastro+astrometry.net for finding the WCS properties
# of each input image.
#
# USAGE: set the variables under the "Input parameters" section below,
# then run the script. Once it is finished, you will see a
# '*-wcs.fits' file for every input file within the build
# directory. That file contains the WCS headers of your images from
# astrometry.net. To add those headers to the input image, you can
# simply run the following Gnuastro command (just replacing 'IMAGE'
# with the base name of your inputs).
#
#    $ astarithmetic IMAGE.fits --wcsfile=IMAGE-wcs.fits --wcshdu=0
#
# TIPS:
#  - To visually test the astrometry, use: --checkaper.
#  - Feel free to change the NoiseChisel or Segment options.
#
# Copyright (C) 2023      Sepideh Eskandarlou <sepideh.eskandarlou@gmail.com>
# Copyright (C) 2022-2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2022-2023 Elham Saremi <saremy.elham@gmail.com>
# Copyright (C) 2021-2023 Zohreh Ghaffari <zoh.ghaffari@gmail.com>
# Copyright (C) 2018-2023 Raul Infante-Sainz <infantesainz@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Exit the script in the case of failure
set -e

# 'LC_NUMERIC' is responsible for formatting numbers printed by the OS.  It
# prevents floating points like '23,45' instead of '23.45'.
export LC_NUMERIC=C





# Default parameter's values
hdu=1
outdir=.
quiet=""
center=""
radius=""
output=""
tmpdir=""
indices=""
keeptmp=""
checkaper=""
version=@VERSION@
suffix="-wcs.fits"
dataset="gaia --dataset=dr3"
scriptname=$0 #@SCRIPT_NAME@

segparams="--snminarea 5"
detparams="--qthresh=0.2 \
           --tilesize=10,10 \
           --interpnumngb=50 \
           --detgrowmaxholesize=10000"





# Output of '--usage'
print_usage() {
     cat <<EOF
$scriptname: run with '--help' to list the options.
EOF
}





# Output of '--help'
print_help() {
   cat <<EOF
Usage: $scriptname [OPTIONS] image.fits

Do the astrometry of the images using astrometry.net

To run this script on multiple images with a '*' (shell wild-card), replace
'image.fits' with '"path/to/images/*fits"' (keeping the double-quotations).

$scriptname options:
 Input:
  -h, --hdu=STR/INT       HDU/Extension name or number of the input file.
  -r, --radius=FLT        Radius of the images FOV (in deg).
  -c, --center=FLT,FLT    Center of the images FOV (RA,DEC in deg).
  -p, --scale=FLT,FLT     Pixel scale range of values (in arcsec).
  -d, --dataset=STR       Query dataset (default: "$dataset").
  -o, --outdir=STR        Output directory where the WCS headers are saved.
  -s, --suffix=STR        Suffix of the WCS solution (default is "$suffix").
  -n, --detparams="STR"   NoiseChisel conf (e.g., --detparams="--qthresh=0.2").
  -N, --segparams="STR"   Segment params (e.g.,  --segparams="--snminarea 5").
  -i, --indices=INT,INT   Indices to split the ref. catalog. Approximate:
                           Values   : < -1  0   2   4        |  6   8  12     >
                           FOV size : <  3  6  12  24 arcmin |  1   2   4 deg >

 Output:
  -t, --tmpdir=STR        Directory to keep temporary files.
  -k, --keeptmp           Keep temporal/auxiliar files.
  -a, --checkaper         Check WCS solution with apertures on images

 Operating mode:
  -h, --help              Print this help.
      --cite              BibTeX citation for this program.
  -q, --quiet             Don't print any extra information in stdout.
  -V, --version           Print program version.

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponfing short options.

GNU Astronomy Utilities home page: http://www.gnu.org/software/gnuastro/

Report bugs to bug-gnuastro@gnu.org
EOF
}





# Output of '--version':
print_version() {
     cat <<EOF
$scriptname (GNU Astronomy Utilities) $version
Copyright (C) 2020-2023 Free Software Foundation, Inc.
License GPLv3+: GNU General public license version 3 or later.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written/developed by Mohammad Akhlaghi, Raul Infante-Sainz and Sepideh Eskandarlou.
EOF
}





# Output of `--cite':
print_citation() {
    empty="" # needed for the ascii art!
    cat <<EOF

Thank you for using $scriptname (GNU Astronomy Utilities) $version

Citations and acknowledgement are vital for the continued work on Gnuastro.

Please cite the following record(s) and add the acknowledgement statement below in your work to support us. Please note that different Gnuastro programs may have different corresponding papers. Hence, please check all the programs you used. Don't forget to also include the version as shown above for reproducibility.

First paper introducing Gnuastro
--------------------------------
  @ARTICLE{gnuastro,
     author = {{Akhlaghi}, M. and {Ichikawa}, T.},
      title = "{Noise-based Detection and Segmentation of Nebulous Objects}",
    journal = {ApJS},
  archivePrefix = "arXiv",
     eprint = {1505.01664},
   primaryClass = "astro-ph.IM",
       year = 2015,
      month = sep,
     volume = 220,
        eid = {1},
      pages = {1},
        doi = {10.1088/0067-0049/220/1/1},
     adsurl = {https://ui.adsabs.harvard.edu/abs/2015ApJS..220....1A},
    adsnote = {Provided by the SAO/NASA Astrophysics Data System}
  }

Acknowledgement
---------------
This work was partly done using GNU Astronomy Utilities (Gnuastro, ascl.net/1801.009) version $version. Work on Gnuastro has been funded by the Japanese Ministry of Education, Culture, Sports, Science, and Technology (MEXT) scholarship and its Grant-in-Aid for Scientific Research (21244012, 24253003), the European Research Council (ERC) advanced grant 339659-MUSICOS, the Spanish Ministry of Economy and Competitiveness (MINECO, grant number AYA2016-76219-P) and the NextGenerationEU grant through the Recovery and Resilience Facility project ICTS-MRR-2021-03-CEFCA.
                                               ,
                                              {|'--.
                                             {{\    \ $empty
      Many thanks from all                   |/\`'--./=.
      Gnuastro developers!                   \`\.---' \`\\
                                                  |\  ||
                                                  | |//
                                                   \//_/|
                                                   //\__/
                                                  //
                   (http://www.chris.com/ascii/) |/

EOF
}




# Functions to check option values and complain if necessary.
on_off_option_error() {
    if [ x"$2" = x ]; then
        echo "$scriptname: '$1' doesn't take any values"
    else
        echo "$scriptname: '$1' (or '$2') doesn't take any values"
    fi
    exit 1
}

check_v() {
    if [ x"$2" = x ]; then
        cat <<EOF
$scriptname: option '$1' requires an argument. Try '$scriptname --help' for more information
EOF
        exit 1;
    fi
}





# Separate command-line arguments from options and put the option values
# into the respective variables.
#
# OPTIONS WITH A VALUE:
#
#   Each option has three lines because we take into account the three common
#   formats:
#   For long option names, '--longname value' and '--longname=value'.
#   For short option names, '-l value', '-l=value' and '-lvalue'
#   (where '-l' is the short version of the hypothetical '--longname option').
#
#   The first case (with a space between the name and value) is two
#   command-line arguments. So, we'll need to shift it twice. The
#   latter two cases are a single command-line argument, so we just need to
#   "shift" the counter by one.
#
#   IMPORTANT NOTE: the ORDER OF THE LATTER TWO cases matters: '-h*' should be
#   checked only when we are sure that its not '-h=*').
#
# OPTIONS WITH NO VALUE (ON-OFF OPTIONS)
#
#   For these, we just want the forms of '--longname' or '-l'. Nothing
#   else. So if an equal sign is given we should definitely crash and also,
#   if a value is appended to the short format it should crash. So in the
#   second test for these ('-l*') will account for both the case where we
#   have an equal sign and where we don't.
while [ $# -gt 0 ]
do
   case "$1" in
   # Input parameters.
       -h|--hdu)               hdu="$2";                                    check_v "$1" "$hdu";  shift;shift;;
       -h=*|--hdu=*)           hdu="${1#*=}";                               check_v "$1" "$hdu";  shift;;
       -h*)                    hdu=$(echo "$1" | sed -e's/-h//');           check_v "$1" "$hdu";  shift;;
       -p|--scale)             scale="$2";                                  check_v "$1" "$scale";  shift;shift;;
       -p=*|--scale=*)         scale="${1#*=}";                             check_v "$1" "$scale";  shift;;
       -p*)                    scale=$(echo "$1" | sed -e's/-p//');         check_v "$1" "$scale";  shift;;
       -c|--center)            center="$2";                                 check_v "$1" "$center";  shift;shift;;
       -c=*|--center=*)        center="${1#*=}";                            check_v "$1" "$center";  shift;;
       -c*)                    center=$(echo "$1" | sed -e's/-c//');        check_v "$1" "$center";  shift;;
       -r|--radius)            radius="$2";                                 check_v "$1" "$radius";  shift;shift;;
       -r=*|--radius=*)        radius="${1#*=}";                            check_v "$1" "$radius";  shift;;
       -r*)                    radius=$(echo "$1" | sed -e's/-r//');        check_v "$1" "$radius";  shift;;
       -d|--dataset)           dataset="$2";                                check_v "$1" "$dataset";  shift;shift;;
       -d=*|--dataset=*)       dataset="${1#*=}";                           check_v "$1" "$dataset";  shift;;
       -d*)                    dataset=$(echo "$1" | sed -e's/-d//');       check_v "$1" "$dataset";  shift;;
       -i|--indices)           indices="$2";                                check_v "$1" "$indices";  shift;shift;;
       -i=*|--indices=*)       indices="${1#*=}";                           check_v "$1" "$indices";  shift;;
       -i*)                    indices=$(echo "$1" | sed -e's/-i//');       check_v "$1" "$indices";  shift;;
       -o|--outdir)            outdir="$2";                                 check_v "$1" "$outdir";  shift;shift;;
       -o=*|--outdir=*)        outdir="${1#*=}";                            check_v "$1" "$outdir";  shift;;
       -o*)                    outdir=$(echo "$1" | sed -e's/-o//');        check_v "$1" "$outdir";  shift;;
       -s|--suffix)            suffix="$2";                                 check_v "$1" "$suffix";  shift;shift;;
       -s=*|--suffix=*)        suffix="${1#*=}";                            check_v "$1" "$suffix";  shift;;
       -s*)                    suffix=$(echo "$1" | sed -e's/-s//');        check_v "$1" "$suffix";  shift;;
       -n|--detparams)         detparams="$2";                              check_v "$1" "$detparams";  shift;shift;;
       -n=*|--detparams=*)     detparams="${1#*=}";                         check_v "$1" "$detparams";  shift;;
       -n*)                    detparams=$(echo "$1" | sed -e's/-n//');     check_v "$1" "$detparams";  shift;;
       -N|--segparams)         segparams="$2";                              check_v "$1" "$segparams";  shift;shift;;
       -N=*|--segparams=*)     segparams="${1#*=}";                         check_v "$1" "$segparams";  shift;;
       -N*)                    segparams=$(echo "$1" | sed -e's/-N//');     check_v "$1" "$segparams";  shift;;


# Output parameters
       -a|--checkaper)         checkaper=1; shift;;
       -a*|--checkaper=*)      on_off_option_error --checkaper -a;;
       -k|--keeptmp)           keeptmp=1; shift;;
       -k*|--keeptmp=*)        on_off_option_error --keeptmp -k;;
       -t|--tmpdir)            tmpdir="$2";                                  check_v "$1" "$tmpdir";  shift;shift;;
       -t=*|--tmpdir=*)        tmpdir="${1#*=}";                             check_v "$1" "$tmpdir";  shift;;
       -t*)                    tmpdir=$(echo "$1" | sed -e's/-t//');         check_v "$1" "$tmpdir";  shift;;

   # Non-operating options.
       -q|--quiet)             quiet=" -q"; shift;;
       -q*|--quiet=*)          on_off_option_error --quiet -q;;
       -?|--help)              print_help; exit 0;;
       -'?'*|--help=*)         on_off_option_error --help -?;;
       -V|--version)           print_version; exit 0;;
       -V*|--version=*)        on_off_option_error --version -V;;
       --cite)                 print_citation; exit 0;;
       --cite=*)               on_off_option_error --cite;;

   # Unrecognized option:
       -*) echo "$scriptname: unknown option '$1'"; exit 1;;

       # Not an option (not starting with a `-'): assumed to be input FITS
       # file name.
       *) inputs="$1"; shift;;
   esac
done






# Basic sanity checks
# ===================

# If an input image is not given at all.
if [ x"$inputs" = x ]; then
    cat <<EOF
$scriptname: ERROR: no input FITS image files. Run with '--help' for more information on how to run
EOF
    exit 1
else
    for img in $inputs; do
	if [ ! -f $img ]; then
	    cat <<EOF
$scriptname: $img: no such file or directory
EOF
	    exit 1
	fi
    done
fi


# If center (--center) is not given at all
if [ x"$center" = x ]; then
    cat <<EOF
$scriptname: ERROR: no center provided ('--center' or '-c')
EOF
    exit 1
else
    ncenter=$(echo $center | awk 'BEGIN{FS=","}END{print NF}')
    if [ x$ncenter != x2 ]; then
        cat <<EOF
$scriptname: ERROR: '--center' (or '-c') only take two values, but $ncenter were given in '$center'
EOF
        exit 1
    fi
    ra=$(echo "$center" | awk 'BEGIN{FS=","} {print $1}')
    dec=$(echo "$center" | awk 'BEGIN{FS=","} {print $2}')
fi


# If indices (--indices) is not given at all
if [ x"$indices" = x ]; then
    cat <<EOF
$scriptname: ERROR: no indices provided ('--indices' or '-i')
EOF
    exit 1
else
    nindices=$(echo $indices | awk 'BEGIN{FS=","}END{print NF}')
    if [ x$nindices != x2 ]; then
        cat <<EOF
$scriptname: ERROR: '--indices' (or '-i') only take two values, but $nindices were given in '$indices'
EOF
        exit 1
    fi
    # Obtain the indices min and max.
    indexmin=$(echo "$indices" | awk 'BEGIN{FS=","} {print $1}')
    indexmax=$(echo "$indices" | awk 'BEGIN{FS=","} {print $2}')
fi


# If scale (--scale) is not given at all
if [ x"$scale" = x ]; then
    cat <<EOF
$scriptname: ERROR: no scale provided ('--scale' or '-p')
EOF
    exit 1
else
    nscale=$(echo $scale| awk 'BEGIN{FS=","}END{print NF}')
    if [ x$nscale!= x2 ]; then
        cat <<EOF
$scriptname: ERROR: '--scale' (or '-p') only take two values, but $nscale were given in '$scale'
EOF
        exit 1
    fi
    # Obtain the indices min and max.
    scalelow=$(echo "$scale" | awk 'BEGIN{FS=","} {print $1}')
    scalehigh=$(echo "$scale" | awk 'BEGIN{FS=","} {print $2}')
fi





# Define a temporary directory
# ----------------------------
#
if [ x"$tmpdir" = x ]; then
  tmplabel="c_$ra-$dec-r_$radius-i_$indexmin-$indexmax-s_$scalelow-$scalehigh"
  tmpdir=$(pwd)/astrometrynet_$tmplabel
fi

if [ -d "$tmpdir" ]; then
  junk=1
else
  mkdir -p "$tmpdir"
fi





# Download the Gaia catalog.
# --------------------------
gaia=$tmpdir/gaia.fits
if ! [ -f $gaia ]; then
    astquery $dataset --output=$gaia --center=$center \
             --radius=$radius -cra,dec,phot_g_mean_mag
fi





# Build each index.
#
# -i: input catalog name.
# -e: HDU number
# -o: output name.
# -E: covers small part of the sky.
# -P: defines the number of index (index scale).
# -S: sort the column of magnitute (to find bright stars).
# -A: column with right ascension.
# -D: column with declination.
#     -P 2  should work for images about 12 arcmin across
#     -P 4  should work for images about 24 arcmin across
#     -P 6  should work for images about 1 degree across
#     -P 8  should work for images about 2 degree across
#     -P 10 should work for images about 4 degree across
inddir=$tmpdir/indexs
if ! [ -d $inddir ]; then mkdir $inddir; fi
for i in $(seq $indexmin $indexmax); do
    ind=$inddir/$i.index
    if ! [ -f $ind ]; then
	build-astrometry-index -i $gaia -e1 -o $ind \
			       -E \
			       -P $i \
			       -S phot_g_mean_mag \
			       -A RA \
			       -D DEC
    fi
done





# Setup astrometry.net's configuration file.
config=$tmpdir/astrometry.cfg
printf "inparallel\ncpulimit 300\nadd_path $inddir\nautoindex" > $config





# Construct a catalog of the clumps and do the astrometry.
for i in $inputs; do

    # Output wcs header
    wcs=$outdir/$(basename $i | sed -e's|.fits||' \
                                    -e's|.fit||')$suffix

    # If no astrometry results is present yet, find it:
    if ! [ -f $wcs ]; then

        # Build the base directory for each input image.
        base=$tmpdir/$(basename $i)
        if ! [ -d $base ]; then mkdir $base; fi


        # Necessary images/catalogs
        nc=$base/nc.fits
        seg=$base/seg.fits
        catraw=$base/cat-raw.fits
        cat=$base/cat-sorted.fits
        cat_pure_clumps=$base/cat-pure-clumps.fits
        cat_diff_clumps=$base/cat-diff-clumps.fits

        # Detection
        if ! [ -f $nc ]; then
        astnoisechisel $i --hdu $hdu --output $nc \
                       $detparams
        fi

        # Segmentation
        if ! [ -f $seg ]; then
        astsegment $nc --output $seg \
                       $segparams
        fi

        # Catalog
        if ! [ -f $cat ]; then
            astmkcatalog $seg -o$catraw --clumpscat \
                         --max-val-x --max-val-y \
                         --sum --num-clumps

            asttable $catraw --hdu=CLUMPS \
                     --output=$cat_pure_clumps

            asttable $catraw --hdu OBJECTS --equal NUM_CLUMPS,0 \
                     --output=$cat_diff_clumps

            asttable $cat_pure_clumps \
                     --catrowfile $cat_diff_clumps --catrowhdu 1 \
                     -cMAX_VAL_X,MAX_VAL_Y,SUM \
                     --sort=sum --descending --output $cat

        fi

        # Find the number of pixels in each dimension fo the image.
        xsize=$(astfits $nc --keyvalue NAXIS1 --quiet)
        ysize=$(astfits $nc --keyvalue NAXIS2 --quiet)

        # Run astrometry.net's 'solve-field'
        solve-field $cat --x-column MAX_VAL_X --y-column MAX_VAL_Y \
                    --width $xsize --height $ysize \
                    --scale-units arcsecperpix \
                    --scale-high $scalehigh \
                    --scale-low $scalelow \
                    --config $config \
                    --overwrite \
                    --wcs $wcs \
                    --no-plot

        # If the checking mode has been enabled do the check.
        if [ x"$checkaper" = x1 ]; then

            # If user wants to check, do not delete the temporary directory
            keeptmp=1

            # Build the with-WCS image.
            withwcs=$base/with-wcs.fits
            if ! [ -f $withwcs ]; then
                astarithmetic $nc -hINPUT-NO-SKY --wcsfile=$wcs \
                              --wcshdu=0 --output $withwcs
            fi

            # Extract the sky coverage of the image (to only show those
            # regions).
            ropt=$(astfits $withwcs --skycoverage --quiet \
                           | awk 'NR==2{printf "--range=ra,%s:%s --range=dec,%s:%s", \
                                        $1, $2, $3, $4}')

            # Write the region file (while putting the magnitudes of the
            # objects.
            gaiareg=$base/gaia.reg
            asttable $gaia $ropt --txtf32p=2 -O \
                | astscript-ds9-region --namecol=phot_g_mean_mag \
                                       --color=green --width=4 \
                                       -cra,dec --mode=wcs --output $gaiareg
cat <<EOF
$scriptname: Run the following line to check the astrometry: ds9 $withwcs -regions load $gaiareg
EOF
        fi

    fi

done





# Remove temporary directory
# --------------------------
#
# If user does not specify to keep build file with the option of
# --keeptmp', then the directory will be removed.
if [ x"$keeptmp" = x ]; then
   rm -rf $tmpdir
fi
