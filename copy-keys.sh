#!/bin/bash
#
# Copy multiple keys from one FITS file/extension to another. Note
# that the keys have to be within double quotes (like the usage
# example below).
#
# Usage:
#  ./copy-keys.sh input.fits inhdu output.fits outhdu "KEYA KEYB ..."
#
# Only the keys that exist in the input will be written in the
# output. Also, their order will be based on the order of the keys in
# the input.
#
# Copyright (C) 2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.





# Customizations (input, output and key names).
INPUT=$1
IHDU=$2
OFITS=$3
OHDU=$4
KEYS="$5"
title="Keys of $INPUT (hdu $IHDU)"





# Put a '\|' between each key in a case that whole context of input
# doesn't consider as keywords(for 'grep').
keys=""
if ! [ $KEYS = _all ]; then
    for k in $KEYS; do
	if [ x$keys = x ]; then keys="^$k"
	else                    keys="$keys\\|$k"
	fi
    done
fi



# Keep the default Internal Field Separate (IFS) in a temporary
# variable, then remove any value it had (so the spaces are kept
# within the read keyword).
oIFS=$IFS
IFS=''





# Read the keywords from input.
#
# Chacking the keywords in the case that the input file is a FITS file
# and in a case that is a text file. In the possibility that the input
# file is a text file, this possibility exists that the whole text
# file context considers as keywords.
if astfits $INPUT &> /dev/null; then
    fullkeys="$(astfits $INPUT -h$IHDU | grep $keys)"
else
    if [ $KEYS = _all ]; then
	fullkeys="$(cat $INPUT)"
    else
	fullkeys="$(grep $keys $INPUT)"
    fi
fi




# If any keywords matched, then write them into the output.
if [ x"$fullkeys" = x ]; then
    echo "$0: no keys matched!"
else
    # Set the start of the necessary command.
    c="astfits $OFITS -h$OHDU --write=/,\"$title\""

    # Add the option for each keyword to the command with the
    # loop. Just note the parenthesis around 'while' and 'eval'. This
    # is because every pipe is actually a new shell. So without it,
    # the extra options added to the command within the 'while' loop
    # will be lost when the loop finishes. With the parenthesis, the
    # 'eval' will be called within the same shell as 'while'.
    echo "$fullkeys" \
	| ( while read line; do c="$c --asis=\"$line\""; done;
	    eval $c);
fi





# Reset the internal field separator.
IFS=$oIFS
