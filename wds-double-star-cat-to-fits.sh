#!/bin/bash
# Script to convert the plain-text table format of The Washington
# Double Star Catalog (WDS) into FITS. For more on the WDS, please see
# http://www.astro.gsu.edu/wds/.
#
# The raw WDS catalog URL:
#    http://www.astro.gsu.edu/wds/Webtextfiles/wdsweb_summ2.txt
#
# USAGE:
#    ./this-script input.txt output.fits
#
# Copyright (C) 2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.





# Input arguments and sanity checks.
input=$1
output=$2
if [ x$input = x ]; then
    echo "The WDS dataset (first argument) isn't given."; exit 1
fi
if [ x$output = x ]; then output=wds.fits; fi





# To stop the script if any command fails.
set -e





# The September 17th 2020 version has a problem on line 134245
correctedinput=wds-corrected.txt
checksum=$(sha1sum $input | awk '{print $$1}')
if [ $checksum = 1a8ccf19ad8c0aaaa30181976c219b1ebc333fe8 ]; then
    sed -e's/TW 10052.53/TW  210052.53/' $input > $correctedinput
    input=$correctedinput
fi





# Put the metadata in the final file.  For the types of the RA and Dec
# columns, in Gnuastro the strings '_h_m_s' and '_d_m_s' are directly
# converted to degrees and written as 64-bit floating point.
txtcat=tmp-wds-cat.txt
echo "# Column 1:  id         [counter,         str10, NaN] ID generated from coordinates." > $txtcat
echo "# Column 2:  ra         [counter,         f64,   NaN] ID generated from coordinates." >> $txtcat
echo "# Column 3:  dec        [counter,         f64,   NaN] ID generated from coordinates." >> $txtcat
echo "# Column 4:  discoverer [name,            str7,  NaN] Discoverer & Number." >> $txtcat
echo "# Column 5:  components [name,            str6,  NaN] Components, when there is more than two." >> $txtcat
echo "# Column 6:  date_first [counter,         u16,   NaN] First observation date." >> $txtcat
echo "# Column 7:  date_last  [counter,         u16,   NaN] Last observation date." >> $txtcat
echo "# Column 8:  obs_num    [counter,         u16,   NaN] Number of observations (max 9999)" >> $txtcat
echo "# Column 9:  pa_first   [deg,             i16,   NaN] Position angle in degrees for date_first" >> $txtcat
echo "# Column 10: pa_last    [deg,             i16,   NaN] Position angle in degrees for date_last." >> $txtcat
echo "# Column 11: sep_first  [arcsec,          f32,   NaN] Separation in arc-seconds for date_first." >> $txtcat
echo "# Column 12: sep_last   [arcsec,          f32,   NaN] Separation in arc-seconds for date_last." >> $txtcat
echo "# Column 13: mag_first  [log,             f32,   NaN] Magnitude of first component." >> $txtcat
echo "# Column 14: mag_second [log,             f32,   NaN] Magnitude of second component." >> $txtcat
echo "# Column 15: spec_type  [name,            str10, NaN] Spectral Type (Primary/Secondary)." >> $txtcat
echo "# Column 16: pm_1_ra    [arcsec/kiloyear, i16,   NaN] Primary Proper Motion (RA)." >> $txtcat
echo "# Column 17: pm_1_dec   [arcsec/kiloyear, i16,   NaN] Primary Proper Motion (Dec), arcsec/kiloyear." >> $txtcat
echo "# Column 18: pm_2_ra    [arcsec/kiloyear, i16,   NaN] Secondary Proper Motion (RA), arcsec/kiloyear." >> $txtcat
echo "# Column 19: pm_2_dec   [arcsec/kiloyear, i16,   NaN] Secondary Proper Motion (Dec), arcsec/kiloyear." >> $txtcat
echo "# Column 20: durch_num  [name,            str9,  NaN] Durchmusterung Number." >> $txtcat
echo "# Column 21: notes      [name,            str4,  NaN] Notes, see http://www.astro.gsu.edu/wds" >> $txtcat





# Use AWK to separate the columns, and fill empty ones with blank
# before converting to FITS.
awk 'BEGIN{ FIELDWIDTHS = "10 7 6 5 5 5 4 4 6 6 6 6 10 4 5 4 5 9 5 2 2 5 3 2 4"} \
     !/^#/ && NR>5 && NF>1 \
       { sub("\r$",""); \
         for(i=1; i<=NF; ++i) { sub(/^ +/, "", $i); sub(/ +$/, "", $i); if ($i=="" || $i==".") $i="NaN"; } \
         ra  = ($20=="NaN" || $21=="NaN" || $22=="NaN") ? "NaN" : $20"h"$21"m"$22; \
         dec = ($23=="NaN" || $24=="NaN" || $25=="NaN") ? "NaN" : $23"d"$24"m"$25; \
         printf "%-10s %-12s %-12s %-7s %-6s %-5s %-5s %-5s %-4s %-4s %-5s %-5s %-6s %-6s %-10s %-5s %-5s %-5s %-5s %-9s %-4s\n", \
                $1, ra, dec, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, \
                $19 }' $input >> $txtcat





# Convert it to FITS and add some metadata from the input.
asttable $txtcat -o$output
astfits $output -h1 --write=/,"Washington Double Star (WDS) Catalog" \
	--write=INCHKSUM,$checksum,"Input SHA1 checksum"




# Delete the temporary file(s).
if [ $input = $correctedinput ]; then rm $input; fi
rm $txtcat
