# Build the profiles in an input catalog based on a truncation table,
# see the description in the 'print_help' function for more.
#
# Usage:
#
#  ./mkprof-truncation-match-build.sh mkprof-input.fits \
#          --truncations=trunc.fits --zeropoint=26
#
# Copyright (C) 2024-2024 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.




# Initial values.
zp=""
trunc=""
incat=""
output=""
tmpdir=""
keeptmp=0
mkprofopts=""





# Abort the script in case of an error.
set -e





# Output of '--usage'
print_usage() {
     cat <<EOF
$0: run with '--help' to list the options.
EOF
}





# Output of '--help'
print_help() {
   cat <<EOF
Usage: $0 [OPTIONS] mkprof-input.fits

Having found the truncation radius using 'mkprof-truncation.mk' (which
is in the same repository as this at
https://gitlab.cefca.es/gnuastro/scripts), match each profile of the
input catalog to its corresponding point in the parameter space and
build the image.

The 'mkprof-input.fits' table is the input to MakeProfiles (all the
columns should be assumed to be passed onto MakeCatalog), except for
the truncation column. That column can either not exist, or can have
an arbitrary value.

$0 options:
 Input:
  -t, --truncations=STR   File created by 'mkprof-truncation.mk'.

 Output:
  -o, --output            Output table with the object coordinates.
  -t, --tmpdir            Directory to keep temporary files.
  -k, --keeptmp           Keep temporal/auxiliar files.
  -m, --mkprofopts        Options to pass to MakeProfiles; we need at least
                          --mkprofopts="--zeropoint=XX.XX --mergedsize=XX,XX"

 Operating mode:
  -?, --help              Print this help.

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponfing short options.

Report bugs to mohammad@akhlaghi.org
EOF
}





# Functions to check option values and complain if necessary.
on_off_option_error() {
    if [ x"$2" = x ]; then
        echo "$0: '$1' doesn't take any values"
    else
        echo "$0: '$1' (or '$2') doesn't take any values"
    fi
    exit 1
}

check_v() {
    if [ x"$2" = x ]; then
        cat <<EOF
$0: option '$1' requires an argument. Try '$0 --help' for more information
EOF
        exit 1;
    fi
}





# Separate command-line arguments from options and put the option values
# into the respective variables.
#
# OPTIONS WITH A VALUE:
#
#   Each option has three lines because we take into account the three common
#   formats:
#   For long option names, '--longname value' and '--longname=value'.
#   For short option names, '-l value', '-l=value' and '-lvalue'
#   (where '-l' is the short version of the hypothetical '--longname option').
#
#   The first case (with a space between the name and value) is two
#   command-line arguments. So, we'll need to shift it twice. The
#   latter two cases are a single command-line argument, so we just need to
#   "shift" the counter by one.
#
#   IMPORTANT NOTE: the ORDER OF THE LATTER TWO cases matters: '-h*' should be
#   checked only when we are sure that its not '-h=*').
#
# OPTIONS WITH NO VALUE (ON-OFF OPTIONS)
#
#   For these, we just want the forms of '--longname' or '-l'. Nothing
#   else. So if an equal sign is given we should definitely crash and also,
#   if a value is appended to the short format it should crash. So in the
#   second test for these ('-l*') will account for both the case where we
#   have an equal sign and where we don't.
while [ $# -gt 0 ]
do
    case "$1" in

        # Input parameters.
        -t|--truncations)       trunc="$2";                                  check_v "$1" "$trunc";  shift;shift;;
        -t=*|--truncations=*)   trunc="${1#*=}";                             check_v "$1" "$trunc";  shift;;
        -t*)                    trunc=$(echo "$1" | sed -e's/-t//');         check_v "$1" "$trunc";  shift;;

        # Output parameters
        -k|--keeptmp)           keeptmp=1; shift;;
        -k*|--keeptmp=*)        on_off_option_error --keeptmp -k;;
        -t|--tmpdir)            tmpdir="$2";                                  check_v "$1" "$tmpdir"; shift;shift;;
        -t=*|--tmpdir=*)        tmpdir="${1#*=}";                             check_v "$1" "$tmpdir"; shift;;
        -t*)                    tmpdir=$(echo "$1" | sed -e's/-t//');         check_v "$1" "$tmpdir"; shift;;
        -o|--output)            output="$2";                                  check_v "$1" "$output"; shift;shift;;
        -o=*|--output=*)        output="${1#*=}";                             check_v "$1" "$output"; shift;;
        -o*)                    output=$(echo "$1" | sed -e's/-o//');         check_v "$1" "$output"; shift;;
        -m|--mkprofopts)        mkprofopts="$2";                              check_v "$1" "$mkprofopts"; shift;shift;;
        -m=*|--mkprofopts=*)    mkprofopts="${1#*=}";                         check_v "$1" "$mkprofopts"; shift;;
        -m*)                    mkprofopts=$(echo "$1" | sed -e's/-m//');     check_v "$1" "$mkprofopts"; shift;;

        # Non-operating options.
        -?|--help)              print_help; exit 0;;
        -'?'*|--help=*)         on_off_option_error --help -?;;

        # Unrecognized option:
        -*) echo "$0: unknown option '$1'"; exit 1;;

        # Not an option (not starting with a `-'): assumed to be input FITS
        # file name.
        *) if [ x"$incat" = x ]; then incat="$1"; else
               echo "$0: only one input argument should be given"; exit 1
           fi; shift;;
    esac
done





# Sanity checks.
if [ x"$incat" = x ]; then
    printf "$0: input catalog (assumed to be input to MakeProfiles, "
    printf "but with a redundant truncation radius) not given; see "
    printf "'--help'\n"; exit 1
fi
if [ x"$mkprofopts" = x ]; then
    echo "$0: MakeProfiles options not given; see '--help'"; exit 1
fi
if [ x"$trunc" = x ]; then
    echo "$0: no truncation file given; see '--help'"; exit 1
fi
if [ x"$tmpdir" = x ]; then
    echo "$0: no temporary directory given; see '--help'"; exit 1
fi
if [ x"$output" = x ]; then
    echo "$0: no output given; see '--help'"; exit 1
fi





# Make the build directory if necessary.
if ! [ -d $tmpdir ]; then mkdir $tmpdir; fi





# Match each profile with its pre-determined truncation.
catr=$tmpdir/cat-raw.fits
astmatch $trunc --ccol1=r_e,n,mag $incat --ccol2=5,6,9 \
         --arrange=outer --aperture=1 --output=$catr \
         --outcols=b1,b2,b3,b4,b5,b6,b7,b8,b9,atrunc




# For debugging.
#echo "Before:"
#asttable $catr --range=1,1200,1215 -c1,8,10 -Y

# Make the necessary modifications:
#  - Rows with a zero truncation (even the brightest pixel was below
#    the given surface brightness threshold) should be removed.
#  - When the axis ratio is very small, we should increase the
#    truncation radius.
cat=$tmpdir/cat.fits
asttable $catr --notequal=trunc,0 --rowfirst \
         -c1,2,3,4,5,6,7,8,9, \
         -c'arith $8 set-q $10 set-t \
                  t q 0.5 le q 0.4 gt and t 1 + where set-t \
                  t q 0.4 le q 0.3 gt and t 2 + where set-t \
                  t q 0.3 le q 0.2 gt and t 3 + where set-t \
                  t q 0.2 le q 0.1 gt and t 4 + where set-t \
                  t q 0.1 le q 0.0 gt and t 5 + where ' \
         --output=$cat

#echo "After:"
#asttable $cat --range=1,1200,1215 -c1,8,10 -Y
#echo GOOD; exit 1





# Extract the oversample and pixel scale
os=$(astfits $trunc -h1 --keyvalue=OVSAMP -q)
cdelt=$(astfits $trunc -h1 --keyvalue=PIXSCALE -q)





# Find the width of the image and build it.
astmkprof $cat \
          --oversample=$os \
          --cdelt=$cdelt,$cdelt \
          --output=$output \
          $mkprofopts





# Delete the temporary directory (if requested).
if [ $keeptmp = 0 ]; then rm -rf $tmpdir; fi
