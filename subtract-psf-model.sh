#!/bin/bash
#
# Assume a fixed Moffat PSF, then find all small and ciruclar sources,
# built the Moffat PSF over them to create a scatter field and
# subtract it.
#
# To run this script, set the "User input parameters" below, then run
# it like this:
#     $ ./subtract-psf-model.sh input.fits /path/to/build/directory
#
# Copyright (C) 2020, Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This script is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# For a copy of the GNU General Public License see
# <http://www.gnu.org/licenses/>.





# Input arguments and sanity check.
BDIR=$2
INPUT=$1
if [ x$INPUT = x ]; then
    echo "Input image should be given as first argument"; exit 1
fi
if [ x$BDIR = x ]; then
    echo "Build-directory should be given as second argument"; exit 1
fi





# Parameters:
fwhm=2                  # FWHM of Moffat function to use in the model.
beta=1.9                # Beta of Moffat function fo tuse in the model.
bmultip=1		# Multiple of clump flux to be total model flux.
maxarea=10000		# Maximum area of clump to use for modelling.
axisratio=0.9           # Minimum acceptable axis ratio.
ds9_scale_min=-10       # Minimum value for the 'scale' of ds9.
ds9_scale_max=30        # Maximum value for the 'scale' of ds9.





# Stop the script on an error.
set -e





# If the build-directory doesn't exist, make it.
if ! [ -d $BDIR ]; then mkdir $BDIR; fi





# Produce a catalog of clumps
nc=$BDIR/nc.fits
seg=$BDIR/seg.fits
cat=$BDIR/cat.fits
if ! [ -f $cat ]; then
    astnoisechisel --tilesize=20,20 $INPUT -o$nc
    astsegment $nc -o$seg --snquant=0.9 --onlyclumps
    astmkcatalog $seg -hCLUMPS --ids --ra --dec --brightness \
		 --axisratio --area -o$cat
fi





# Select the necessary objects to assume as being stars, create the
# input parameters to MakeProfiles, then run MakeProfiles to build the
# scatter field.
model=$BDIR/model.fits
if ! [ -f $model.junk ]; then
    asttable $cat --range=AREA,0,$maxarea \
	     --range=AXIS_RATIO,0.85,1 -cra,dec,brightness \
	| awk -vf=$fwhm -vb=$beta -va=$axisratio -vm=$bmultip \
	      '{print NR, $1, $2, 2, f, b, 0, a, m*$3, 50}' \
	| astmkprof --mode=wcs --background=$INPUT --backhdu=1 \
		    --tolerance=0.1 --clearcanvas --mcolisbrightness \
		    --oversample=1 --psfinimg -o$model
fi





# Subtract the scatter field model from the input.
sub=$BDIR/subtracted.fits
astarithmetic $INPUT $model -g1 - -o$sub





# Use DS9 to inspect the output.
ds9 -zscale -geometry 1500x1000 -wcs degrees -zoom to fit \
    -cmap sls $INPUT $sub $model -match frame image -lock frame image \
    -scale limits $ds9_scale_min $ds9_scale_max -match scale -lock scale \
    -match colorbar -lock colorbar -tile mode column -mode pan
