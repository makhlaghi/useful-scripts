#!/bin/bash
#
# Crop multi extintion of an image (in same coordinate and same width)
# and then append them together.
#
# Usage:
#  ./crop.sh input.fits xcoordinate ycoordinate width bdir output
#
# Only the keys that exist in the input will be written in the
# output. Also, their order will be based on the order of the keys in
# the input.
#
# Copyright (C) 2023 Sepideh Eskandarlou <sepideh.eskandarlou@gmail.com>
# Copyright (C) 2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.






# Customizations (input, output, crop-coordinate and width in pixel).
input=$1
mode=$2
xcenter=$3
ycenter=$4
widthinpix=$5
bdir=$6
output=$7






# Find the different extintion names.
listhdus="$(astfits $input --listimagehdus)"






# Creat the build directory.
if ! [ -d $bdir ]; then
    mkdir $bdir
fi






# Crop the different extintion and append them together.
for i in $listhdus; do
    astcrop $input --mode=$mode --widthinpix --hdu=$i \
	    --width=$widthinpix --center=$xcenter,$ycenter \
	    --output=$bdir/$output --append
done
