#!/bin/bash
#
# Given a surface brightness image (from simulations for example), add
# noise with a given surface brightness limit.
#
# Usage:
#
#   ./add-noise-to-sb-img.sh input.fits 30.5 tmp-build-dir output.fits
#
# The '30.5' is an example surface brightness limit (SBL) for the
# noise. The SBL is assumed to be in mag/arcsec^2 at n-sigma over A
# arcsec^2, 'n' is the 'sblimitsigman' variable below and 'A' is the
# 'sblimitareaarcsec2' variable below.
#
# This script requires Gnuastro version 0.20 or above:
#   https://www.gnu.org/software/gnuastro/
#
# Copyright (C) 2023-2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this script. If not, see <http://www.gnu.org/licenses/>.





# Read input arguments
# --------------------
bdir=$3
output=$4
sblimit=$2
inputraw=$1
if [ x$inputraw = x ]; then
    echo "$0: no first argument (the name of the input FITS file)"; exit 1;
fi
if [ x$sblimit = x ]; then
    echo "$0: no second argument (the surface brightness limit of noise)"; exit 1;
fi
if [ x$bdir = x ]; then
    echo "$0: no third argument (the build directory)"; exit 1;
fi
if [ x$output = x ]; then
    echo "$0: no fourth argument (name of the output)"; exit 1;
fi





# Settings (parameters) go here
# -----------------------------

# Simulation
pixel_width_arcsec=1.67482

# Position on the sky (not relevant for study, just for a WCS
# structure).
center_dec=0
center_ra=180

# Surface brightness limit (SBL) parameters
#sblimit=30.5			# SBL (mag/arcsec^2) at n-sigma over A arcsec^2
sblimitsigman=3			# Multiple of sigma.
sblimitareaarcsec2=100		# Area SBL was measured on.

# In case you want to check the output measurement (by running
# NoiseChisel and measuring the surface brightness limit of the
# result).
validate=0







# Processing starts
# -----------------

# Abort the script in case of an error.
set -e
export LC_NUMERIC=C

# Directories to keep input and built files separate
if ! [ -d $bdir ]; then mkdir $bdir; fi

# Add a WCS structure to the image. To do that, we'll first make an
# empty image with a basic WCS structure, then we'll update the
# keywords and put the WCS over the raw downloaded input.
cdelt=$(echo $pixel_width_arcsec | awk '{print $1/3600}')
naxis1=$(astfits $inputraw -h0 --keyvalue=NAXIS1 -q)
naxis2=$(astfits $inputraw -h0 --keyvalue=NAXIS2 -q)
crpix1=$(echo $naxis1 | awk '{print int($1/2)+1}')
crpix2=$(echo $naxis2 | awk '{print int($1/2)+1}')
forwcs=$bdir/forwcs.fits
input=$bdir/input.fits
echo "1 1 1 4 0 0 0 0 1 1" \
    | astmkprof --mergedsize=3,3 -o$forwcs
astfits $forwcs \
	--update=CDELT1,$cdelt \
	--update=CDELT2,$cdelt \
	--update=CRPIX1,$crpix1 \
	--update=CRPIX2,$crpix2 \
	--update=CRVAL1,$center_ra \
	--update=CRVAL2,$center_dec
astarithmetic $inputraw -h0 --wcsfile=$forwcs -o$input
rm $forwcs

# Convert the image to counts (assuming a temporary zeropoint; the
# absolute value of the zeropoint is irrelevant so it is not in the
# input parameters at the top of this script).
zeropoint=22.5
counts=$bdir/counts.fits
pixarea=$(astfits $input --pixelareaarcsec2)
astarithmetic $input $zeropoint $pixarea sb-to-counts \
	      --output=$counts

# Convert the surface brightness limit to a noise level in counts.
sigma_counts=$(astarithmetic $sblimit $zeropoint \
			     $sblimitareaarcsec2 $pixarea x sqrt \
			     sb-to-counts $sblimitsigman / --quiet)
echo "Sigma (counts): $sigma_counts"

# Add noise to the counts image.
astarithmetic $counts $sigma_counts mknoise-sigma -o$output

# Run NoiseChisel and measure the surface brightness limit of this
# noise level.
if [ x$validate = x1 ]; then
    det=$bdir/det.fits
    cat=$bdir/cat.fits
    astnoisechisel $output -o$det
    astmkcatalog $det -hDETECTIONS --zeropoint=$zeropoint --sn -o$cat
    astfits $cat --keyvalue=SBLMAG
fi
