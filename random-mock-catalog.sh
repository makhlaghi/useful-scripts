#!/bin/sh

# Construct a mock catalog that MakeProfiles understands. The goal of this
# script is to generate a mock catalog with random values for different
# columns. For generating the random values a Python script is used. The
# main reason of using the Python script is that it is possible to specify
# any function for the random values. Run with `--help', or see description
# under `print_help' (below) for more.
#
# Original author:
#   Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2022 Free Software Foundation, Inc.
#
# Gnuastro is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Gnuastro is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with Gnuastro. If not, see <http://www.gnu.org/licenses/>.


# Exit the script in the case of failure
set -e

# To avoid floating points like '23,45' instead of '23.45'.
export LANG=C





# Default option values (can be changed with options on the command-line).
hdu=1
mode=img
quiet=""
output=""
tmpdir=""
keeptmp=0
number=10
version=@VERSION@
scriptname=@SCRIPT_NAME@

# Rest of parameters. Notation: xx_params="min,max,colname,function"
xpos="0,1,xpos,1"
ypos="0,1,ypos,1"
profile="3,3,profile,1"
axisratio="1,1,axisratio,1"
effradius="3,3,effradius,1"
magnitude="0,1,magnitude,1"
sersicindex="1,1,sersicindex,1"
truncation="10,10,truncation,1"
positionangle="0,0,positionangle,1"







# Output of `--usage' and `--help':
print_usage() {
    cat <<EOF
$scriptname: run with '--help' for list of options
EOF
}

print_help() {
    cat <<EOF
Usage: $scriptname [OPTION] FITS-files

This script is part of GNU Astronomy Utilities $version.

This script will generate a mock catalog that 'MakeProfiles' will
understand. The goal is to obtain the catalog with random values for the
necessary columns. The random values can follow a specific function that
the user can provide. To obtain the random values, a Python script is used.

Example:

    $ random-mock-catalog.sh \\
       --xpos "1,9000,x,1" \\
       --ypos "1,9000,y,1" \\
       --profile "1,4,profile,1" \\
       --effradius "2,8,effradius,1" \\
       --sersicindex "0.4,4,sersic,1" \\
       --positionangle "0,180,pangle,1" \\
       --axisratio "0.1,1,axisratio,1" \\
       --magnitude "15,23,magnitude,10**(0.01*x+5)" \\
       --truncation "10,10,truncation,1" \\
       --number 10000 --output cat.fits

For more information, please run any of the following commands. In
particular the first contains a very comprehensive explanation of this
script's invocation: expected input(s), output(s), and a full description
of all the options.

     Inputs/Outputs and options:           $ info $scriptname
     Full Gnuastro manual/book:            $ info gnuastro

If you couldn't find your answer in the manual, you can get direct help from
experienced Gnuastro users and developers. For more information, please run:

     $ info help-gnuastro

$scriptname options:
 Input:
  -h, --hdu=STR              HDU/extension of all input FITS files.
  -O, --mode=STR             Coordinates mode ('wcs' or 'img').
  -n, --number=INT           Number of objects.

 Notation: [params]=min,max,colname,function
  -x, --xpos=[params]          x-position coordinate.
  -y, --ypos=[params]          y-position coordinate.
  -f, --profile=[params]       Profile type.
  -r, --effradius=[params]     Effective radius.
  -s, --sersicindex=[params]   Sersic index.
  -Q, --axisratio=[params]     Axis ratio.
  -p, --positionangle=[params] Position angle.
  -m, --magnitude=[params]     Magnitude.
  -T, --truncation=[params]    Truncation.

 Output:
  -o, --output            Output mock catalog.
  -t, --tmpdir            Directory to keep temporary files.
  -k, --keeptmp           Keep temporal/auxiliar files.

 Operating mode:
  -?, --help              Print this help list.
      --cite              BibTeX citation for this program.
  -q, --quiet             Don't print any extra information in stdout.
  -V, --version           Print program version.

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

GNU Astronomy Utilities home page: http://www.gnu.org/software/gnuastro/

Report bugs to bug-gnuastro@gnu.org.
EOF
}





# Output of `--version':
print_version() {
    cat <<EOF
$scriptname (GNU Astronomy Utilities) $version
Copyright (C) 2020-2022 Free Software Foundation, Inc.
License GPLv3+: GNU General public license version 3 or later.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written/developed by Raul Infante-Sainz
EOF
}





# Functions to check option values and complain if necessary.
on_off_option_error() {
    if [ x"$2" = x ]; then
        echo "$scriptname: '$1' doesn't take any values"
    else
        echo "$scriptname: '$1' (or '$2') doesn't take any values"
    fi
    exit 1
}

check_v() {
    if [ x"$2" = x ]; then
        cat <<EOF
$scriptname: option '$1' requires an argument. Try '$scriptname --help' for more information
EOF
        exit 1;
    fi
}





# Separate command-line arguments from options. Then put the option
# value into the respective variable.
#
# OPTIONS WITH A VALUE:
#
#   Each option has three lines because we want to all common formats: for
#   long option names: `--longname value' and `--longname=value'. For short
#   option names we want `-l value', `-l=value' and `-lvalue' (where `-l'
#   is the short version of the hypothetical `--longname' option).
#
#   The first case (with a space between the name and value) is two
#   command-line arguments. So, we'll need to shift it two times. The
#   latter two cases are a single command-line argument, so we just need to
#   "shift" the counter by one. IMPORTANT NOTE: the ORDER OF THE LATTER TWO
#   cases matters: `-h*' should be checked only when we are sure that its
#   not `-h=*').
#
# OPTIONS WITH NO VALUE (ON-OFF OPTIONS)
#
#   For these, we just want the two forms of `--longname' or `-l'. Nothing
#   else. So if an equal sign is given we should definitely crash and also,
#   if a value is appended to the short format it should crash. So in the
#   second test for these (`-l*') will account for both the case where we
#   have an equal sign and where we don't.
inputs=""
while [ $# -gt 0 ]
do
    case "$1" in
        # Input parameters.
        -h|--hdu)            hdu="$2";                               check_v "$1" "$hdu";  shift;shift;;
        -h=*|--hdu=*)        hdu="${1#*=}";                          check_v "$1" "$hdu";  shift;;
        -h*)                 hdu=$(echo "$1"  | sed -e's/-h//');     check_v "$1" "$hdu";  shift;;
        -O|--mode)           mode="$2";                              check_v "$1" "$mode";  shift;shift;;
        -O=*|--mode=*)       mode="${1#*=}";                         check_v "$1" "$mode";  shift;;
        -O*)                 mode=$(echo "$1"  | sed -e's/-O//');    check_v "$1" "$mode";  shift;;
        -n|--number)         number="$2";                            check_v "$1" "$number";  shift;shift;;
        -n=*|--number=*)     number="${1#*=}";                       check_v "$1" "$number";  shift;;
        -n*)                 number=$(echo "$1"  | sed -e's/-n//');  check_v "$1" "$number";  shift;;

        -x|--xpos)           xpos="$2";                            check_v "$1" "$xpos";  shift;shift;;
        -x=*|--xpos=*)       xpos="${1#*=}";                       check_v "$1" "$xpos";  shift;;
        -x*)                 xpos=$(echo "$1"  | sed -e's/-x//');  check_v "$1" "$xpos";  shift;;
        -y|--ypos)           ypos="$2";                            check_v "$1" "$ypos";  shift;shift;;
        -y=*|--ypos=*)       ypos="${1#*=}";                       check_v "$1" "$ypos";  shift;;
        -y*)                 ypos=$(echo "$1"  | sed -e's/-y//');  check_v "$1" "$ypos";  shift;;
        -f|--profile)        profile="$2";                               check_v "$1" "$profile";  shift;shift;;
        -f=*|--profile=*)    profile="${1#*=}";                          check_v "$1" "$profile";  shift;;
        -f*)                 profile=$(echo "$1"  | sed -e's/-f//');     check_v "$1" "$profile";  shift;;
        -r|--effradius)      effradius="$2";                             check_v "$1" "$effradius";  shift;shift;;
        -r=*|--effradius=*)  effradius="${1#*=}";                        check_v "$1" "$effradius";  shift;;
        -r*)                 effradius=$(echo "$1"  | sed -e's/-r//');   check_v "$1" "$effradius";  shift;;
        -s|--sersicindex)    sersicindex="$2";                           check_v "$1" "$sersicindex";  shift;shift;;
        -s=*|--sersicindex=*)sersicindex="${1#*=}";                      check_v "$1" "$sersicindex";  shift;;
        -s*)                 sersicindex=$(echo "$1"  | sed -e's/-s//'); check_v "$1" "$sersicindex";  shift;;
        -Q|--axisratio)      axisratio="$2";                             check_v "$1" "$axisratio";  shift;shift;;
        -Q=*|--axisratio=*)  axisratio="${1#*=}";                        check_v "$1" "$axisratio";  shift;;
        -Q*)                 axisratio=$(echo "$1"  | sed -e's/-Q//');   check_v "$1" "$axisratio";  shift;;
        -p|--positionangle)     positionangle="$2";                            check_v "$1" "$positionangle";  shift;shift;;
        -p=*|--positionangle=*) positionangle="${1#*=}";                       check_v "$1" "$positionangle";  shift;;
        -p*)                    positionangle=$(echo "$1"  | sed -e's/-p//');  check_v "$1" "$positionangle";  shift;;
        -m|--magnitude)      magnitude="$2";                            check_v "$1" "$magnitude";  shift;shift;;
        -m=*|--magnitude=*)  magnitude="${1#*=}";                       check_v "$1" "$magnitude";  shift;;
        -m*)                 magnitude=$(echo "$1"  | sed -e's/-m//');  check_v "$1" "$magnitude";  shift;;
        -T|--truncation)     truncation="$2";                            check_v "$1" "$truncation";  shift;shift;;
        -T=*|--truncation=*) truncation="${1#*=}";                       check_v "$1" "$truncation";  shift;;
        -T*)                 truncation=$(echo "$1"  | sed -e's/-T//');  check_v "$1" "$truncation";  shift;;

        # Output parameters
        -k|--keeptmp)     keeptmp=1; shift;;
        -k*|--keeptmp=*)  on_off_option_error --keeptmp -k;;
        -t|--tmpdir)      tmpdir="$2";                          check_v "$1" "$tmpdir";  shift;shift;;
        -t=*|--tmpdir=*)  tmpdir="${1#*=}";                     check_v "$1" "$tmpdir";  shift;;
        -t*)              tmpdir=$(echo "$1" | sed -e's/-t//'); check_v "$1" "$tmpdir";  shift;;
        -o|--output)      output="$2";                          check_v "$1" "$output"; shift;shift;;
        -o=*|--output=*)  output="${1#*=}";                     check_v "$1" "$output"; shift;;
        -o*)              output=$(echo "$1" | sed -e's/-o//'); check_v "$1" "$output"; shift;;

        # Non-operating options.
        -q|--quiet)       quiet="--quiet"; shift;;
        -q*|--quiet=*)    on_off_option_error --quiet -q;;
        -?|--help)        print_help; exit 0;;
        -'?'*|--help=*)   on_off_option_error --help -?;;
        -V|--version)     print_version; exit 0;;
        -V*|--version=*)  on_off_option_error --version -V;;
        --cite)           print_citation; exit 0;;
        --cite=*)         on_off_option_error --cite;;

        # Unrecognized option:
        -*) echo "$scriptname: unknown option '$1'"; exit 1;;

        # Not an option (not starting with a `-'): assumed to be input FITS
        # file name.
        *) if [ x"$inputs" = x ]; then inputs="$1"; else inputs="$inputs $1"; fi; shift;;
    esac

done







# Basic sanity checks
# ===================

# If an input image is not given at all.
if [ x"$inputs" = x ]; then
    cat <<EOF
$scriptname: WARNING: no input FITS image provided. A mock catalogue with default parameters will be generated. Run with '--help' for more information on how to run
EOF
elif [ ! -f $inputs ]; then
    cat <<EOF
$scriptname: ERROR: $inputs, no such file or directory
EOF
    exit 1
fi

# If mode (--mode) is not given at all.
if [ x"$mode" = x ]; then
    cat <<EOF
$scriptname: no coordinate mode provided. The '--mode' ('-O') takes one of the following two values: 'img' (for pixel coordinates) or 'wcs' (for celestial coordinates)
EOF
    exit 1

# Make sure the value to '--mode' is either 'wcs' or 'img'. Note: '-o'
# means "or" and is preferred to '[ ] || [ ]' because only a single
# invocation of 'test' is done. Run 'man test' for more.
elif [ "$mode" = wcs     -o      $mode = "img" ]; then
    junk=1
else
    cat <<EOF
$scriptname: value to '--mode' (or '-O') is not recognized ('$mode'). This option takes one of the following two values: 'img' (for pixel coordinates) or 'wcs' (for celestial coordinates)
EOF
    exit 1
fi






# Define a temporal directory and the final output file
# -----------------------------------------------------
#
# Construct the temporary directory. If the user does not specify any
# directory, then a default one will be created. If the user set the
# directory, then make it. This directory will be deleted at the end of the
# script if the user does not want to keep it (with the `--keeptmp'
# option). The final output catalog is also defined here if the user does
# not provide an explicit name.
if [ x"$tmpdir" = x ]; then \
  tmpdir=$(pwd)/create-mock-catalog
fi

if [ -d "$tmpdir" ]; then
  junk=1
else
  mkdir -p "$tmpdir"
fi


# Output
if [ x"$output" = x ]; then
  output=mock-catalog.fits
fi





# Get the original inputs size
# ----------------------------
#
# In order to have the X and Y centers randomly distributed over the input
# imate, it is necessary to compute the original size of the input image. It is
# possible that the input image is compressed as '.fz' file. In these
# situations, the keywords with the original dimesion of the array are
# ZNAXIS1 and ZNAXIS2, instead of NAXIS1 and NAXIS2. The former are the
# real size of the array (as it were not compressed), the latter are the
# size after compressing the data (as a table). If there are not ZNAXIS
# keywords, output values are "n/a". So, they are removed with sed, and
# then count the number of remaining values. Overall, when there are 4
# remaining keywords, the data is compressed, otherwise use the simple
# NAXIS1 and NAXIS2 keywords (that are always into the header).

# Finally, when the size of the image has been obtained, the origina 'xpos'
# and 'ypos' paramters are modified with 'sed'. The replacement is the
# '0,1' by the '0,$xpos' (same for Y). So, basically the default '1' is
# replaced by the maximum pixel of the original image.

if [ x"$inputs" != x ]; then
    axises=$(astfits $inputs --hdu=$hdu --quiet \
                     --keyvalue ZNAXIS1,ZNAXIS2,NAXIS1,NAXIS2)
    naxises=$(echo $axises \
                   | sed 's/n\/a//g' \
                   | awk '{print NF}')
    if [ x$naxises = x4 ]; then
        xaxis=$(echo $axises | awk '{print $1}')
        yaxis=$(echo $axises | awk '{print $2}')
    else
        xaxis=$(echo $axises | awk '{print $3}')
        yaxis=$(echo $axises | awk '{print $4}')
    fi

# Replace the default '1' by the Maximum value
xpos=$(echo $xpos | sed -e"s/0,1/0,$xaxis/g")
ypos=$(echo $ypos | sed -e"s/0,1/0,$yaxis/g")

fi





# Parameters for the different columns
# ------------------------------------
#
# Read and save the necessary parameters for each column. Each column is
# generated using a Python script that needs some basic parameters:
# (1) minimum, (2) maximum, (3) column name, (4) function.
# As a consquence, each column will be a distribution of values between the
# minimum and maximum following the specified function. The output column
# name is specified by the colname. An exception to this is the first
# column that correspond to the IDs of the different objects. The IDS
# columns is just a label/counter that correspond to the first column.

ids_params="1,$number,ids,1"

ids_min=$(echo $ids_params | awk 'BEGIN{FS=","} {print $1}')
ids_max=$(echo $ids_params | awk 'BEGIN{FS=","} {print $2}')
ids_colname=$(echo $ids_params | awk 'BEGIN{FS=","} {print $3}')
ids_function=$(echo $ids_params | awk 'BEGIN{FS=","} {print $4}')

xpos_min=$(echo $xpos | awk 'BEGIN{FS=","} {print $1}')
xpos_max=$(echo $xpos | awk 'BEGIN{FS=","} {print $2}')
xpos_colname=$(echo $xpos | awk 'BEGIN{FS=","} {print $3}')
xpos_function=$(echo $xpos | awk 'BEGIN{FS=","} {print $4}')

ypos_min=$(echo $ypos | awk 'BEGIN{FS=","} {print $1}')
ypos_max=$(echo $ypos | awk 'BEGIN{FS=","} {print $2}')
ypos_colname=$(echo $ypos | awk 'BEGIN{FS=","} {print $3}')
ypos_function=$(echo $ypos | awk 'BEGIN{FS=","} {print $4}')

profile_min=$(echo $profile | awk 'BEGIN{FS=","} {print $1}')
profile_max=$(echo $profile | awk 'BEGIN{FS=","} {print $2}')
profile_colname=$(echo $profile | awk 'BEGIN{FS=","} {print $3}')
profile_function=$(echo $profile | awk 'BEGIN{FS=","} {print $4}')

effradius_min=$(echo $effradius | awk 'BEGIN{FS=","} {print $1}')
effradius_max=$(echo $effradius | awk 'BEGIN{FS=","} {print $2}')
effradius_colname=$(echo $effradius | awk 'BEGIN{FS=","} {print $3}')
effradius_function=$(echo $effradius | awk 'BEGIN{FS=","} {print $4}')

sersicindex_min=$(echo $sersicindex | awk 'BEGIN{FS=","} {print $1}')
sersicindex_max=$(echo $sersicindex | awk 'BEGIN{FS=","} {print $2}')
sersicindex_colname=$(echo $sersicindex | awk 'BEGIN{FS=","} {print $3}')
sersicindex_function=$(echo $sersicindex | awk 'BEGIN{FS=","} {print $4}')

positionangle_min=$(echo $positionangle | awk 'BEGIN{FS=","} {print $1}')
positionangle_max=$(echo $positionangle | awk 'BEGIN{FS=","} {print $2}')
positionangle_colname=$(echo $positionangle | awk 'BEGIN{FS=","} {print $3}')
positionangle_function=$(echo $positionangle | awk 'BEGIN{FS=","} {print $4}')

axisratio_min=$(echo $axisratio | awk 'BEGIN{FS=","} {print $1}')
axisratio_max=$(echo $axisratio | awk 'BEGIN{FS=","} {print $2}')
axisratio_colname=$(echo $axisratio | awk 'BEGIN{FS=","} {print $3}')
axisratio_function=$(echo $axisratio | awk 'BEGIN{FS=","} {print $4}')

magnitude_min=$(echo $magnitude | awk 'BEGIN{FS=","} {print $1}')
magnitude_max=$(echo $magnitude | awk 'BEGIN{FS=","} {print $2}')
magnitude_colname=$(echo $magnitude | awk 'BEGIN{FS=","} {print $3}')
magnitude_function=$(echo $magnitude | awk 'BEGIN{FS=","} {print $4}')

truncation_min=$(echo $truncation | awk 'BEGIN{FS=","} {print $1}')
truncation_max=$(echo $truncation | awk 'BEGIN{FS=","} {print $2}')
truncation_colname=$(echo $truncation | awk 'BEGIN{FS=","} {print $3}')
truncation_function=$(echo $truncation | awk 'BEGIN{FS=","} {print $4}')





# Temporary files
# ---------------
#
# Name of temporary files. They correspond to the different column tables
# that are later concatenated to construct the final output table.
ids_tmp=$tmpdir/$ids_colname.fits
xpos_tmp=$tmpdir/$xpos_colname.fits
ypos_tmp=$tmpdir/$ypos_colname.fits
xpos_tmp_inpix=$tmpdir/xcentersinpix.fits
ypos_tmp_inpix=$tmpdir/ycentersinpix.fits
profile_tmp=$tmpdir/$profile_colname.fits
axisratio_tmp=$tmpdir/$axisratio_colname.fits
effradius_tmp=$tmpdir/$effradius_colname.fits
magnitude_tmp=$tmpdir/$magnitude_colname.fits
truncation_tmp=$tmpdir/$truncation_colname.fits
sersicindex_tmp=$tmpdir/$sersicindex_colname.fits
positionangle_tmp=$tmpdir/$positionangle_colname.fits





# Create the column catalogs
# --------------------------
#
# Create each necessary column. The first one is the IDS so it is generated
# just with a 'seq' command. The rest of columns are generated using a
# Python script and the already read parameters

# 1. IDS
for n in $(seq $ids_min $ids_max); do echo $n; done \
    | asttable -c'arith $1 float32' \
               --output $ids_tmp \
               --colmetadata=1,$ids_colname,none,"IDS"
# 2. XPOS
random-values-distribution.py --number=$number \
                              --colname=$xpos_colname \
                              --range=$xpos_min,$xpos_max \
                              --function="def f(x): return $xpos_function" \
                              --output=$xpos_tmp_inpix
# 3. YPOS
random-values-distribution.py --number=$number \
                              --colname=$ypos_colname \
                              --range=$ypos_min,$ypos_max \
                              --function="def f(x): return $ypos_function" \
                              --output=$ypos_tmp_inpix
# 4. PROFILE
random-values-distribution.py --number=$number --dtype int --decimals 0 \
                              --colname=$profile_colname \
                              --range=$profile_min,$profile_max \
                              --function="def f(x): return $profile_function" \
                              --output=$profile_tmp
# 5. EFFRADIUS
random-values-distribution.py --number=$number \
                              --colname=$effradius_colname \
                              --range=$effradius_min,$effradius_max \
                              --function="def f(x): return $effradius_function" \
                              --output=$effradius_tmp
# 6. SERSICINDEX
random-values-distribution.py --number=$number \
                              --colname=$sersicindex_colname \
                              --range=$sersicindex_min,$sersicindex_max \
                              --function="def f(x): return $sersicindex_function" \
                              --output=$sersicindex_tmp
# 7. POSITIONANGLE
random-values-distribution.py --number=$number \
                              --colname=$positionangle_colname \
                              --range=$positionangle_min,$positionangle_max \
                              --function="def f(x): return $positionangle_function" \
                              --output=$positionangle_tmp
# 8. AXISRATIO
random-values-distribution.py --number=$number \
                              --colname=$axisratio_colname \
                              --range=$axisratio_min,$axisratio_max \
                              --function="def f(x): return $axisratio_function" \
                              --output=$axisratio_tmp
# 9. MAGNITUDE
random-values-distribution.py --number=$number \
                              --colname=$magnitude_colname \
                              --range=$magnitude_min,$magnitude_max \
                              --function="def f(x): return $magnitude_function" \
                              --output=$magnitude_tmp
# 10. TRUNCATION
random-values-distribution.py --number=$number \
                              --colname=$truncation_colname \
                              --range=$truncation_min,$truncation_max \
                              --function="def f(x): return $truncation_function" \
                              --output=$truncation_tmp





# Transform IMG to WCS center coordinates
# ---------------------------------------
#
# The original coordinates have been computed in pixels. Transform them to
# WCS if the user specifies it with the option '--mode wcs'.  Here, this is
# done by using the WCS information from the original input image. If the
# original coordinates were done in IMG, then just use them.
if [ "$mode" = wcs ]; then
  xypos_tmp_inpix=$tmpdir/xycentersinpix.fits
  asttable $xpos_tmp_inpix --output $xypos_tmp_inpix \
            --catcolumnfile $ypos_tmp_inpix --catcolumnhdu 1
  xycenterpix=$(asttable $xypos_tmp_inpix --quiet \
                        | asttable  --column='arith $1 $2 img-to-wcs' \
                                    --wcsfile=$inputs --wcshdu=$hdu $quiet)

  # Create two columns table with the coordinates in WCS
  echo "$xycenterpix" \
       | awk '{print $1}' \
       | asttable -c'arith $1 float32' --output $xpos_tmp \
                  --colmetadata=1,$xpos_colname,$mode,$mode
  echo "$xycenterpix" \
       | awk '{print $2}' \
       | asttable -c'arith $1 float32' --output $ypos_tmp \
                  --colmetadata=1,$ypos_colname,$mode,$mode

else
  ln -fs $xpos_tmp_inpix $xpos_tmp
  ln -fs $ypos_tmp_inpix $ypos_tmp
fi





# Concatenate all columns into a single output file
# -------------------------------------------------
#
# Once all columns have been generated, concatenated them into the single
# final output catalog. Keep the original column names. The order of the
# columns is important: it is the order that 'MakeProfiles' assumes.
asttable $ids_tmp --hdu 1 \
  --catcolumnfile $xpos_tmp --catcolumnhdu 1 \
  --catcolumnfile $ypos_tmp --catcolumnhdu 1 \
  --catcolumnfile $profile_tmp --catcolumnhdu 1 \
  --catcolumnfile $effradius_tmp --catcolumnhdu 1 \
  --catcolumnfile $sersicindex_tmp --catcolumnhdu 1 \
  --catcolumnfile $positionangle_tmp --catcolumnhdu 1 \
  --catcolumnfile $axisratio_tmp --catcolumnhdu 1 \
  --catcolumnfile $magnitude_tmp --catcolumnhdu 1 \
  --catcolumnfile $truncation_tmp --catcolumnhdu 1\
  --catcolumnrawname --output $output





# Remove temporary files
# ----------------------
#
# If the user does not specify to keep the temporal files with the option
# `--keeptmp', then remove the whole directory.
if [ $keeptmp = 0 ]; then
    rm -r $tmpdir
fi
