#!/bin/sh
#
# Allow customized shells for each rule within a Makefile. For more on
# the purpose of this function, see the description in the
# 'print_help' function below.
#
# Copyright (C) 2023-2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic settings
# --------------
# Stop the script if there are any errors.
set -e





# Variable initializations
ram=""
recipe=""
shell=/bin/bash





# For debugging.
#echo "input: $@"





# Interface functions
print_help() {
    cat <<EOF
Usage: $scriptname [OPTIONS] -- COMMAND

This script is meant to be given to the 'SHELL' variable within
Makefiles. Its goal is to be a high-level wrapper for customizations
of each recipe within a single Makefile. This is necessary in
scenarios like below:

 - When using Make to run commands in a HPC cluster. In such cases,
   the job-scheduler like Slurm or Sun Grid System (SEG) engines are
   actually in charge of putting the recipes in a queue to be executed
   on certain nodes.

 - When each recipe is expected to be run with a different
   interpreter/shell. For example one recipe is written in Python,
   while another is written in Perl, but most other recipes are simple
   Bash.

Below you can see an example Makefile that uses this script (and the
script is located in the same directory as the Makefile, feel free to
change this):

	------------------------
	# '.SHELLFLAGS' should not contain any equal sign!
	SHELL:=$(shell pwd)/makefile-recipe-customize.sh
	.SHELLFLAGS := --ram 4 --shell /bin/bash --
	.ONESHELL:
	all:
		EXECCONF_SHELL=/usr/bin/bash
		EXECCONF_RAM=2
		abc=VALUE
		echo def; echo 1 2 3 | awk '{print $$1+$$2+$$3}'
	------------------------

This script can be customized with the options below, which are
intended to be given to the '.SHELLFLAGS' variable within the Makefile
(and thus applied to all recipes within the Makefile). In case you
would like to customize them on a per-recipe basis, you need to start
the recipe with special shell variables that have 'EXECCONF_' as a
prefix. The suffix is the option name in full-caps.

  - For example if you want a custom shell for a certain recipe, you
    should start that recipe with 'EXECCONF_SHELL=/usr/bin/perl' on a
    separate line. This will replace the value given to the '--shell'
    command within your '.SHELLFLAGS' Makefile variable.

$scriptname options:

 Custom settings:
  -r, --ram=FLT           Gigabytes of RAM to pass to job scheduler.
  -s, --shell=STR         Location of interpretter to use.

 Operating mode:
  -?, --help              Print this help list.

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

Report bugs to mohammad@akhlaghi.org.
EOF
}

on_off_option_error() {
    if [ x"$2" = x ]; then
        echo "$0: '$1' doesn't take any values"
    else
        echo "$0: '$1' (or '$2') doesn't take any values"
    fi
    exit 1
}

check_v() {
    if [ x"$2" = x ]; then
        cat <<EOF
$0: option '$1' requires an argument. Try '$scriptname --help' for more information
EOF
        exit 1;
    fi
}





# Separate command-line arguments from options and put the option values
# into the respective variables.
#
# OPTIONS WITH A VALUE:
#
#   Each option has three lines because we take into account the three common
#   formats:
#   For long option names, '--longname value' and '--longname=value'.
#   For short option names, '-l value', '-l=value' and '-lvalue'
#   (where '-l' is the short version of the hypothetical '--longname option').
#
#   The first case (with a space between the name and value) is two
#   command-line arguments. So, we'll need to shift it twice. The
#   latter two cases are a single command-line argument, so we just need to
#   "shift" the counter by one.
#
#   IMPORTANT NOTE: the ORDER OF THE LATTER TWO cases matters: '-h*' should be
#   checked only when we are sure that its not '-h=*').
#
# OPTIONS WITH NO VALUE (ON-OFF OPTIONS)
#
#   For these, we just want the forms of '--longname' or '-l'. Nothing
#   else. So if an equal sign is given we should definitely crash and also,
#   if a value is appended to the short format it should crash. So in the
#   second test for these ('-l*') will account for both the case where we
#   have an equal sign and where we don't.
while [ $# -gt 0 ]
do
    case "$1" in
	# The necessary RAM to use.
	-r|--ram)     ram="$2";                          check_v "$1" "$ram";  shift;shift;;
	-r=*|--ram=*) ram="${1#*=}";                     check_v "$1" "$ram";  shift;;
	-r*)          ram=$(echo "$1" | sed -e's/-r//'); check_v "$1" "$ram";  shift;;

	# The necessary shell to use
	-s|--shell)     shell="$2";                          check_v "$1" "$shell";  shift;shift;;
	-s=*|--shell=*) shell="${1#*=}";                     check_v "$1" "$shell";  shift;;
	-s*)            shell=$(echo "$1" | sed -e's/-r//'); check_v "$1" "$shell";  shift;;

	# Print help message.
        -?|--help)        print_help; exit 0;;
        -'?'*|--help=*)   on_off_option_error --help -?;;

	# Command to pass onto the script
	--)  recipe="$2"; break;;

	# Un-known option.
	-*) echo "$0: unknown option '$1'"; exit 1;;
    esac
done


# Sanity checks:
if [ x"$recipe" = x ]; then
    cat <<EOF
$0: no recipe given! Please place the recipe (set of commands you would like to exectue) after a '--' as the last argument (within double quotations)
EOF
fi





# Parse the first set of values given within the command to see if
# they are recipe-specific customizations.
for token in $recipe; do
    case $token in
	EXECCONF_RAM=*)   ram="${token#*=}";;
	EXECCONF_SHELL=*) shell="${token#*=}";;
	*) break;
    esac
done




# For check
echo "SHELL:   $shell"
echo "RAM:     $ram"
echo "RECIPE:  ###$recipe###"
$shell -c "$recipe"
echo DONE
