# Find the best truncation radius for the full parametric space of
# Sersic index, effective radius and magnitude, based on an input
# catalog. The output can be given to the
# './mkprof-truncation-match-build.sh' script which is in the same
# repository as this: https://gitlab.cefca.es/gnuastro/scripts
#
# Usage:
#
#     bdir=/path/to/temporary/directory
#     make -f mkprof-truncation.mk -j$(nproc) \
#          OUT=/path/to/output/truncations.fits \
#          CAT=/path/to/large/catalog.fits \
#          M_COL=9 R_COL=5 N_COL=6 \
#          PIXSCALE_DEG=0.5/3600 \
#          BDIR=$bdir \
#          OVSAMP=3
#     rm $bdir
#
# The main input catalog is 'CAT' which should have the following
# three columns. All columns can be identified by number or name:
#   - M_COL: Magnitude.
#   - N_COL: Sersic index.
#   - R_COL: Effective radius (in pixels).
#
# The parameters to the script are:
#
#   - PIXSCALE_DEG: pixel scale (in units of degrees/pix) of the
#     detector you want to simulate for (this is necessary to define
#     the surface brighntess!). By "final image", we mean after you
#     have convolved the MakeProfiles output with the PSF and
#     undersampled the convolved image. The given value will be
#     written in the FITS headers of the output and used by
#     './mkprof-truncation-match-build.sh'.
#
#   - OVSAMP: the oversampling factor (that the first image and PSF
#     will be created in), in relation to the detector (this should be
#     an odd integer, for example 3 or 5).
#
#   - OUT: name of the output file.
#
#   - BDIR: Name of directory containing temporary outputs. You can
#     delete this directory afterwards.
#
# Copyright (C) 2024-2024 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.





# Build directory (for outputs)
BDIR=build

# Output name
OUT=truncations.fits

# Surface brightness threshold
SB_THRESH=35

# Name of input table and relevant columns in table (the radius column
# is assumed ot be in pixels).
CAT=cat.fits
M_COL=mag
R_COL=r_e
N_COL=sersic_index
PIXSCALE_DEG=0.5/3600

# Internal parameters
# - The axis ratio:
#   - With the same total magnitude, a more circular profile
#     will distribute the same total flux between more pixels,
#     so its outer pixels will go fainter. Therefore The
#     resulting truncation radius will be:
#     - An over-estimation for more circular profiles.
#     - An under-estimation for more elliptical profiles.
OVSAMP=3
AXISRATIO=0.7








# Make rules
# ----------
#
# There are no more parameters beyond this point. The group of
# non-commented lines below are basic GNU Make settings.
all: $(OUT)
.ONESHELL:
.SECONDEXPANSION:
.SHELLFLAGS = -ec
$(BDIR):; mkdir $@





# Find the minimum, maximum, and from them build the range of values
# for each parameter.
r_min := $(shell aststatistics $(CAT) -c$(R_COL) --minimum)
r_max := $(shell aststatistics $(CAT) -c$(R_COL) --maximum)
n_min := $(shell aststatistics $(CAT) -c$(N_COL) --minimum)
n_max := $(shell aststatistics $(CAT) -c$(N_COL) --maximum)
m_min := $(shell aststatistics $(CAT) -c$(M_COL) --minimum)
m_max := $(shell aststatistics $(CAT) -c$(M_COL) --maximum)
rs := $(shell echo $(r_min) $(r_max) \
                   | awk '{st = $$1<0.2 ? 0.2 : $$1; \
                           for(r=st; r<1 && r<=$$2; r+=0.2) \
                             printf "%.1f ", r; \
                           for(r=1; r<10 && r<=$$2; r+=1) \
                             printf "%d ", r; \
                           for(r=10; r<20 && r<=$$2; r+=2) \
                             printf "%d ", r; \
                           for(r=20; r<50 && r<=$$2; r+=5) \
                             printf "%d ", r; \
                           for(r=50; r<100 && r<=$$2; r+=10) \
                             printf "%d ", r; \
                           for(r=100; r<=$$2; r+=50) \
                             printf "%d ", r; \
                          } ')
ns := $(shell echo $(n_min) $(n_max) \
                   | awk '{ \
                           for(n=$$1; n<1.5 && n<=$$2; n+=0.2) \
                             printf "%.1f ", n; \
                           for(n=1.5; n<2.5 && n<=$$2; n+=0.3) \
                             printf "%.1f ", n; \
                           for(n=2.5; n<4.5 && n<=$$2; n+=0.5) \
                             printf "%.1f ", n; \
                           for(n=4.5; n<=$$2; n+=0.6) \
                             printf "%.1f ", n; \
                          } ')
ms := $(shell echo $(m_min) $(m_max) \
                   | awk '{ \
                           for(m=$$1; m<20 && m<=$$2; m+=2) \
	                     printf "%d ", m; \
                           for(m=20; m<30 && m<=$$2; m+=1) \
	                     printf "%d ", m; \
                           for(m=30; m<=$$2; m+=3) \
	                     printf "%d ", m; \
                          } ')





# Truncation for each profile.
profiles=$(foreach r, $(rs), \
          $(foreach n, $(ns), \
           $(foreach m, $(ms), \
             $(BDIR)/$(r)_$(n)_$(m).txt) ) )
$(profiles):$(BDIR)/%.txt: | $(BDIR)

#	Variables to simplify the later commands.
	@r=$(word 1, $(subst _, ,$*))
	n=$(word 2, $(subst _, ,$*))
	m=$(word 3, $(subst _, ,$*))

#	Set the zero point of the image: the zero point is arbitrary:
#	makes no difference in the output. It is set to a very faint
#	value so low surface brithness pixels do not hit the
#	floating-point error regime.
	zp=35

#	The half-interval is used around $(SB_THRESH) when applying
#	the threshold and when checking the output, to account for
#	strong gradients (where the faintest pixel brighter than the
#	threshold can be too distant from the threshold; leading to an
#	infinite loop of creating profiles).
	halfinterval=0.5

#	Set the initial truncation radius.
	tr=5
	needsbuild=1

#	For a very small profile (less than 2 pixels), the gradient is
#	very strong, so we need to modify the following values.
	issmall=$$(echo $$r | awk '{if($$1<1) print 1; else print 0}')
	if [ $$issmall = 1 ]; then tr=10; fi

#	Loop over incremental size increases
	while [ $$needsbuild = 1 ]; do

#	  Report the status.
	  echo "r=$$r; n=$$n; m=$$m testing truncation: $$tr"

#	  Build the profile. We set the output of MakeProfiles to
#	  'mock', but because we have used '--nomerged', only the
#	  individual profile is created and that has a '0_' at the
#	  start of its name (in the same directory as the output).
	  mock=$(subst .txt,.fits,$@)
	  indiv=$(subst .txt,.fits,$(subst $*,0_$*,$@))
	  echo "1 0 0 1 $$r $$n 0 $(AXISRATIO) $$m $$tr" \
	       | astmkprof --nomerged --oversample=$(OVSAMP) \
	                   --zeropoint=$$zp --output=$$mock --individual \
	                   --quiet --cdelt=$(PIXSCALE_DEG),$(PIXSCALE_DEG)

#	  To help in finding files when debugging (the actual '$mock'
#	  file is not made by MakeProfiles).
	  mv $$indiv $$mock

#	  Convert image to surface brightness and only keep the pixels
#	  that are brighter than $(SB_THRESH).
	  sb=$(subst .txt,-sb.fits,$@)
	  sbthresh=$(subst .txt,-sb-thresh.fits,$@)
	  pixarea=$$(astfits $$mock --pixelareaarcsec2 -q)
	  astarithmetic $$mock $$zp $$pixarea counts-to-sb \
	                tofile-$$sb set-sb \
	                sb sb $(SB_THRESH) $$halfinterval + ge nan where \
	                trim -o$$sbthresh --quiet

#	  If there was no pixels, or just one pixel, above the
#	  threshold the trimmed image, break out of the loop.
	  na1=$$(astfits $$sbthresh --keyvalue=NAXIS1 --quiet)
	  if [ $$na1 = 0 ] || [ $$na1 = 1 ]; then break; fi

#	  If the truncation that is being tested didn't reach the
#	  desired SB threshold, then the faintest SB on the
#	  thresholded and non-thresholded images will be the sama. In
#	  this case, we need at least another pass in the loop (for a
#	  larger truncation). Otherwise (the SB profile went larger
#	  than the threshold), then everything is fine and we can
#	  break out.
	  maxsb=$$(aststatistics $$sb --maximum)
	  maxsbt=$$(aststatistics $$sbthresh --maximum)
	  if [ $$maxsb = $$maxsbt ]; then

#	    Based on the difference between the threshold and the
#	    faintest SB, increase the truncation radius.
	    newtr=$$(aststatistics $$sbthresh --maximum \
	                | awk '{\
	                        tr='$$tr'; diff=$(SB_THRESH)-$$1; \
	                        if      (diff>4) print 3*tr; \
	                        else if (diff>2) print 2*tr; \
	                        else             print 1.5*tr; \
	                        }')
	    tr=$$newtr

#	  The theshold was good! Break out of the loop.
	  else break
	  fi
	done

#	Extract the necessary truncation from the radius and size of
#	this image.
	if [ $$na1 = 0 ]; then
	  t=0
	elif [ $$na1 = 1 ]; then
	  t=$$(echo $$r | awk '{print 1/$$1}')
	else
	  t=$$(astfits $$sbthresh --keyvalue=NAXIS1 --quiet \
	                   | awk '{rad=($$1-1)/2; \
	                           print rad/('$$r'*$(OVSAMP))}')
	fi

#	Print the result, write the output and clean up.
	echo "r=$$r; n=$$n; m=$$m  -->  trunc: $$t" # Others are quiet
	printf "%-5.1f %-10.2f %-5d %.2f\n" $$r $$n $$m $$t > $@
	rm $$sb $$mock $$sbthresh





# Final target.
$(OUT): $(profiles)

#	Build the table.
	echo "# Column 1: r_e   [pix,f32] Effective radius" > $@.txt
	echo "# Column 2: n     [n/a,f32] Sersic index"      >> $@.txt
	echo "# Column 3: mag   [n/a,int16] Magnitude"       >> $@.txt
	echo "# Column 4: trunc [r_e,f32] Truncation radius" >> $@.txt
	cat $(BDIR)/*.txt >> $@.txt
	asttable $@.txt -o$@
	rm $@.txt

#	Extract the pixel scale as a single number.
	ps=$$(echo 1 | awk '{print '$(PIXSCALE_DEG)'}')

#	Add the necessary metadata.
	astfits $@ --write=/,"MKPROF-TRUNCATIONS.mk parameters" \
	        --write=SBTHRESH,$(SB_THRESH),"mag/arcsec^2","SB threshold." \
	        --write=PIXSCALE,$$ps,"deg","Pixel scale (deg/pix) assumed." \
	        --write=OVSAMP,$(OVSAMP),"multiple","Oversample assumed." \
	        --write=AXRATIO,$(AXISRATIO),"frac","Axis ratio assumed."
