/* Small program to convert SIP header keywords in all the extensions
   of a FITS file (in place) into TPV. This program is written with
   Gnuastro 0.14 and will not work with later versions (very minor
   modifications to the library API will be necessary).

   Assuming your FITS file is called 'FILENAME.fits' you can compile
   and run it in one command using Gnuastro's BuildProgram like this
   (you can use a '--quiet' option to avoid compilation information):

       astbuildprog sip-to-tpv-inplace.c FILE.fits

   If your input file doesn't have a '.fits' suffix, you can make a
   symbolic link to it that has this suffix like below. In this way,
   you won't need to make any extra copy (this is one of the things
   that has been improved in later versions of Gnuastro).

       ln -s FILE FILE.fits

   With BuildProgram, you won't have to worry about linking issues
   either (the full compile command, including all the linkings, is
   printed on standard output to inspect).

   This program can also optionally do a quality check by converting a
   grid of pixel coordinates to RA/DEC using the original and
   converted distortions and saving the result in a table. To do that,
   in the 'main', function, set 'p.qualitycheck=1'. The parameters (of
   the input pixel grid can be set in the 'quality_check' function).

   However, since compilation takes much longer (~1 sec) than the
   actual job of this program (~0.005 sec for each HDU). Hence, if you
   have many files, its much better to compile it in a single/separate
   command first, then run it on all the files.

       astbuildprog sip-to-tpv-inplace.c --onlybuild
       for f in /path/to/files-*.fits; do ./sip-to-tpv-inplace $f; done

   This program can also operate in parallel. However, due to the very
   small CPU footprint of each conversion, multi-threading can make
   the program slower! Overheads of multi-threading management can be
   larger than the actual processing time! So always check once before
   considering this.

   Copyright (C) 2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   (LGPL) as published by the Free Software Foundation, either version
   3 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License with this Makefile. If not, see
   <http://www.gnu.org/licenses/>.*/
#include <stdio.h>
#include <stdlib.h>

#include <gnuastro/wcs.h>
#include <gnuastro/fits.h>
#include <gnuastro/type.h>
#include <gnuastro/list.h>
#include <gnuastro/threads.h>





/* For errors printed when aborting program. */
#define PACKAGE_BUGREPORT "mohammad@akhlaghi.org"





/* Internal structure to avoid passing too many arguments. */
struct params
{
  int             argc;		/* Number of input arguments.  */
  char          **argv;		/* Each argument.              */
  char       *progname;		/* Name of program.            */
  char       *filename;		/* Input filename.             */
  size_t        numhdu;		/* Number of HDUs.             */
  size_t    numthreads;		/* Number of threads.          */
  int     qualitycheck;		/* ==1: a quality check table. */
};





/* Basic sanity checks before program starts. */
static void
sanity_check_and_prepare(struct params *p)
{
  void *nth=&p->numthreads;

  /* Sanity check: we can only accept two or three arguments. */
  if(p->argc!=2 && p->argc!=3)
    {
      fprintf(stderr, "%s: incorrect number of arguments.\n"
	      "Usage:\n"
	      "   %s filename\n"
	      "   %s filename number-of-threads \n"
	      "In the first, all threads on the system will be used, "
	      "in the second, your given number of threads will be used",
	      p->progname, p->progname, p->progname);
      exit(EXIT_FAILURE);
    }

  /* Set the number of threads. */
  if(p->argc==2) p->numthreads=1;
  else
    {
      if( gal_type_from_string(&nth, p->argv[2], GAL_TYPE_SIZE_T) )
	{
	  fprintf(stderr, "%s: '%s' couldn't be read as an integer.\n",
		  p->progname, p->argv[2]);
	  exit(EXIT_FAILURE);
	}
    }

  /* Find the number of HDUs in the file. */
  p->numhdu=gal_fits_hdu_num(p->filename);
}





/* Delete all the conflicting WCS-related keywords from the HDU (to
   write the new ones afterwards). This is done by printing the WCS
   strings and removing all the relevant keywords that exist there.

   Resetting the status is necessary in case the keywords don't
   exist.*/
static void
delete_old_keys(fitsfile *fptr)
{
  int status;
  status=0; fits_delete_key(fptr, "CRPIX1",  &status);
  status=0; fits_delete_key(fptr, "CRPIX2",  &status);
  status=0; fits_delete_key(fptr, "CD1_1",   &status);
  status=0; fits_delete_key(fptr, "CD1_2",   &status);
  status=0; fits_delete_key(fptr, "CD2_1",   &status);
  status=0; fits_delete_key(fptr, "CD2_2",   &status);
  status=0; fits_delete_key(fptr, "CUNIT1",  &status);
  status=0; fits_delete_key(fptr, "CUNIT2",  &status);
  status=0; fits_delete_key(fptr, "CTYPE1",  &status);
  status=0; fits_delete_key(fptr, "CTYPE2",  &status);
  status=0; fits_delete_key(fptr, "CRVAL1",  &status);
  status=0; fits_delete_key(fptr, "CRVAL2",  &status);
  status=0; fits_delete_key(fptr, "LONPOLE", &status);
  status=0; fits_delete_key(fptr, "LATPOLE", &status);
  status=0; fits_delete_key(fptr, "MJDREF",  &status);
  status=0; fits_delete_key(fptr, "RADESYS", &status);
  status=0; fits_delete_key(fptr, "EQUINOX", &status);
  status=0; fits_delete_key(fptr, "A_ORDER", &status);
  status=0; fits_delete_key(fptr, "A_0_2",   &status);
  status=0; fits_delete_key(fptr, "A_0_3",   &status);
  status=0; fits_delete_key(fptr, "A_1_1",   &status);
  status=0; fits_delete_key(fptr, "A_1_2",   &status);
  status=0; fits_delete_key(fptr, "A_2_0",   &status);
  status=0; fits_delete_key(fptr, "A_2_1",   &status);
  status=0; fits_delete_key(fptr, "A_3_0",   &status);
  status=0; fits_delete_key(fptr, "B_ORDER", &status);
  status=0; fits_delete_key(fptr, "B_0_2",   &status);
  status=0; fits_delete_key(fptr, "B_0_3",   &status);
  status=0; fits_delete_key(fptr, "B_1_1",   &status);
  status=0; fits_delete_key(fptr, "B_1_2",   &status);
  status=0; fits_delete_key(fptr, "B_2_0",   &status);
  status=0; fits_delete_key(fptr, "B_2_1",   &status);
  status=0; fits_delete_key(fptr, "B_3_0",   &status);
}





static void
correct_pc_to_cd(fitsfile *fptr)
{
  int status;
  status=0; fits_delete_str(fptr, "CDELT1", &status);
  status=0; fits_delete_str(fptr, "CDELT2", &status);
  status=0; fits_modify_name(fptr, "PC1_1", "CD1_1", &status);
  status=0; fits_modify_name(fptr, "PC1_2", "CD1_2", &status);
  status=0; fits_modify_name(fptr, "PC2_1", "CD2_1", &status);
  status=0; fits_modify_name(fptr, "PC2_2", "CD2_2", &status);
}





/* Set the WCS structure to use the CD matrix.

   IMPORTANT NOTE: this function was staticly built in Gnuastro
   0.14. In the development branch of Gnuastro this has already been
   fixed (so it is available to any Gnuastro library user. But version
   0.15 isn't released yet. */
void
gal_wcs_to_cd(struct wcsprm *wcs)
{
  size_t i, j, naxis;

  /* If there is on WCS, then don't do anything. */
  if(wcs==NULL) return;

  /* 'wcs->altlin' identifies which rotation element is being used (PCi_j,
     CDi_J or CROTAi). For PCi_j, the first bit will be 1 (==1), for CDi_j,
     the second bit is 1 (==2) and for CROTAi, the third bit is 1 (==4). */
  naxis=wcs->naxis;
  switch(wcs->altlin)
    {
   /* PCi_j: Convert it to CDi_j. */
    case 1:

      /* Fill in the CD matrix and correct the PC and CDELT arrays. We have
         to do this because ultimately, WCSLIB will be writing the PC and
         CDELT keywords, even when 'altlin' is 2. So effectively we have to
         multiply the PC and CDELT matrices, then set cdelt=1 in all
         dimensions. This is actually how WCSLIB reads a FITS header with
         only a CD matrix. */
      for(i=0;i<naxis;++i)
        {
          for(j=0;j<naxis;++j)
            wcs->cd[i*naxis+j] = wcs->pc[i*naxis+j] *= wcs->cdelt[i];
          wcs->cdelt[i]=1;
        }

      /* Set the altlin to be the CD matrix and free the PC matrix. */
      wcs->altlin=2;
      break;

    /* CDi_j: No need to do any conversion. */
    case 2: return; break;

    /* CROTAi: not yet supported. */
    case 4:
      fprintf(stderr, "%s: WARNING: Conversion of 'CROTAi' keywords to the CD "
	      "matrix is not yet supported (for lack of time!), please "
	      "contact us at %s to add this feature. But this may not cause a "
	      "problem at all, so please check if the output's WCS is "
	      "reasonable", __func__, PACKAGE_BUGREPORT);
      exit(EXIT_FAILURE);
      break;

    /* The value isn't supported! */
    default:
      fprintf(stderr, "%s: a bug! Please contact us at %s to fix the "
	      "problem. The value %d for wcs->altlin isn't recognized",
	      __func__, PACKAGE_BUGREPORT, wcs->altlin);
      exit(EXIT_FAILURE);
    }
}





/* To check the quality of the conversion, we'll make a grid of points
   based on image pixels and convert them to RA/Dec with both WCSs. */
void
quality_check(struct params *p, fitsfile *fptr, struct wcsprm *sip,
	      size_t hdu_index)
{
  int nwcs;
  struct wcsprm *tpv;
  char tablename[50];
  double *x, *y, *diff, *rasip, *decsip, *ratpv, *dectpv;
  gal_data_t *tmp, *diffcrd, *imgcrd=NULL, *sipcrd, *tpvcrd;
  size_t i, j, dsize, xwidth=4000, ywidth=4000, num_in_dim=100;

  /* Read the newly written WCS structure. */
  tpv=gal_wcs_read_fitsptr(fptr, 0, 0, &nwcs);

  /* Allocate the coordinates. */
  dsize=num_in_dim*num_in_dim;
  gal_list_data_add_alloc(&imgcrd, NULL, GAL_TYPE_FLOAT64, 1, &dsize,
			  NULL, 0, -1, 1, NULL, NULL, NULL);
  gal_list_data_add_alloc(&imgcrd, NULL, GAL_TYPE_FLOAT64, 1, &dsize,
			  NULL, 0, -1, 1, NULL, NULL, NULL);
  diffcrd=gal_data_alloc(NULL, GAL_TYPE_FLOAT64, 1, &dsize,
			 NULL, 0, -1, 1, NULL, NULL, NULL);

  /* Fill the image coordinates. */
  x=imgcrd->array;
  y=imgcrd->next->array;
  for(i=0;i<num_in_dim;++i)
    for(j=0;j<num_in_dim;++j)
      {
	x[i*num_in_dim+j] = xwidth / num_in_dim * i;
	y[i*num_in_dim+j] = ywidth / num_in_dim * j;
      }

  /* Convert the coordinates to the WCS. */
  sipcrd=gal_wcs_img_to_world(imgcrd, sip, 0);
  tpvcrd=gal_wcs_img_to_world(imgcrd, tpv, 0);

  /* Calculate the distance between each point's calculated RA and Dec
     within the two distortions and write this difference into the
     first column of the 'imgcrd'. */
  diff=diffcrd->array;
  rasip=sipcrd->array;
  ratpv=tpvcrd->array;
  decsip=sipcrd->next->array;
  dectpv=tpvcrd->next->array;
  for(i=0;i<dsize;++i)
    diff[i]=gal_wcs_angular_distance_deg(rasip[i], decsip[i],
					 ratpv[i], dectpv[i])*3600;

  /* Set the column meta data for the final table. */
  tmp=diffcrd;      tmp->name="dist_arcsec";  tmp->unit="arcsec";
  tmp=imgcrd;       tmp->name="x-img";        tmp->unit="pix";
  tmp=imgcrd->next; tmp->name="y-img";        tmp->unit="pix";
  tmp=sipcrd;       tmp->name="ra--sip";      tmp->unit="deg";
  tmp=sipcrd->next; tmp->name="dec-sip";      tmp->unit="deg";
  tmp=tpvcrd;       tmp->name="ra--tpv";      tmp->unit="deg";
  tmp=tpvcrd->next; tmp->name="dec-tpv";      tmp->unit="deg";

  /* Put the columns after each other. */
  diffcrd->next=imgcrd;
  imgcrd->next->next=sipcrd;
  sipcrd->next->next=tpvcrd;

  /* Write the table into a binary FITS table format. */
  sprintf(tablename, "%zu.fits", hdu_index);
  gal_table_write(diffcrd, NULL, NULL, GAL_TABLE_FORMAT_BFITS,
		  tablename, NULL, 0);

  /* Clean up and return. But first set all the name an unit strings
     (that weren't allocated, but are literal). */
  tmp=diffcrd;      tmp->name=tmp->unit=NULL;
  tmp=imgcrd;       tmp->name=tmp->unit=NULL;
  tmp=imgcrd->next; tmp->name=tmp->unit=NULL;
  tmp=sipcrd;       tmp->name=tmp->unit=NULL;
  tmp=sipcrd->next; tmp->name=tmp->unit=NULL;
  tmp=tpvcrd;       tmp->name=tmp->unit=NULL;
  tmp=tpvcrd->next; tmp->name=tmp->unit=NULL;
  gal_list_data_free(diffcrd);
}





/* Main worker function to convert the SIP headers to TPV. */
void *
sip_to_tpv(void *in_prm)
{
  /* Extract the proper pointers. */
  struct gal_threads_params *tprm=(struct gal_threads_params *)in_prm;
  struct params *p=(struct params *)tprm->params;

  /* Necessary definitions. */
  fitsfile *fptr;
  size_t i, hdu_index;
  char *wcsstr, hdu[50];
  int nwcs, status=0, nkeyrec;
  struct wcsprm *sip=NULL, *tpv=NULL;

  /* Go over all the HDUs that were assigned to this thread. */
  for(i=0; tprm->indexs[i] != GAL_BLANK_SIZE_T; ++i)
    {
      /* For easy reading. */
      hdu_index = tprm->indexs[i];

      /* Convert the HDU counter into a string (necessary for
	 'gal_wcs_read', because it can also take HDU names). */
      sprintf(hdu, "%zu", hdu_index);

      /* Open this FITS HDU. */
      fptr=gal_fits_hdu_open(p->filename, hdu, READWRITE);

      /* Read the WCS structure of this HDU */
      sip=gal_wcs_read_fitsptr(fptr, 0, 0, &nwcs);

      /* It may happen that the HDU doesn't have any WCS! In this
	 case, safely ignore this HDU (this can happen for the first
	 HDU). */
      if(sip)
	{
	  /* Convert the distortion to TPV. */
	  tpv=gal_wcs_distortion_convert(sip, GAL_WCS_DISTORTION_TPV, NULL);

	  /* Convert the TPV 'wcsprm' into FITS keywords (as a single
	     string). But first, convert its linear matrix to the CD
	     format (CDELT=1). */
	  gal_wcs_to_cd(tpv);
	  status=wcshdo(WCSHDO_safe, tpv, &nkeyrec, &wcsstr);
	  if(status)
	    {
	      fprintf(stderr, "%s: WARNING: WCSLIB couldn't write the "
		      "new keywords as a string. wcshdu ERROR %d: %s",
		      __func__, status, wcs_errmsg[status]);
	      exit(EXIT_FAILURE);
	    }

	  /* Delete the old keys. */
	  delete_old_keys(fptr);

	  /* Write the new WCS-related keywords into the header. */
	  gal_fits_key_write_wcsstr(fptr, wcsstr, nkeyrec);
	  free(wcsstr);

	  /* Correct the PC+CDELT keywords in the output of WCSLIB.*/
	  correct_pc_to_cd(fptr);

	  /* Clean up (and reset pointer to NULL). */
	  wcsfree(tpv); tpv=NULL;

	  /* Quality check (if requested). */
	  if(p->qualitycheck)
	    quality_check(p, fptr, sip, hdu_index);
	}

      /* Close the FITS HDU and clean up (reset pointer to NULL). */
      fits_close_file(fptr, &status);
      wcsfree(sip); sip=NULL;
    }

  /* Wait for all the other threads to finish, then return. */
  if(tprm->b) pthread_barrier_wait(tprm->b);
  return NULL;
}





int
main(int argc, char *argv[])
{
  struct params p;

  /* Setup the input arguments. */
  p.argc=argc;
  p.argv=argv;
  p.qualitycheck=0;
  p.progname=argv[0];
  p.filename=argv[1];

  /* Basic sanity check(s) and preparations. */
  sanity_check_and_prepare(&p);

  /* Do the job on each HDU individually. */
  gal_threads_spin_off(sip_to_tpv, &p, p.numhdu, p.numthreads, -1, 1);

  /* Clean up and return. */
  return EXIT_SUCCESS;
}
