#!/usr/bin/env python3

# Fit (and extrapolate) a function to a given dataset
#
# This is a Python script to fit a function to a given table of data (two
# columns with X,Y). It will fit any given function the user provides from
# the command line with a guessed parameters. The output result will be a
# table with the values given by the computed function. It is also possible
# to extrapolate to larger values from a given x-point. Basic metadata will
# be added to the output header. A plot is also generated if the user use
# the option '--plot figure.pdf'.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2022, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Usage
# -----
#
# python3 function-fit.py \
#         --guess -2 0 \
#         --rangefit 5,60 --rangenew 1,60 \
#         --plot figure.pdf --output fit.fits \
#         --function "def f(x, a, b): return x**a*b"





# Import modules
import re
import argparse
import numpy as np
from astropy.io import fits
from astropy.table import Table
from scipy.optimize import curve_fit





# Read input arguments from the command line
# ------------------------------------------
#
parser = argparse.ArgumentParser(description='Arguments from the command line')
# By default, argparse uses -h, --help as arguments to provide help. To be
# able to supress this behaviour and use -h for the extension number (as
# all Gnuastro programs do), it is necessary to disable this option. Here,
# the options to obtain help are: -H, --help
parser = argparse.ArgumentParser(add_help=False)

usage="""Example:
         $ python3 python3 function-fit.py
                           --guess -2 0
                           --rangefit 5,60 --rangenew 1,60
                           --plot figure.pdf --output fit.fits
                           --function "def f(x, a, b): return x**a*b"
      """

parser.add_argument('-H', '--help', action='help', default=argparse.SUPPRESS,
                    help=usage)

parser.add_argument('-i', '--inputs', type=str,
                    help='Input file name.')

parser.add_argument('-o', '--output', type=str,
                    help='Output file name.')

parser.add_argument('-p', '--plot', default=None,
                    help='Name of the check plot.')

parser.add_argument('-h', '--hdu', type=int, default=1,
                    help='HDU number. Default is: 1')

parser.add_argument('-g', '--guess', nargs='*', type=float, default=[-2, 0],
                    help='Guess of parameters for the fit. Default is: -2, 0')

parser.add_argument('-s', '--subsky', default=None,
                    help='Subtract the last extrapolated value. Default is "no"')

parser.add_argument('-r', '--rangefit', type=str,  default="0,10",
                    help='Range of x-index values: (imin,imax). Default is: 0,10')

parser.add_argument('-n', '--rangenew', type=str,  default="5,20",
                    help='Range of x-index values: (imin,imax). Default is: 5,20')

parser.add_argument('-F', '--function', type=str, default='def f(x, a, b): return x**a*b',
                    help='''Function definition. IMPORTANT: name of the function has to be 'f'.
                            E.g.,: --function "def f(x, a, b): return b*x**a"
                         ''')





# Define variables from command line arguments
# --------------------------------------------
#
args = parser.parse_args()
hdu = args.hdu
plot = args.plot
guess = args.guess
inputs = args.inputs
output = args.output
subsky = args.subsky
function = args.function
range_fit = args.rangefit.split(',')
range_new = args.rangenew.split(',')





# Define the function given by the user
# -------------------------------------
#
# From this point, the way of calling the function that the user has
# provided with the option '--function' is: eval(fname+"(xmin)").
# Note that 'fname' is the name that the user has provided with the option
# '--fname'.  This is equivalent to have a function 'f(x)' and evaluate it
# at the point xmin with: f(xmin).
exec(function)



# Read the input data
hdu_list = fits.open(inputs)
profile = hdu_list[hdu].data
x = profile[profile.names[0]]
y = profile[profile.names[1]]

# Range of data to be fit
rmin = int(range_fit[0])
rmax = int(range_fit[1])

# Range of data to be extrapolated
x_new_min = int(range_new[0])
x_new_max = int(range_new[1])


# Consider X, Y to be used in the fitting
X = x[rmin:rmax]
Y = y[rmin:rmax]

pini = np.asarray(guess)
popt, pcov = curve_fit(f, X, Y, p0 = pini, check_finite=False)


# Once the fitting has finished, extrapolate the function to the wanted
# values
x_junction = x_new_min
x_new = np.arange(0, x_new_max)

y_new_inner = y[:x_junction]
y_new_outer = f(x_new[x_junction:], *popt)
y_new = np.concatenate((y_new_inner, y_new_outer))


if subsky != None:
    y_out = y_new - y_new[-1]
else:
    y_out = y_new

col1 = fits.Column(name=profile.names[0], format='I', array=x_new)
col2 = fits.Column(name=profile.names[1], format='E', array=y_out)
hdu_output = fits.BinTableHDU.from_columns([col1, col2])

# Injecting fit metadata into the header
function_header = function.split('return ')[-1]

# Select only alpha
function_header_re = re.split('[^a-zA-Z]', function_header)

# Filter the list to only have the function coefficients (removing the 'x'
# and empty spaces '').
fit_coeff_keywords = [i for i in function_header_re if i not in ['','x']]

fit_coeff_values = list(popt)
hdu_output.header['FIT_0'] = function_header
plot_title='F='+function_header
for i,j in zip(fit_coeff_keywords, fit_coeff_values):
    print("COEFFICIENT:", i, j)
    keyword='FIT_'+str(i)
    hdu_output.header[keyword] = j
    plot_title = plot_title +'; '+ i + '=' + str(round(j,2))


hdu_output.writeto(output, overwrite=True)




# Plot figure with useful information if needed
# ---------------------------------------------
#
# Generate a check plot
if plot != None:

     # Import and define the figure (two vertical plots)
     import matplotlib.pyplot as plt

     plt.clf()
     plt.ion()
     transparency = 0.7
     plt.figure(figsize=(10, 5))
     plt.plot(x, y, 'ro', label='Input data', alpha=transparency)
     plt.plot(x_new, y_out, 'k.', label='Extrapolated', alpha=transparency)
     plt.plot(x, f(x, *popt), '--', label='Fitted, ' + function_header)

     plt.vlines(rmin, np.nanmin(y), np.nanmax(y), 'k')
     plt.vlines(rmax, np.nanmin(y), np.nanmax(y), 'k')
     plt.vlines(x_junction, np.nanmin(y), np.nanmax(y), 'b')

     plt.title(plot_title)
     plt.xlabel('x')
     plt.ylabel('y')

     plt.grid()
     plt.show()
     plt.legend()
     plt.xscale('log')
     plt.yscale('log')

     plt.savefig(plot, bbox_inches='tight')


