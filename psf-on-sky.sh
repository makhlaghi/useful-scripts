# Script to put a given PSF file over the locations of stars on any
# position of the sky, and scale the PSF to the total magnitude of the
# star in the given Gaia magnitude.
#
# Things to do:
#  - After the warp of the PSF, its size may become even! In such
#    cases, we need to crop one pixel off one edge of the warped
#    PSF. The edge to crop can be determined from the location of the
#    pixel with the largest value.
#
# Copyright (C) 2025-2025 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Crash script in case of errors.
set -e



# Paramters of script
tmpdir=$(pwd)/build
img_width_deg=1
max_star_dist=2
sb_check_width_arcsec=60
faintest_star_gaia_g_mag=19
galaxy_ra=202.469575
galaxy_dec=47.195258
psf_down_sample=5             # Must be an odd integer
psf=/home/CEFCA/makhlaghi/tmp/psf.fits.fz



# If the temporary directory doesn't exist, make it.
if ! [ -d $tmpdir ]; then mkdir $tmpdir; fi



# From the pixel scale of the PSF, calculate the final image pixel
# scale (CDELT keyword) and the width of the final image (note that
# this should be an odd integer)
cdelt=$(astfits $psf -h0 \
            | awk '/^HISTORY     Propagating w\/ MFT/\
                     {print $5*'$psf_down_sample'/3600}')
img_width_pix=$(echo $img_width_deg $cdelt \
                    | awk '{w=int($1/$2); print w%2?w:w-1}')
img_center_fits=$(echo $img_width_pix \
                      | awk '{print int($1/2)+1}')



# Downsample the PSF to the desired scale.
psfds=$tmpdir/psf-down-sample.fits
astwarp $psf -h0 --scale=1/$psf_down_sample --centeroncorner \
        -o$psfds



# Get list of stars near the given center.
stars=$tmpdir/stars-gaia.fits
astquery gaia --dataset=dr3 --center=$galaxy_ra,$galaxy_dec -o$stars \
         -cra,dec,phot_g_mean_mag --radius=$max_star_dist \
         --range=phot_g_mean_mag,-inf,$faintest_star_gaia_g_mag



# Paint the PSF over the image.
zp=26
psfs=$tmpdir/psfs.fits
asttable $stars \
    | awk '{print NR, $1, $2, 10, 1, 0, 0, 0, $3, 1}' \
    | astmkprof --customimg=$psfds --mode=wcs --zeropoint=$zp \
                --mergedsize=$img_width_pix,$img_width_pix \
                --output=$psfs --crval=$galaxy_ra,$galaxy_dec \
                --crpix=$img_center_fits,$img_center_fits \
                --cdelt=$cdelt,$cdelt --oversample=1



# Convert the image to surface brightness
sb=$tmpdir/sb.fits
pa=$(astfits $psfs --pixelareaarcsec2)
astarithmetic $psfs $pa $zp counts-to-sb -o$sb



# Crop out the central region and use the median to report the surface
# brightness limit.
sbcrop=$tmpdir/sb-crop.fits
sbwp=$(echo $sb_check_width_arcsec $cdelt | awk '{print $1/($2*3600)}')
astcrop $sb --mode=img --center=$img_center_fits,$img_center_fits \
        --zeroisnotblank --width=$sbwp,$sbwp -o$sbcrop



# Report the final surface brighntess limit.
medsb=$(aststatistics $sbcrop --median | asttable -Y)
echo; echo; echo "Central surface brighntess: $medsb"
