# Script to convert HyperLEDA plain-text outputs to FITS tables.
#
# See the description of '--help' within the source below.
#
# Copyright (C) 2022 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.





# Initial values.
bdir=""
quiet=""
input=""
output=""
scriptname=$0





# Output of '--usage' and '--help':
print_usage() {
    cat <<EOF
$scriptname: run with '--help' for list of options
EOF
}

print_help() {
    cat <<EOF
Usage: $scriptname from-leda.txt --output=leda.fits --tmpdir=/path/to/tmp

This script will convert the outputs of HyperLEDA to FITS. To generate
the properly formatted output, you need to request a special format as
described below:

You can query the HyperLEDA catalog through this page:
   http://leda.univ-lyon1.fr/fullsql.html

There are some examples at the bottom of the page above. You can see
the description of all the columns here:
   http://leda.univ-lyon1.fr/leda/meandata.html

Once you have filled your 'SELECT' and 'WHERE' components do the
following steps. The output format is very important!

  1. For "Output as" select "Formatted text".
  2. In the "Separator" box that appears, put ',' (a comma).
  3. Press "Save to file".
  4. Give the downloaded file as the input of this script, and
     optionally select an output name with '--output'.
  5. Specify your column meta data through separate calls
     '--metadata'. It is important that they be in the same order as
     the columns you downloaded. For string columns, do not worry
     about the width, the script will automatically extract the width.
     https://www.gnu.org/s/gnuastro/manual/html_node/Gnuastro-text-table-format.html
  6. Execute the script.

$scriptname options:

 Output:
  -o, --output=STR                  Name of output file.
  -m, --metadata=STR,STR,STR,STR    Name,Unit,Type,About.

 Operating mode:
  -q, --quiet                       Don't print any status information.
  -t, --tmpdir=STR                  Directory for temporary files.

Report bugs to mohammad@akhlaghi.org.
EOF
}





# Output of '--version':
print_version() {
    cat <<EOF
Copyright (C) 2022 Mohammad Akhlaghi <mohammad@akhlaghi.org>
License GPLv3+: GNU General public license version 3 or later.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written/developed by Mohammad Akhlaghi.
EOF
}





# Functions to check option values and complain if necessary.
on_off_option_error() {
    if [ x"$2" = x ]; then
        echo "$scriptname: '$1' doesn't take any values"
    else
        echo "$scriptname: '$1' (or '$2') doesn't take any values"
    fi
    exit 1
}

check_v() {
    if [ x"$2" = x ]; then
        cat <<EOF
$scriptname: option '$1' requires an argument. Try '$scriptname --help' for more information
EOF
        exit 1;
    fi
}





# Extract the metadata values given to this column.
metadata_parse() {


    # Extract each component.
    mc=1
    while read meta; do

	# Depending on the counter, set the value.
	if   [ $mc = 1 ]; then    cname[$inc]="$meta";
	elif [ $mc = 2 ]; then    cunit[$inc]="$meta";
	elif [ $mc = 3 ]; then    ctype[$inc]="$meta";
	elif [ $mc = 4 ]; then ccomment[$inc]="$meta";
	else
	    cat <<EOF
$scriptname: only four value should be given to '--metadata': NAME,UNIT,TYPE,COMMENT.
EOF
	    exit 1
	fi

	# Increment the meta-item counter.
	mc=$((mc+1))

    # Replace a ',' with new-line to read the values one-by-one.
    done < <(echo $1 | tr ',' '\n')

    # If there aren't four entries, report an error.
    if [ $mc != 5 ]; then
	cat <<EOF
$scriptname: four coma-separated values should be given to '--metadata': NAME,UNIT,TYPE,COMMENT, run with '--help' for more
EOF
    fi
}





# Separate command-line arguments from options. Then put the option
# value into the respective variable.
#
# OPTIONS WITH A VALUE:
#
#   Each option has three lines because we want to all common formats: for
#   long option names: '--longname value' and '--longname=value'. For short
#   option names we want '-l value', '-l=value' and '-lvalue' (where '-l'
#   is the short version of the hypothetical '--longname' option).
#
#   The first case (with a space between the name and value) is two
#   command-line arguments. So, we'll need to shift it two times. The
#   latter two cases are a single command-line argument, so we just need to
#   "shift" the counter by one. IMPORTANT NOTE: the ORDER OF THE LATTER TWO
#   cases matters: '-h*' should be checked only when we are sure that its
#   not '-h=*').
#
# OPTIONS WITH NO VALUE (ON-OFF OPTIONS)
#
#   For these, we just want the two forms of '--longname' or '-l'. Nothing
#   else. So if an equal sign is given we should definitely crash and also,
#   if a value is appended to the short format it should crash. So in the
#   second test for these ('-l*') will account for both the case where we
#   have an equal sign and where we don't.
inc=1
input=""
while [ $# -gt 0 ]
do
    # Put the values in the proper variable.
    case "$1" in

        # output
        -o|--output)          output="$2";                           check_v "$1" "$output";  shift;shift;;
        -o=*|--output=*)      output="${1#*=}";                      check_v "$1" "$output";  shift;;
        -o*)                  output=$(echo "$1" | sed -e's/-o//');  check_v "$1" "$output";  shift;;
        -t|--tmpdir)          tmpdir="$2";                           check_v "$1" "$tmpdir";  shift;shift;;
        -t=*|--tmpdir=*)      tmpdir="${1#*=}";                      check_v "$1" "$tmpdir";  shift;;
        -t*)                  tmpdir=$(echo "$1" | sed -e's/-t//');  check_v "$1" "$tmpdir";  shift;;
        -m|--metadata)        m="$2";                                check_v "$1" "$m";       shift;shift;;
        -m=*|--metadata=*)    m="${1#*=}";                           check_v "$1" "$m";       shift;;
        -m*)                  m=$(echo "$1"      | sed -e's/-t//');  check_v "$1" "$m";       shift;;
        -q|--quiet)           quiet=" -q"; shift;;
        -q*|--quiet=*)        on_off_option_error --quiet -q;;

        # Unrecognized option:
        -*) echo "$scriptname: unknown option '$1'"; exit 1;;

        # Not an option (not starting with a '-'): assumed to be input FITS
        # file name.
        *) if [ x"$input" = x ]; then
	       input="$1";
	   else cat <<EOF
$scriptname: only one input should be given
EOF
	   fi; shift;;
    esac

    # If a metadata entry was given, extract its components and add a
    # counter for the number of given metadata.
    if ! [ x"$m" = x ]; then metadata_parse "$m"; inc=$((inc+1)); fi
done





# Sanity checks.
if [ x"$input" = x ]; then
    cat <<EOF
$scriptname: no input file specified, run with '--help' for a description of how to prepare the input
EOF
    exit 1
fi

if [ x"${cname[1]}" = x ]; then
    cat <<EOF
$scriptname: no metadta specified! Use '--metadata=NAME,UNIT,TYPE,COMMENT' (possibly multiple times) to give the metadata of each column, unfortunately this information is not available in HyperLEDA.
EOF
    exit 1
fi

# Optional variables
if [ x"$output" = x ]; then output=$input.fits; fi
if [ x"$tmpdir" = x ]; then tmpdir="hyperleda-to-fits-tmp"; fi





# Build the temporary directory if it doesn't yet exist and make sure
# that the temporary plain-text file hosting the table to be converted
# into FITS doesn't exist (we will be appending lines to it later).
ftxt=$tmpdir/formatted.txt
if ! [ -d $tmpdir ]; then mkdir $tmpdir; fi
rm -f $ftxt




# Extract the widths (number of characters) of each column and write
# the Gnuastro column metadata into the temporary plain-text file.
#
# Note that the input to the 'while' loop comes from the 'grep'
# command after the 'done'.
c=1
ws=""
while read line; do

    # Make sure a metadata was given for this column.
    if [ x"${cname[$c]}" = x ]; then
	echo "$scriptname: not enough '--metadata' entries"
	exit 1
    fi

    # Calculate the number of characaters in this column (there is a
    # single '-' for every character).
    w=$(echo $line | awk '{print length($1)}');

    # Add the width to the string of widths (what will be given to AWK
    # later.
    ws="$ws $w"

    # If this is a string column, we should include its width into its
    # type.
    cctype="${ctype[$c]}"
    if [ $cctype = "str" ]; then cctype=$cctype$w; fi

    # Print the metadata in the plain-text output.
    echo "# Column $c: ${cname[$c]} [${cunit[$c]}, $cctype] ${ccomment[$c]}" \
	 >> $ftxt

    # Increment the counter.
    c=$((c+1))

# Input to the loop above (using the HyperLEDA meta data format).
done < <(grep \#- $input \
             | awk 'BEGIN{FS=","} \
                    NR==1{for(i=1;i<=NF;++i) \
                           printf "%s\n", $i}')
if [ x"$quiet" = x ]; then echo "$scriptname: Metadata prepared."; fi




# Extract the column values with AWK and replace the empty columns
# with NaN.
if [ x"$quiet" = x ]; then echo "$scriptname: reformatting input table ..."; fi
sed -e's/,//g' $input \
    | awk -vws="$ws" \
	  'BEGIN{FIELDWIDTHS = ws} \
           !/^#/{for(i=1;i<=NF;++i) \
                  { t=$i; gsub(" ", "", t);
                    if(length(t)==0) printf("nan "); \
                    else             printf("%s ", $i); \
		  } printf("\n");}' \
    >> $ftxt
if [ x"$quiet" = x ]; then echo "$scriptname: ... reformatting done, writing output..."; fi




# Build the final FITS table and delete the temporary plain-text file.
asttable $ftxt -o$output
if [ x"$quiet" = x ]; then echo "$scriptname: finished! Created $output"; fi
rm -r $tmpdir
