#!/bin/sh
#
# Measure the RAM and CPU usage of a command (assuming it is the only
# major thing that is running).
#
# USAGE (the 'YOUR FULL COMMAND' part can be any command that you
# would run on the command-line, do not run anything while this is
# being executed):
#
#    ./cpu-ram-usage.sh YOUR FULL COMMAND
#
# If your program uses more than one thread, it is best to run it in a
# way that it only consumes one thread. This will make your tests more
# comparable in different scenarios; and is generally the preferred
# way to run programs on clusters (so each thread can be associated to
# one job). You can compare the resulting plots with a single or
# multiple threads to understand this effect.
#
# This script will use the output of 'top' to read the Memory and CPU
# usage every second and store the values in a plain-text table (value
# to the 'out' variable below). If you have Gnuastro, it will also
# convert the plain-text table to a FITS table (with a more
# standardized meta-data structure that you can easily use in tools
# like TOPCAT.
#
# Before starting your command, and after it has finished, this script
# will also get several measurements of the RAM and CPU usage to
# define the "background" usage (before and after your command).
#
# Professional "profiling" tools will not need this "background"
# measurement, but they may be hard to install and will probably also
# need the to-be-tested programs to be compiled with special
# flags. But this script only needs a shell (like Bash), the 'top' and
# 'date' programs that are avaiable on any Unix-like operating system.
#
# Copyright (C) 2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Parameters:

# Number of "background" measurements to take (before and after the
# command).
numback=5

# Name of output file.
out=usage.txt

# Name of the running user.
user=$(whoami)

# Name of optional FITS output file (if Gnuastro is present).
outfits=usage.fits

# Name of temporary file (that will be deleted in the end.
tmpout=usage-tmp.txt










# Basic initialization
firstsec=""
rm -f $tmpout


# Extract the total used memory and CPU percentage for the running user.
statusage () {
    time=$(date +%s)
    used=$(top -bn1 | awk '/^MiB Mem/{m=$8} \
                           $1=="PID"{intab=1} \
                           intab==1 && $2=="'$user'" {c+=$9} \
                           END{print m, c}')
    echo $time $used $1 >> $tmpout
    if [ x"$firstsec" = x ]; then firstsec=$time; fi
    sleep 1
}





# Get some initial background measurements.
for i in $(seq $numback); do
    statusage 0
    echo "$0: Pre-analysis background usage ($i of $numback)."
done





# Run the given command (using a file to make sure it is done)
cdone=command-done.txt
rm -f $cdone
$@ && touch $cdone &
while true; do
    if [ -f $cdone ]; then break; fi
    statusage 1
done
rm $cdone





# Get some final background measurements.
for i in $(seq $numback); do
    statusage 0
    echo "$0: Post-analysis background usage ($i of $numback)."
done





# Print metadata of output and make the final table (subtract the
# starting Unix time to have relative time and re-order the columns).
echo "# Column 1: TimeRel  [sec, u32] Seconds from start of this test." \
     > $out
echo "# Column 2: Memory   [MB,  f32] Megabytes of memory used at this moment." \
     >> $out
echo "# Column 3: CPU      [percent, f32] Percentage of CPU used by '$user'" \
     >> $out
echo "# Column 4: TimeUnix [sec, i64] Seconds from 1970-01-01TT00:00:00." \
     >> $out
echo "# Column 5: Run      [binary, u8] 0: background, 1: runnning command" \
     >> $out
awk '{printf "%-6d %-8.1f %-8.1f %-12ld %d\n", \
      $1-'$firstsec', $2, $3, $1, $4}' $tmpout >> $out
rm $tmpout




# If Gnuastro is present, build a FITS table for a more standardized
# format.
echo
if asttable --version &> /dev/null; then
    asttable $out -o$outfits
    echo "Output: '$outfits' and '$out' (both are the same)";
else
    echo "Output: '$out'";
fi
