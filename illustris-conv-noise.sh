#!/bin/sh

# Given an image from the Illustris simulated exposures of MAST, crop
# a certain region, convolve it with a given kernel and add noise at a
# certain surface brightness limit.
#
# Illustris: https://archive.stsci.edu/hlsp/illustris
#
# Example command below (remove '--fullres' when you are not debugging)
#
#     $ wget https://archive.stsci.edu/hlsps/illustris/tng300-6-5-xyz_365sqarcmin/hlsp_illustris_hst_acs_tng300-6-5-xyz_f814w_v1_sim.fits
#
#     $ width=1
#     $ fwhm=0.6
#     $ sblimit=28
#     $ cdelt=0.2/3600
#     $ ./illustris-conv-noise.sh hlsp_illustris_hst_acs_tng300-6-5-xyz_f814w_v1_sim.fits --center=10860,15162 --width-arcmin=$width --fwhm-arcsec=$fwhm --cdelt=$cdelt --sblimit=$sblimit --fullres --output=fwhm-$fwhm-sblimit-$sblimit.fits
#
# Copyright (C) 2024-2024 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.


# Exit the script in the case of failure
set -e

# To avoid floating points like '23,45' instead of '23.45'.
export LANG=C

# Internal parameters.
hdu=0
quiet=""
inimg=""
cdelt=""
keeptmp=0
tmpdir=""
output=""
center=""
sblimit=""
fullres=0
fwhmarcsec=""
widtharcmin=""

# Internal variables (that don't change the result).
zp=22.5
sblimitsigman=3
sblimitareaarcsec2=100





# Functions to check option values and complain if necessary.
on_off_option_error() {
    if [ x"$2" = x ]; then
        echo "$0: '$1' doesn't take any values"
    else
        echo "$0: '$1' (or '$2') doesn't take any values"
    fi
    exit 1
}

check_v() {
    if [ x"$2" = x ]; then
        cat <<EOF
$0: option '$1' requires an argument. Try '$0 --help' for more information
EOF
        exit 1;
    fi
}

all_nan_warning() {
    cat <<EOF
$0: WARNING: all the pixels in the requested normalization radius are NaN. Therefore the output stamp will be all NaN. If you later use this stamp (among others) with any stack operator of 'astarithmetic' this image will effectively be ignored, so you can safely continue with stacking all the files
EOF
}

print_usage() {
    cat <<EOF
$0: run with '--help' for list of options
EOF
}

print_help() {
    cat <<EOF
Usage: $0 [OPTION] FITS-file(s)

This script will take an FITS file (assumed to be from the Illustris
MAST survey: https://archive.stsci.edu/hlsp/illustris) crop a certain
region of the image, convolve it with a given PSF and add noise at a
certain surface brightness level.

$0 options:
 Input/Output
  -h, --hdu=STR             Extension name or number of input data.
  -o, --output=STR         Name of output file with multiple extensions.

 Processing:
  -c, --center=INT,INT      Center (in pixels) of the crop, given to the
                            '--center' option of Gnuastro's Crop program.
  -w, --width-arcmin=FLT    Width of crop in arcminutes.
  -k, --fwhm-arcsec=FLT     Gaussian Kernel FWHM in arcsec.
      --cdelt=FLT           Pixel scale (deg/pixel) of output (after
                            convolving); for example '--cdelt=0.2/3600'.
  -s, --sblimit=FLT         Surface brightness limit (in magnitudes per
                            arcsec squared), 3-sigma, over 100 arcsec squared.
      --fullres             Include full resolution images in the output HDUs.

 Operating mode:
  -q, --quiet              Don't print outputs of intermediate programs.
  -t, --tmpdir=STR         Name of directory for temporary/intermediate files.
      --keeptmp            Don't delete the temporary directory in the end.
  -?, --help               Print this help list.
  -q, --quiet              Don't print any extra information in stdout.

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

Report bugs to mohammad@akhlaghi.org.
EOF
}






# Separate command-line arguments from options. Then put the option
# value into the respective variable.
#
# OPTIONS WITH A VALUE:
#
#   Each option has three lines because we want to all common formats: for
#   long option names: `--longname value' and `--longname=value'. For short
#   option names we want `-l value', `-l=value' and `-lvalue' (where `-l'
#   is the short version of the hypothetical `--longname' option).
#
#   The first case (with a space between the name and value) is two
#   command-line arguments. So, we'll need to shift it two times. The
#   latter two cases are a single command-line argument, so we just need to
#   "shift" the counter by one. IMPORTANT NOTE: the ORDER OF THE LATTER TWO
#   cases matters: `-h*' should be checked only when we are sure that its
#   not `-h=*').
#
# OPTIONS WITH NO VALUE (ON-OFF OPTIONS)
#
#   For these, we just want the two forms of `--longname' or `-l'. Nothing
#   else. So if an equal sign is given we should definitely crash and also,
#   if a value is appended to the short format it should crash. So in the
#   second test for these (`-l*') will account for both the case where we
#   have an equal sign and where we don't.
while [ $# -gt 0 ]
do
    case "$1" in
        # Input parameters.
        -h|--hdu)            hdu="$2";                                  check_v "$1" "$hdu";  shift;shift;;
        -h=*|--hdu=*)        hdu="${1#*=}";                             check_v "$1" "$hdu";  shift;;
        -h*)                 hdu=$(echo "$1"  | sed -e's/-h//');        check_v "$1" "$hdu";  shift;;
        -o|--output)      output="$2";                          check_v "$1" "$output"; shift;shift;;
        -o=*|--output=*)  output="${1#*=}";                     check_v "$1" "$output"; shift;;
        -o*)              output=$(echo "$1" | sed -e's/-o//'); check_v "$1" "$output"; shift;;

        # Processing options.
        --fullres)              fullres=1; shift;;
        --fullres=*)            on_off_option_error --fullres;;
        -k|--fwhm-arcsec)       fwhmarcsec="$2";                            check_v "$1" "$fwhmarcsec";  shift;shift;;
        -k=*|--fwhm-arcsec=*)   fwhmarcsec="${1#*=}";                       check_v "$1" "$fwhmarcsec";  shift;;
        -k*)                    fwhmarcsec=$(echo "$1" | sed -e's/-k//');   check_v "$1" "$fwhmarcsec";  shift;;
        -s|--sblimit)           sblimit="$2";                               check_v "$1" "$sblimit";  shift;shift;;
        -s=*|--sblimit=*)       sblimit="${1#*=}";                          check_v "$1" "$sblimit";  shift;;
        -s*)                    sblimit=$(echo "$1"  | sed -e's/-s//');     check_v "$1" "$sblimit";  shift;;
        -w|--width-arcmin)      widtharcmin="$2";                           check_v "$1" "$widtharcmin";  shift;shift;;
        -w=*|--width-arcmin=*)  widtharcmin="${1#*=}";                      check_v "$1" "$widtharcmin";  shift;;
        -w*)                    widtharcmin=$(echo "$1"  | sed -e's/-w//'); check_v "$1" "$widtharcmin";  shift;;
        -c|--center)            center="$2";                                check_v "$1" "$center";  shift;shift;;
        -c=*|--center=*)        center="${1#*=}";                           check_v "$1" "$center";  shift;;
        -c*)                    center=$(echo "$1"  | sed -e's/-c//');      check_v "$1" "$center";  shift;;
        --cdelt)                cdelt="$2";                                 check_v "$1" "$cdelt";  shift;shift;;
        --cdelt=*)              cdelt="${1#*=}";                            check_v "$1" "$cdelt";  shift;;

        # operating mode.
        -k|--keeptmp)     keeptmp=1; shift;;
        -k*|--keeptmp=*)  on_off_option_error --keeptmp -k;;
        -t|--tmpdir)      tmpdir="$2";                          check_v "$1" "$tmpdir";  shift;shift;;
        -t=*|--tmpdir=*)  tmpdir="${1#*=}";                     check_v "$1" "$tmpdir";  shift;;
        -t*)              tmpdir=$(echo "$1" | sed -e's/-t//'); check_v "$1" "$tmpdir";  shift;;
        -q|--quiet)       quiet="--quiet"; shift;;
        -q*|--quiet=*)    on_off_option_error --quiet -q;;

        # Unrecognized option:
        -*) echo "$0: unknown option '$1'"; exit 1;;

        # Not an option (not starting with a `-'): assumed to be input FITS
        # file name.
        *) if [ x"$inimg" = x ]; then inimg="$1";
           else echo "$0: only a single input image should be given"; exit 1
           fi; shift;;
    esac

done





# Basic sanity checks
# ===================

# If an input image is not given at all.
if [ x"$inimg" = x ]; then
    echo "$0: no input FITS images. Please given an image from the MAST Illustris survey; see https://archive.stsci.edu/hlsp/illustris"
    exit 1
elif [ ! -f $inimg ]; then
    echo "$0: '$inimg', no such file or directory"; exit 1
fi
if [ x"$center" = x ]; then
    echo "$0: no crop center pixel coordinates specified; use '--center'"
    exit 1
fi
if [ x"$widtharcmin" = x ]; then
    echo "$0: no crop width (in arcmin) specified; use '--width-arcmin'"; exit 1
fi
if [ x"$fwhmarcsec" = x ]; then
    echo "$0: no kernel FWHM (in arcsec); use '--fwhm-arcsec'"; exit 1
fi
if [ x"$cdelt" = x ]; then
    echo "$0: no output pixel scale; use '--cdelt'"; exit 1
fi
if [ x"$sblimit" = x ]; then
    echo "$0: no surface brightness limit (3-sigma, over 100 arcsec^2) to define noise level; use '--sblimit'"
    exit 1
fi
if [ x"$output" = x ]; then
    echo "$0: no output file name; use '--output'"; exit 1
fi
bunit=$(astfits $inimg -h0 --keyvalue=BUNIT --quiet)
if [ x"$bunit" != xnanoJanskies ]; then
    echo "$0: input ('$inimg') is not in nanoJanskies"; exit 1
fi





# The temporary directory (build name if not given).
if [ x"$tmpdir" = x ]; then tmpdir=$output-tmp; fi
if [ -d "$tmpdir" ]; then  rm -r "$tmpdir"; fi
mkdir "$tmpdir"





# Build the kernel in the illustris resolution.
kernelimg=$tmpdir/kernel.fits
ps=$(astfits $inimg --hdu=$hdu --pixelscale --quiet \
             | awk '{print $1}')
kfwhmpix=$(echo $fwhmarcsec $ps | awk '{print $1/($2*3600)}')
astmkprof --kernel=gaussian,$kfwhmpix,5 --oversample=1 \
          --output=$kernelimg $quiet





# Crop the desired region of the image: we are including the width of
# the kernel to the pixel-width of the crop since it will affect
# convolution.
crop=$tmpdir/crop.fits
width=$(echo $widtharcmin $ps $kfwhmpix \
            | awk '{print $1/($2*60)+2*$3}')
astcrop $inimg --hdu=$hdu --mode=img --center=$center \
        --width=$width,$width --output=$crop $quiet





# For some reason the WCS of Illustris is not standard (its RA is
# aligned with the horizontal). So we will flip it.
flipped=$tmpdir/flipped.fits
astwarp $crop --flip=0,1 -o$flipped





# Convert the units.
counts=$tmpdir/counts.fits
astarithmetic $flipped 1e9 / $zp jy-to-counts --output=$counts \
              $quiet
astfits $counts -h1 --write=EXTNAME,ORIG-RES-NO-CONV





# Convolve the cropped image with the given kernel.
conv=$tmpdir/conv.fits
astconvolve $counts --kernel=$kernelimg --domain=spatial \
            --output=$conv $quiet
astfits $conv -h1 --write=EXTNAME,ORIG-RES-NO-NOISE





# Warp the images to the desired pixel scale.
warped=$tmpdir/warped.fits
warpednc=$tmpdir/warped-no-conv.fits
astwarp $conv --cdelt=$cdelt --width=$widtharcmin/60 -o$warped $quiet
astwarp $counts --cdelt=$cdelt --width=$widtharcmin/60 -o$warpednc $quiet
astfits $warped   -h1 --update=EXTNAME,NO-NOISE
astfits $warpednc -h1 --update=EXTNAME,NO-CONV





# Warp the kernel to the pixel scale of the output. First we will add
# the input's WCS, the use Warp's --cdelt.
kernelo=$tmpdir/kernel-out-pixel-scale.fits
kernelot=$tmpdir/kernel-out-pixel-scale-tmp.fits
kernelott=$tmpdir/kernel-out-pixel-scale-tmp-tmp.fits
astarithmetic $quiet $kernelimg --wcsfile=$flipped -o$kernelot
astwarp $kernelot --cdelt=$cdelt -o$kernelott
astarithmetic $kernelott set-i i i isblank 0 where set-j \
              j j sumvalue / float32 --wcsfile=none -o$kernelo
astfits $kernelo -h1 --write=EXTNAME,KERNEL
rm $kernelot $kernelott





# Check if the kernel that is convolved to the output resolution has
# an odd number of pixels.
check=$(astfits $kernelo --keyvalue=NAXIS1,NAXIS2 --quiet \
            | awk '{ if($1%2==0 || $2%2==0) print "bad"; \
                     else print "good"}')
if [ $check = bad ]; then
    echo "$0: the kernel is not centralized in the requested pixel scale (in other words, the kernel image in this resolution has an even set of pixels in the width or height)"; exit 1
fi





# Add noise to the counts image; but first, convert the surface
# brightness limit to a noise level in counts.
pixarea=$(astfits $warped --pixelareaarcsec2)
sigma_counts=$(astarithmetic $sblimit $zp \
			     $sblimitareaarcsec2 $pixarea x sqrt \
			     sb-to-counts $sblimitsigman / --quiet)
astarithmetic $warped $sigma_counts mknoise-sigma float32 \
              -o$output $quiet
astfits $output -h0 \
        --write=EXTNAME,METADATA \
        --write=/,"Paramters used to build this image" \
        --write=ZEROPT,$zp,"Zeropoint of image."
astfits $output -h1 --write=EXTNAME,REAL-SIM





# Surface brightness image (only if requested)
sbcounts=$tmpdir/sb-counts.fits
astarithmetic $warped $zp $pixarea counts-to-sb \
              --output=$sbcounts $quiet





# Copy other necessary images into the output.
astfits $warped   --copy=1 -o $output
astfits $warpednc --copy=1 -o $output
astfits $kernelo  --copy=1 -o $output
if [ $fullres = 1 ]; then
    astfits $conv   --copy=1 -o $output
    astfits $counts --copy=1 -o $output
fi





# Delete the temporary file.
if [ $keeptmp = 0 ]; then rm -r $tmpdir; fi
