# This Makefile is for stacking many large images (such that loading them
# all together will become larger than the available RAM).
#
# Usage:
#
#   - Before executing this Makefile, set the input and build (output)
#     directories:
#
#     - INPUTS: This is the list of input files that will be used,
#       they can be anywhere within your filesystem, it is just
#       important that the files do not have the same file name.
#
#     - INPUTHDU: The HDU name/number to be used for all the input
#       files.
#
#     - BDIR: This is the build or output directory where all
#       intermediate files are placed. You can set this to any
#       directory on your file system.
#
#     - OUTPUT: Output file: name of table containing the value to add
#       to each image so that its background is matched to the rest.
#
#     - CROP_NUMBER_D1: Number of crops along first dimension.
#
#     - CROP_NUMBER_D2: Number of crops along second dimension.
#
#     - MASK_OPERATOR: Arithmetic operator (with parameters, but without
#       the number of inputs) to use for masking. For example '5 0.01
#       madclip-maskfilled'.
#
#     - STACK_OPERATOR: Arithmetic operator (with parameters) to use for
#       stacking for example: '3 0.1 sigclip-median'.
#
#     - STACK_NEEDED_RAM_GB: [OPTIONAL] needed RAM for each stacking
#       operation. Since this script is designed for scnearios with
#       hundreds of inputs, they can easily fill up the whole RAM of the
#       Pipeline to crash. Only use this option if you actually confront a
#       crash due to the RAM (where the programs will be "kill"ed by the
#       OS). To find the used RAM, take these step:
#         - Put an 'echo GOOD; exit 1' after the 'astarithmetic' command of
#          the '$(stack-crop)' target below.
#         - Run your pipeline on a single thread without changing the
#          'BDIR', but put '/usr/bin/time --format=%M' before your 'make'
#          command.
#         - After your pipeline finishes (with the 'GOOD' printed), a
#           number will be printed: this is the amount of RAM (in
#           kilobytes) a single run consumed; so multiply it by 1e9/1e3=1e6
#           to convert it to Gigabytes and give it to this option.
#
#     - LIBGNUASTRO_MAKE: Location of 'libgnuastro_make.so'. This GNU
#       Make extension (installed with Gnuastro) is mandatory for this
#       script. For more on how to find the location, see this page:
#       https://www.gnu.org/software/gnuastro//manual/html_node/Loading-the-Gnuastro-Make-functions.html
#
#   - After setting the directories above, to execute this Makefile,
#     simply run 'make' like below:
#
#        export BDIR=/path/to/build
#        export INPUTS="/path/to/file-1.fits /other/path/file-2.fits ..."
#        make -f 2009-flat-master-scale.mk
#
#     You can also set the 'INPUTS' and 'BDIR' variables from the
#     command-line. In this case, the command-line values will take
#     precedence over the values within the Makefile. Like below:
#
#        make -f 2009-flat-master-scale.mk \
#                BDIR=/path/to/build \
#                INPUTS="/path/to/file-1.fits /other/path/file-2.fits ..."
#
#   - To execute it on many threads use the '-j' (for "jobs") option
#     of Make). For example '-j12' will execute on 12 threads.
#
# Copyright (C) 2024-2024 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# For a copy of the GNU General Public License see
# <http://www.gnu.org/licenses/>.





# First target (print a message if the necessary options are are empty).
ifeq ($(strip $(INPUTS)),)
all:
	@echo "stack-large.mk: 'INPUTS' is empty!"; exit 1
else
ifeq ($(words $(INPUTHDU) $(OUTPUT) $(BDIR) $(CROP_NUMBER_D1) \
              $(CROP_NUMBER_D2) ),5)
all: $(OUTPUT)
else
all:
	@printf "stack-large.mk: at least one of the following "
	printf "mandatory input environment variables are missing "
	printf "'INPUTHDU', 'OUTPUT', 'BDIR', 'CROP_NUMBER_D1', "
	printf "CROP_NUMBER_D2\n"; exit 1
endif
endif





# Internal Make settings
# ----------------------
#
# These are just basic Make settings to make sure things run smoothly.
.ONESHELL:
.SECONDEXPANSION:
.SHELLFLAGS = -ec
$(BDIR):; mkdir $@
load $(LIBGNUASTRO_MAKE)




# Width of the first image
# ------------------------
#
# This is necessary to make sure that there will be no problem later.
refsizes = $(BDIR)/reference-sizes.txt
$(refsizes): $(word 1, $(INPUTS)) | $(BDIR)
	astfits $< -h$(INPUTHDU) --keyvalue=NAXIS1,NAXIS2 --quiet \
	        > $@.txt
	awk '{print int($$1/$(CROP_NUMBER_D1)), \
	            int($$2/$(CROP_NUMBER_D2))}' \
	    $@.txt >> $@.txt
	mv $@.txt $@






# Identifier for each input
# -------------------------
#
# Associate each input file with a number in the build directory. We will
# create a plain-text file containing the full name of the input.
iseq := $(shell seq $(words $(INPUTS)))
xseq := $(shell seq $(CROP_NUMBER_D1))
yseq := $(shell seq $(CROP_NUMBER_D2))
ids := $(foreach i, $(iseq), \
         $(foreach x, $(xseq), \
          $(foreach y, $(yseq), $(i)--$(x)-$(y))))
crops := $(foreach i, $(ids), $(BDIR)/crop-$(i).txt)
stack-crop := $(foreach x, $(xseq), \
                $(foreach y, $(yseq), \
                  $(BDIR)/stack-$(x)-$(y).fits))
$(crops): $(BDIR)/crop-%.txt: \
          $$(ast-text-prev \
             $(BDIR)/stack-$$(word 2,$$(subst --, ,%)).fits, \
             $(stack-crop)) \
          $(refsizes) | $(BDIR)

#	Get the dimension of this crop.
	@x=$$(echo $(word 2,$(subst -, ,$*)) | awk '{print $$1-1}')
	y=$$(echo $(word 3,$(subst -, ,$*)) | awk '{print $$1-1}')
	i=$$(echo "$$x $$y" \
	          | awk -vw=$(CROP_NUMBER_D1) '{print $$2*w+$$1}')

#	Check if the input is a FITS file.
	wr1=$$(awk 'NR==1{print $$1}' $(refsizes))
	wr2=$$(awk 'NR==1{print $$2}' $(refsizes))
	name=$(word $(word 1,$(subst -, ,$*)), $(INPUTS))
	if astfits -h$(INPUTHDU) $$name &> /dev/null; then

#	  Extract the width of this input.
	  eval "$$(astfits $$name --keyvalue=NAXIS1,NAXIS2 --quiet \
                           | xargs printf "w1=%s; w2=%s")"

#	  The width should be the same as the first input.
	  if [ $$w1 = $$wr1 ] && [ $$w2 == $$wr2 ]; then

#	    The "--section" values that should be used for this crop.
	    sec=$$(awk 'NR==2{x='$$x'; y='$$y'; \
	                      xmax=$(CROP_NUMBER_D1)-1; \
	                      ymax=$(CROP_NUMBER_D2)-1; \
	                      printf "%s:%s,%s:%s\n", \
	                      $$1*x+1, x==xmax?"":$$1*(x+1), \
	                      $$2*y+1, y==ymax?"":$$2*(y+1)}' \
	               $(refsizes))

#	    Do the crop (in a final FITS file).
	    astcrop $$name -h$(INPUTHDU) --section=$$sec --mode=img \
	            --quiet -o$(subst .txt,.fits,$@)

#	    Print a small sentence (since the recipe of this rule is not
#	    printed in the output.
	    echo "stack-large.mk: crop-$*.fits"

#	  The width is different from the first.
	  else
	    printf "ERROR: '$$name' ($$w1"x"$$w2) does not have the same "
	    printf "size as '$(word 1, $(INPUTS))' ($$wr1"x"$$wr2)! All "
	    printf "inputs should have the same number of pixels in both "
	    printf "dimensions\n"
	    exit 1
	  fi
	else
	  echo "ERROR: '$$name' does not exist or is not a FITS file"
	  exit 1
	fi

#	Final (fake!) target.
	touch $@





# Stack all the inputs in each crop
# ---------------------------------
#
# No problem if 'STACK_NEEDED_RAM_GB' is empty, the Gnuastro Makefile
# extension 'ast-text-prev-batch-by-ram' will not do anything when the
# second argument is empty.
$(stack-crop): $(BDIR)/stack-%.fits: \
               $$(foreach i, $(iseq), $(BDIR)/crop-$$(i)--%.txt) \
               $$(ast-text-prev-batch-by-ram $$@, $(STACK_NEEDED_RAM_GB), \
                                             $(stack-crop))

#	In case 'STACK_NEEDED_RAM_GB' is given, the prerequite list may
#	contain other sources. So we need to only use the 'crop-%.fits'
#	files of the prerequisite list.
	fits="$(subst .txt,.fits,$(filter $(BDIR)/crop-%.txt, $^))"

#	Stack all these crops and clean up (delete the extra crops).
	num=$$(echo $$fits | wc -w)
	astarithmetic $$fits $$num $(MASK_OPERATOR) \
	              $$num $(STACK_OPERATOR) --writeall -g1 -o$@
	rm $$fits





# Final target
# ------------
#
# Stitch all the stacked crops together into the original image and put the
# WCS of one of the inputs on it.
$(OUTPUT): $(stack-crop)

#	Construct the list of arguments.
	arg=""
	for y in $(yseq); do
	  for x in $(xseq); do
	    arg="$$arg $(BDIR)/stack-$$x-$$y.fits "
	  done
	  arg="$$arg $(CROP_NUMBER_D1) 1 stitch"
	done
	arg="$$arg $(CROP_NUMBER_D2) 2 stitch"

#	Build the numbers and stack images separately.
	stack=$(BDIR)/stack.fits
	num=$(BDIR)/stack-numbers.fits
	astarithmetic $$arg -g2 --wcsfile=$(word 1,$(INPUTS)) -o $$num
	astarithmetic $$arg -g1 --wcsfile=$(word 1,$(INPUTS)) -o $$stack

#	Put them in one file: we have not used '$@' instead of the 'stack'
#	shell variable so if anything goes wrong, Make continues (it will
#	not continue if '$@' exists).
	astfits $$num --copy=1 --output=$$stack
	mv $$stack $@
